<?php

namespace App;

use App\Notifications\ChefquartierResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ChefQuartiers extends Authenticatable
{
    use Notifiable;

    protected $table = "chefquartiers";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName',
        'lastName',
        'nationalIdentity',
        'telephone',
        'email',
        'dateOfBirth',
        'hireDate',
        'picture',
        'working',
        'role',
        'quartier_id',
        'verify',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ChefquartierResetPassword($token));
    }


    public function quartier(){
        return $this->belongsTo('App\Quartier');
    }
}
