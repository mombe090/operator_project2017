<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quartier extends Model
{


    public function chefquartier(){
        return $this->hasOne('App\ChefQuartiers');
    }
    public function commune(){
        return $this->belongsTo('App\Commune');
    }
    public function secteurs(){
        return $this->hasMany('App\Secteur');
    }

    protected $fillable = [
        'code',
        'name',
        'commune_id'
    ];
}
