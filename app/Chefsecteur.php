<?php

namespace App;

use App\Notifications\ChefsecteurResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Chefsecteur extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName',
        'lastName',
        'nationalIdentity',
        'telephone',
        'email',
        'dateOfBirth',
        'hireDate',
        'picture',
        'working',
        'role',
        'secteur_id',
        'verify',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ChefsecteurResetPassword($token));
    }
    public function secteur(){
        return $this->belongsTo('App\Secteur');
    }
}
