<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable =[
        'code',
        'name',
    ];
    public function superadmins(){
        return $this->hasOne('App\Superadmin');
    }

    public function prefectures(){
        return $this->hasMany('App\Prefecture');
    }
}
