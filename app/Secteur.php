<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Secteur extends Model
{
    public function chefsecteur(){
        return $this->hasOne('App\ChefSecteurs');
    }
    public function quartier(){
        return $this->belongsTo('App\Quartier');
    }
    public function terrains(){
        return $this->hasMany('App\Terrain');
    }
    protected $fillable =[
        'name',
        'code',
        'quartier_id'
    ];
}
