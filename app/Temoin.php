<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temoin extends Model
{
   protected $fillable =[
       'nationalIdentity' ,
       'firstName',
       'lastName' ,
       'email',
       'telephone',
       'picture',
       'dateOfBirth',
       'terrain_id',
       'acheteur_id'
   ];
   protected $table = 'temoinsA';
}
