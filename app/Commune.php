<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    protected $fillable = [
        'code',
        'name',
        'prefecture_id'
    ];
    public function maire(){
        return $this->hasOne('App\Maire');
    }
    public function quartiers(){
        return $this->hasMany('App\Quartier');
    }
    public function prefecture(){
        return $this->belongsTo('App\Prefecture');
    }
}
