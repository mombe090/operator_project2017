<?php

namespace App;

use App\Notifications\ProprietaireResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Proprietaire extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nationalIdentity' ,
        'firstName',
        'lastName' ,
        'email',
        'telephone',
        'picture',
        'dateOfBirth',
        'verify',
        'working'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ProprietaireResetPassword($token));
    }
    public function  terrains(){
        return $this->hasMany('App\Terrain');
    }
}
