<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prefecture extends Model
{
    protected $fillable = [
        'code',
        'nom',
        'region_id'
    ];
    public function region(){
        return $this->belongsTo('App\Region');
    }

    public function admin(){
        return $this->hasOne('App\Admin');
    }
    public function communes(){
        return $this->hasMany('App\Commune');
    }
}
