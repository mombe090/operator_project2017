<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acheteur extends Model
{
  protected $fillable = [
      'nationalIdentity' ,
      'firstName',
      'lastName' ,
      'email',
      'telephone',
      'picture',
      'dateOfBirth',
      'terrain_id',
 ];
}
