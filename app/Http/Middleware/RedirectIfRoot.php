<?php

namespace App\Http\Middleware;

use const AF_UNIX;
use App\Superadmin;
use Closure;
use Illuminate\Support\Facades\Auth;
use Session;

class RedirectIfRoot
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'root')
	{
	    if (Auth::guard($guard)->check()) {

	        return redirect('root/home');
	    }

	    return $next($request);
	}
}