<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfChefsecteur
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'chefsecteur')
	{
	    if (Auth::guard($guard)->check()) {
	        return redirect('chefsecteur/home');
	    }

	    return $next($request);
	}
}