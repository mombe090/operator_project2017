<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotProprietaire
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'proprietaire')
	{
	    if (!Auth::guard($guard)->check()) {
	        return redirect('proprietaire/login');
	    }

	    return $next($request);
	}
}