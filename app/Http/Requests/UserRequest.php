<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use const true;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastName' => 'bail|required|min:2|alpha_num',
            'firstName' =>  'bail|required|min:2',
            'email' =>  'bail|required|email|unique:users',
            'nationalIdentity' => 'bail|required|numeric',
            'password'  => 'bail|required|min:6|confirmed',
            'password_confirmation'  => 'bail|required',
        ];
    }
}
