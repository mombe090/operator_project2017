<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use const true;

class ChefSecteurRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'bail|required|max:15',
            'lastName' => 'bail|required|max:15',
            'email' => 'bail|required|email|max:30|unique:chefsecteurs',
            'nationalIdentity' => 'bail|required|unique:chefsecteurs',
            'telephone' => 'bail|required|unique:chefsecteurs',
            'password' => 'required|min:6|confirmed',
        ];
    }
}
