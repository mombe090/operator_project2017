<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use const true;

class MaireRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'bail|required|max:255',
            'lastName' => 'bail|required|max:255',
            'email' => 'bail|required|email|max:255|unique:maires',
            'telephone' => 'bail|required|max:12|unique:maires',
            'nationalIdentity' => 'bail|required|unique:maires',
            'password' => 'required|min:6|confirmed',
        ];
    }
}
