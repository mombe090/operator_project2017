<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use const true;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'bail|required|max:255',
            'lastName' => 'bail|required|max:255',
            'email' => 'bail|required|email|max:255|unique:admins',
            'nationalIdentity' => 'bail|required|unique:admins|max:10',
            'telephone' => 'bail|required|unique:admins|max:12',
            'password' => 'required|min:6|confirmed',
        ];
    }
}
