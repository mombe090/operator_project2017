<?php

namespace App\Http\Controllers\ChefsecteurAuth;

use App\Admin;
use App\ChefQuartiers;
use App\Chefsecteur;
use App\ChefSecteurs;
use App\Commune;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChefSecteurRequest;
use App\Maire;
use App\Month;
use App\Notifications\ChefSecteurRegisterNotification;
use App\Notifications\ChefSecteurRegisterNotificationGood;
use App\Quartier;
use App\Root;
use App\Secteur;
use App\Superadmin;
use function dd;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use MercurySeries\Flashy\Flashy;
use const null;
use function ob_flush;
use function session;
use Validator;
use function compact;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/chefsecteur/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('chefsecteur.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:chefsecteurs',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     *
     * @return ChefSecteurs
     */
    protected function create(array $data)
    {
        return ChefSecteurs::create([
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'nationalIdentity' => $data['nationalIdentity'],
            'telephone' => $data['telephone'],
            'email' => $data['email'],
            'dateOfBirth' => $data['jour'].'/'.$data['mois'].'/'.$data['annee'],
            'hireDate' => $data['jourE'].'/'.$data['moisE'].'/'.$data['anneeE'],
            'working' => 'off',
            'picture' => '/storage/images/users/default.jpg',
            'verify' => str_replace('/', '', Hash::make(str_shuffle(str_random(100)))),
            'secteur_id' => $data['secteur_id'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm(Request $request)
    {

        $secteurs = Secteur::where('quartier_id', $request->quartier)->get();
        $months = Month::all();
        return view('chefsecteur.auth.register', compact('secteurs', 'months'));
    }

    public function showRegistrationFormCommune()
    {
        $communes = Commune::orderBy('name')->get();

        return view('connexion.chooseCommuneForChefSecteur', compact( 'communes'));
    }
   public function showRegistrationFormQuartier(Request $request)
    {


        $quartiers = Quartier::where('commune_id', $request->commune)->orderBy('name')->get();
        return view('connexion.chooseQuartierForChefSecteur', compact( 'quartiers'));
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('chefsecteur');
    }
    public function register(ChefSecteurRequest $request)
    {
        //On s'assure que le mail ou l'ide

        $idSecteurForChefSecteur = Chefsecteur::where('secteur_id', $request->input('secteur_id'))->where('working', 'on')->first();
        if ($idSecteurForChefSecteur != null){
            Flashy::error('Le secteur a déjà un chef de secteur en fonction. Merci de vous adresser au chef de quartier.');
            session()->flash('emailConfirmationError', 'Le secteur a déjà un chef de secteur en fonction. Merci de vous adresser au chef de quartier..');
            return redirect()->back()->withInput();
        }
        $rootEmails = Root::where('email', $request->input('email'))->first();
        $superadminEmails = Superadmin::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone')) ->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        $adminEmails = Admin::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone')) ->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        $maireEmails = Maire::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone')) ->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        $chefQuartierEmails = ChefQuartiers::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone')) ->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();

        $secteurIdForChefSecteur = ChefSecteurs::where('secteur_id', $request->input('secteur_id'))->where('working', 'on')->first();


          if ($secteurIdForChefSecteur != null){
              Flashy::error('Le secteur a déjà un chef de secteur en fonction. Merci de vous addresser au chef de quartier de la localité');
              session()->flash('emailConfirmationError', 'Le secteur a déjà un chef de secteur en fonction. Merci de vous addresser au chef de quartier de la localité');
              return redirect()->back()->withInput();
          }



        if ($rootEmails !=null){
            Flashy::error('Quoi......L\'email là est detenu par le root, le boss');
            return redirect()->back()->withInput();
        }
        if ($superadminEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale en question est detenu par un super administrateur ');
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale en question est detenu par un super administrateur.');
            return redirect()->back()->withInput();
        }
        if ($adminEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale en question est detenu par un administrateur ');
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale en question est detenu par un administrateur ');
            return redirect()->back()->withInput();
        }
        if ($maireEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale en question est detenu par un maire ');
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale en question est detenu par un maire ');
            return redirect()->back()->withInput();
        }
        if ($chefQuartierEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale en question est detenu par un chef de secteur ');
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale en question est detenu par un chef de secteur ');
            return redirect()->back()->withInput();
        }
        $request->all();

        event(new Registered($user = $this->create($request->all())));
        $user->notify(new ChefSecteurRegisterNotification());

        Flashy::success('Votre compte a été crée avec succès. Confirmez votre email SVP');
        session()->flash('emailConfirmation', 'Votre compte a été crée avec succès. Confirmez votre email SVP');
        return redirect('/chefsecteur/login');
    }


    public function confirm($id, $token){
        $user = ChefSecteurs::where('id', $id)->where('verify', $token)->first();

        if ($user){
            $user->update(
                [
                    'verify' => null
                ]
            );
            //$this->guard()->login($user);
            Flashy::info("Une dernière chose faite-vous confirmez par votre chef de quartier.");
            session()->flash('emailConfirmationInfo',"Une dernière chose faite-vous confirmez par votre chef de quartier.");
            return redirect('/chefsecteur/login');
        }else{
            Flashy::error('Ce lien ne semble pas valide ou à déjà été confirmé. Merci de bien verifier authenticité de ce mail.');
            session()->flash('emailConfirmationError', 'Ce lien ne semble pas valide ou à déjà été confirmé. Merci de bien verifier authenticité de ce mail.');
            return redirect('/chefsecteur/login');
        }
    }
}
