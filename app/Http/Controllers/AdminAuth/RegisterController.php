<?php

namespace App\Http\Controllers\AdminAuth;

use App\Admin;
use App\Chefquartier;
use App\Chefsecteur;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use App\Maire;
use App\Month;
use App\Notifications\AdminRegisterNotification;
use App\Root;
use App\Superadmin;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use MercurySeries\Flashy\Flashy;
use Validator;
use function compact;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:admins',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Admin
     */
    protected function create(array $data)
    {
        return Admin::create([
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'nationalIdentity' => $data['nationalIdentity'],
            'telephone' => $data['telephone'],
            'email' => $data['email'],
            'dateOfBirth' => $data['jour'].'/'.$data['mois'].'/'.$data['annee'],
            'hireDate' => $data['jourE'].'/'.$data['moisE'].'/'.$data['anneeE'],
            'working' => 'off',
            'picture' => '/storage/images/users/default.jpg',
            'verify' => str_replace('/', '', Hash::make(str_shuffle(str_random(100)))),
            'prefecture_id' => $data['prefecture_id'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $months = Month::all();
        $prefectures = DB::table('prefectures')->orderBy('nom')->get();
        return view('admin.auth.register', compact('months', 'prefectures'));
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }
    public function register(AdminRequest $request)
    {
        //On s'assure que le mail ou l'ide

        $prefectureIdForAdmin = Admin::where('prefecture_id', $request->input('prefecture_id'))->where('working', 'on')->first();
        if ($prefectureIdForAdmin != null){
            Flashy::error('La préfecture a déjà un Directeur du DO-CAD en fonction. Merci de vous addresser au Directeur du BCF de votre région.');
            session()->flash('emailConfirmationError', 'La préfecture a déjà un Directeur du DO-CAD en fonction. Merci de vous addresser au Directeur du BCF de votre région.');
            return redirect()->back()->withInput();
        }

        $rootEmails = Root::where('email', $request->input('email'))->first();
        if ($rootEmails !=null){
            session()->flash('emailConfirmationError','L\'email en question est detenu par le root');
            Flashy::error('L\'email en question est detenu par le root');
            return redirect(route('superadminRegister'))->withInput();
        }

        $superadminEmails = Superadmin::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        if ($superadminEmails!=null){
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale en question est detenu par un super administrateur ');
            Flashy::error('L\'email ou le numero d\'identité nationale en question est detenu par un super administrateur ');
            return redirect(route('superadminRegister'))->withInput();
        }

        $maireEmails = Maire::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        if ($maireEmails!=null){
            session()->flash('emailConfirmationError','L\'email ou le numero d\'identité nationale en question est detenu par un maire ');
            Flashy::error('L\'email ou le numero d\'identité nationale en question est detenu par un maire ');
            return redirect(route('superadminRegister'))->withInput();
        }

        $chefDeQuartierEmails = Chefquartier::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        if ($chefDeQuartierEmails!=null){
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale en question est detenu par un chef de quartier ');
            Flashy::error('L\'email ou le numero d\'identité nationale en question est detenu par un chef de quartier ');
            return redirect(route('superadminRegister'))->withInput();
        }

        $chefSecteurEmails = Chefsecteur::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        if ($chefSecteurEmails!=null){
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale en question est detenu par un chef de secteur ');
            Flashy::error('L\'email ou le numero d\'identité nationale en question est detenu par un chef de secteur ');
            return redirect(route('superadminRegister'))->withInput();
        }





        $request->all();

        event(new Registered($user = $this->create($request->all())));
         $user->notify(new AdminRegisterNotification());
        session()->flash('emailConfirmation', 'Votre compte a été crée avec succès. Confirmez votre email SVP');
        Flashy::success('Votre compte a été crée avec succès. Confirmez votre email SVP');
        return redirect('/admin/login');
    }


    public function confirm($id, $token){
        $user = Admin::where('id', $id)->where('verify', $token)->first();

        if ($user){
            $user->update(
                [
                    'verify' => null
                ]
            );
            //$this->guard()->login($user);
            Flashy::info("Une dernière chose faite-vous confirmez par le super administrateur de votre region.");
            session()->flash('emailConfirmationInfo', 'Une dernière chose faite-vous confirmez par le DG du BCF de votre region.');
            return redirect('/admin/login');
        }else{
            Flashy::error('Ce lien ne semble pas valide. Merci de bien verifier authenticité de ce mail.');
            return redirect('/admin/login');
        }
    }
}
