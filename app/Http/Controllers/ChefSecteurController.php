<?php

namespace App\Http\Controllers;

use App\ChefSecteurs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;

class ChefSecteurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChefSecteurs $chefSecteur
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id != Auth::user()->id){
            Flashy::error("Vous ne pouvez pas voir/ni modifier le profile d'un collègue chef de quartier");
            return redirect()->back();
        }
        $chefSecteur = ChefSecteurs::findOrFail($id);

        return view('gestions_des_administrateurs.chef_secteurs.profiles.chef_secteurs_profile', compact('chefSecteur'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChefSecteurs $chefSecteur
     * @return \Illuminate\Http\Response
     */
    public function edit(ChefSecteurs $chefSecteur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\ChefSecteurs        $chefSecteur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($id != Auth::user()->id){
            Flashy::error("Vous ne pouvez pas voir/ni modifier le profile d'un collègue chef de quartier");
            return redirect()->back();
        }

        $chefSecteur = ChefSecteurs::findOrFail($id);
        if ($request->picture !=null){
            $picture = $request->file('picture');
            $filename = $chefSecteur->email.'.'.str_shuffle(str_random(10)).'.'.$picture->getClientOriginalExtension();
            Image::make($picture)->resize(300, 300)->save('storage/images/users/chefSecteurs/profile_img/'.$filename);
            $chefSecteur->picture = 'storage/images/users/chefSecteurs/profile_img/'.$filename;
            $chefSecteur->save();
            Flashy::success("Votre photo de profile à été mise à jour avect succès");
            return redirect()->back();
        }
        else  if (isset($request->isBio)){
            $this->validate($request, [
                'bio' => 'bail|required|min:10',
            ]);
            $chefSecteur->bio = nl2br(str_replace('<br />', PHP_EOL, $request->bio));
            $chefSecteur->save();

            Flashy::success("Votre biographie a été  mise à jour avec succès.");
            return redirect()->back();
        }else if(isset($request->allUp)) {
            $chefSecteur->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'nationalIdentity' => $request->nationalIdentity,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'dateOfBirth' => $request->dateOfBirth,
                'hireDate' => $request->hireDate,
            ]);

            Flashy::success("Les informations ont été mise à jour avec succès");
            return redirect()->back();
        }else{

            $chefSecteur->working = "on";
            $chefSecteur->save();

            Flashy::success("L'administrateur a été confirmé avec succèss et pourra bénéfié des privilèges accordés");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChefSecteurs $chefSecteur
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChefSecteurs $chefSecteur)
    {
        //
    }
}
