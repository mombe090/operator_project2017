<?php

namespace App\Http\Controllers\ProprietaireAuth;

use App\Proprietaire;
use function bcrypt;
use function compact;
use function dd;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;
use const null;
use function redirect;
use function url;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use function view;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/proprietaire/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('proprietaire.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:proprietaires',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Proprietaire
     */
    protected function create(array $data)
    {
        return Proprietaire::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('proprietaire.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('proprietaire');
    }

    public function showPasswordFormToProprietaire($id, $token){
        $proprietaire = Proprietaire::findOrFail($id);
        if ($proprietaire->verify != $token){
            Flashy::error('Ce lien n\'est pas valide veillez le verifier');
            return redirect(url('/proprietaire/login'));
        }
        return view('proprietaire.auth.passwords.add_password', compact('proprietaire'));
    }

    public function confirm(Request $request, $id){
        $proprietaire = Proprietaire::findOrFail($id);
        if ($proprietaire){
            if ($proprietaire->password!=null){
                Flashy::error("Ce proprietaire a déjà un mot de passe");
                return redirect()->back()->withInput();
            }else{
                $this->validate($request, [
                    'password' => 'bail|required|confirmed'
                ]);
                $proprietaire->verify = null;
                $proprietaire->working = 'on';
                $proprietaire->role = 'proprietaire';
                $proprietaire->password = bcrypt($request->password);

                $proprietaire->save();

                Auth::guard('proprietaire')->login($proprietaire);

                Flashy::success("Votre mot de passe à été enregistré avec succès. Veillez le retenir pour la prochaine fois.");
                return redirect()->route('proprietaire.home');
            }
        }

    }
}
