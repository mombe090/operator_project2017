<?php

namespace App\Http\Controllers;

use App\Notifications\CreateProprietairePassword;
use App\Terrain;
use function compact;
use function dd;
use function fmod;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MercurySeries\Flashy\Flashy;
use const null;
use function view;

class AdminGestionsTerrains extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexConfirm()
    {
        $terrains = DB::table('prefectures')
            ->join('communes', 'communes.prefecture_id', 'prefectures.id')
            ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
            ->join('proprietaires', 'proprietaires.id', '=', 'terrains.proprietaire_id')
            ->where('prefectures.id', Auth::user()->prefecture->id)
            ->where('terrains.verifiedByMaire', 'yes')
            ->where('terrains.verifiedByAdmin', 'yes')
            ->select('terrains.*', 'terrains.id as terrainID', 'quartiers.name as quartier', 'proprietaires.*')
            ->paginate(5);


        return view('gestions_des_administrateurs.admins.terrains.list_land_validate', compact('terrains'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terrains = DB::table('prefectures')
            ->join('communes', 'communes.prefecture_id', 'prefectures.id')
            ->join('quartiers', 'quartiers.commune_id', 'communes.id')
            ->join('secteurs', 'secteurs.quartier_id', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', 'secteurs.id')
            ->join('proprietaires', 'proprietaires.id', 'terrains.proprietaire_id')
            ->where('prefectures.id', Auth::user()->prefecture->id)
            ->where('terrains.verifiedByMaire', 'yes')
            ->where('terrains.verifiedByAdmin', 'no')
            ->select('terrains.*', 'terrains.id as terrainID', 'quartiers.name as quartier', 'proprietaires.*')
            ->paginate(5);

        return view('gestions_des_administrateurs.admins.terrains.list_land_not_validate', compact('terrains'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $terrain = Terrain::findOrFail($id);

        return view('gestions_des_administrateurs.admins.terrains.show_land', compact('terrain'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $terrain = Terrain::findOrFail($id);
        $dommaines = DB::table('dommaines')->select()->get();
        $usages = DB::table('usages')->select()->get();
        if ($terrain->verifiedByAdmin=='no' AND $terrain->verifiedByMaire=='yes' ){
            return view('gestions_des_administrateurs.admins.terrains.edit_land_verified_by_maire', compact('terrain', 'dommaines', 'usages'));
        }else if($terrain->verifiedByMaire=='yes' AND $terrain->verifiedByAdmin=='yes' ){
            return view('gestions_des_administrateurs.admins.terrains.edit_land_verified_by_admin', compact('terrain', 'dommaines', 'usages'));
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $terrain = Terrain::findOrFail($id);

        if (isset($request->allUp)){
            if (isset($request->old)){
                $terrain->update([
                    'forme' => $request->forme,
                    'longer' => $request->longer,
                    'larger' => $request->larger,
                    'dommaine' => $request->dommaine,
                    'usage' => $request->usage,
                    'build' => $request->build,
                    'codeCommune' => $request->codeCommune,

                ]);
            }else{
                $terrain->update([
                    'forme' => $request->forme,
                    'longer' => $request->longer,
                    'larger' => $request->larger,
                    'dommaine' => $request->dommaine,
                    'usage' => $request->usage,
                    'build' => $request->build,
                    'codeCommune' => $request->codeCommune,

                ]);
            }
            Flashy::success("Les modifications de ce terrains ont été effectué avec succès.");
            return redirect()->back();
        }

        $terrain->verifiedByAdmin = 'yes';

        $terrain->codeVille = Auth::user()->prefecture->region->name.'__'.Auth::user()->prefecture->nom.'_'.$terrain->codeQuartier;
        $terrain->save();

        if ($terrain->proprietaire->password==null){
            event(new Registered($user = $terrain->proprietaire));
            $user->notify(new CreateProprietairePassword());
        }

        Flashy::success("La confirmation de ce terrains est effectué avec succès.");
        return redirect(url('/admin/home'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $terrain = Terrain::findOrFail($id);
        if ($terrain->verifiedByAdmin=='yes' AND $terrain->verifiedByMaire=='yes'){
            Flashy::error("Vous ne pouvez pas supprimé une parcelle que le maire et vous avez confirmer.");
            return redirect()->back();
        }
        $terrain->delete();

        Flashy::error("Cette parcelle à été supprimer avec succès en tant que Directeur Général des dommaines et cadastre.");
        return redirect()->back();
    }
}
