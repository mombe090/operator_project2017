<?php

namespace App\Http\Controllers;

use App\Acheteurs;
use Illuminate\Http\Request;

class AcheteurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Acheteurs  $acheteurs
     * @return \Illuminate\Http\Response
     */
    public function show(Acheteurs $acheteurs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Acheteurs  $acheteurs
     * @return \Illuminate\Http\Response
     */
    public function edit(Acheteurs $acheteurs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Acheteurs  $acheteurs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Acheteurs $acheteurs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Acheteurs  $acheteurs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Acheteurs $acheteurs)
    {
        //
    }
}
