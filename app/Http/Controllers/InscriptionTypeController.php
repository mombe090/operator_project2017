<?php

namespace App\Http\Controllers;

use function compact;
use function dd;
use Illuminate\Http\Request;
use function redirect;
use function route;
use function url;
use function view;

class InscriptionTypeController extends Controller
{
        public function index(){
            return view('connexion.InscriptionType');
        }

        public function choose(Request $request){
            if (isset($request->role)){
                if ($request->role=='Root'){
                    return redirect(url('/root/register'));
                }else if($request->role=='SuperAdmin'){
                    return redirect(url('/superadmin/register'));
                }else if($request->role=='Admin'){
                    return redirect(url('/admin/register'));
                }else if($request->role=='Maire'){
                    return redirect(url('/maire/register'));
                }else if($request->role=='ChefQuartier'){
                    return redirect(url('/chefquartier/chooseCommuneForChefQuartier'));
                }else if($request->role=='ChefSecteur'){
                    return redirect(route('chefSecteurCommune'));
                }
                else if($request->role=='landOwner'){
                    return redirect(url('/proprietaire/register'));
                }
            }

            if(isset($request->commune) && !empty($request->commune)){

                $commune = Commune::findOrFail($request->communeId);
                return redirect(url('/chefquartier/register'), compact('commune'));
            }
        }
}
