<?php

namespace App\Http\Controllers;

use App\Acheteur;
use App\Notifications\CreateProprietairePassword;
use App\Notifications\TerrainAchatProprietaireExistant;
use App\Proprietaire;
use App\Terrain;
use function compact;
use function dd;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;
use const null;
use function redirect;
use function view;

class ProprietaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terrains = Terrain::where('proprietaire_id', Auth::user()->id)
            ->where('onSaleSend', '1')
            ->paginate(5);

        return view('gestions_des_administrateurs.chef_secteurs.ventes.list_terrain_ventes_en_confirmation_proprietaire', compact('terrains'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Proprietaire  $proprietaire
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id != Auth::user()->id){
            Flashy::error("Vous ne pouvez pas voir/ni modifier le profile d'un collègue chef de quartier");
            return redirect()->back();
        }
        $proprietaire = Proprietaire::findOrFail($id);

       return view('proprietaire.profiles.proprietaire_profile', compact('proprietaire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Proprietaire  $proprietaire
     * @return \Illuminate\Http\Response
     */
    public function edit(Proprietaire $proprietaire)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proprietaire  $proprietaire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Mise à jour du profile du propriétaire.
        $proprietaire = Proprietaire::findOrFail(Auth::user()->id);
        if(isset($request->profile)){
            $picture = $request->file('picture');
            $filename = $proprietaire->email.'.'.str_shuffle(str_random(10)).'.'.$picture->getClientOriginalExtension();
            Image::make($picture)->resize(300, 300)->save('storage/images/users/proprietaires/profile_img/'.$filename);
            $proprietaire->picture = 'storage/images/users/proprietaires/profile_img/'.$filename;
            $proprietaire->save();
            Flashy::success("Votre photo de profile à été mise à jour avect succès");
            return redirect()->back();
        } else  if (isset($request->isBio)){
            $this->validate($request, [
                'bio' => 'bail|required|min:10',
            ]);

            $proprietaire->bio = nl2br(str_replace('<br />', PHP_EOL, $request->bio));
            $proprietaire->save();

            Flashy::success("Votre biographie a été  mise à jour avec succès.");
            return redirect()->back();
        }else if(isset($request->allUp)) {

            $proprietaire->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'nationalIdentity' => $request->nationalIdentity,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'dateOfBirth' => $request->dateOfBirth,
            ]);

            Flashy::success("Les informations ont été mise à jour avec succès");
            return redirect()->back();
        }else if (isset($request->confirmVente)){
            $acheteur = Acheteur::where('terrain_id', $id)->first();

            $proprietaireExistant = Proprietaire::where('nationalIdentity', $acheteur->nationalIdentity)->first();

            $terrain = Terrain::findOrFail($id);

            if ($proprietaireExistant != null){
                $terrain->proprietaire_id = $proprietaireExistant->id;
                $terrain->onSale = '0';
                $terrain->onSaleSend = '0';
                $terrain->ValidSale = '1';
                $terrain->onTransaction = '1';
                $terrain->save();

                event(new Registered($user = $proprietaireExistant ));
                $user->notify(new TerrainAchatProprietaireExistant());

                Flashy::error('Vous venez de vendre votre terrain à '. $proprietaireExistant->lastName.' '. $proprietaireExistant->firstName);
                return redirect()->route('proprietaire.home');
            }
            $terrain = Terrain::findOrFail($id);

            $newProprietaire =  Proprietaire::create([
                'firstName' => $acheteur->firstName,
                'lastName' => $acheteur->lastName,
                'nationalIdentity' => $acheteur->nationalIdentity,
                'email' => $acheteur->email,
                'telephone' => $acheteur->telephone,
                'dateOfBirth' => $acheteur->dateOfBirth,
                'verify' => str_replace('/', '', Hash::make(str_shuffle(str_random(100)))),
                'working' => 'on'
            ]);

            $terrain->proprietaire_id = $newProprietaire->id;
            $terrain->onSale = '0';
            $terrain->onSaleSend = '0';
            $terrain->ValidSale = '1';
            $terrain->onTransaction = '1';
            $terrain->save();


            event(new Registered($user = $newProprietaire ));
            $user->notify(new CreateProprietairePassword());
            Flashy::error('Vous venez de vendre votre terrain à '. $newProprietaire->lastName.' '. $newProprietaire->firstName);
            return redirect()->route('proprietaire.home');
        }



        //Gestions des terrains du proprietaires

        $terrain = Terrain::findOrFail($id);
        if (isset($request->retire)){
            $terrain->onSale = '0';
            $terrain->save();

            Flashy::success("Votre terrain à été retiré de la vente.");
            return redirect()->back();
        }
        $terrain->onSale = '1';
        $terrain->save();

        Flashy::info("Votre terrain à été mise en vente. Une fois que quelqu'un se manifest un email vous sera envoyé");
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proprietaire  $proprietaire
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proprietaire $proprietaire)
    {
        //
    }
}
