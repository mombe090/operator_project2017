<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Prefecture;
use function dd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id != Auth::user()->id){
            Flashy::error("Vous ne pouvez pas voir/ni modifier le profile d'un collègue administrateur");
            return redirect()->back();
        }
        $admin = Admin::findOrFail($id);
        $prefectures = Prefecture::orderBy('nom')->get();
          return view('gestions_des_administrateurs.admins.profiles.admin_profile', compact('admin', 'prefectures'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($id != Auth::user()->id){
            Flashy::error("Vous ne pouvez pas voir/ni modifier le profile d'un collègue administrateur");
            return redirect()->back();
        }

        $admin = Admin::findOrFail($id);
        if ($request->picture !=null){
            $picture = $request->file('picture');
            $filename = $admin->email.'.'.str_shuffle(str_random(10)).'.'.$picture->getClientOriginalExtension();
            Image::make($picture)->resize(300, 300)->save('storage/images/users/superadmins/profile_img/'.$filename);
            $admin->picture = 'storage/images/users/superadmins/profile_img/'.$filename;
            $admin->save();
            Flashy::success("Votre photo de profile à été mise à jour avect succès");
            return redirect()->back();
        }
        else  if (isset($request->isBio)){
            $this->validate($request, [
                'bio' => 'bail|required|min:10',
            ]);
            $admin->bio = nl2br(str_replace('<br />', PHP_EOL, $request->bio));
            $admin->save();

            Flashy::success("Votre biographie a été  mise à jour avec succès.");
            return redirect()->back();
        }else if(isset($request->allUp)) {
            $admin->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'nationalIdentity' => $request->nationalIdentity,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'dateOfBirth' => $request->dateOfBirth,
                'hireDate' => $request->hireDate,
                'prefecture_id' => $request->prefecture_id,
            ]);

            Flashy::success("Les informations ont été mise à jour avec succès");
            return redirect()->back();
        }else{

            $admin->working = "on";
            $admin->save();

            Flashy::success("L'administrateur a été confirmé avec succèss et pourra bénéfié des privilèges accordés");
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }
}
