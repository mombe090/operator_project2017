<?php

namespace App\Http\Controllers\MaireAuth;

use App\Admin;
use App\ChefQuartiers;
use App\ChefSecteurs;
use App\Commune;
use App\Http\Controllers\Controller;
use App\Http\Requests\MaireRequest;
use App\Maire;
use App\Month;
use App\Notifications\MaireRegisterNotification;
use App\Root;
use App\Superadmin;
use DB;
use function dd;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use MercurySeries\Flashy\Flashy;
use function session;
use Validator;
use function compact;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/maire/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('maire.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:maires',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Maire
     */
    protected function create(array $data)
    {

        return Maire::create([
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'nationalIdentity' => $data['nationalIdentity'],
            'telephone' => $data['telephone'],
            'email' => $data['email'],
            'dateOfBirth' => $data['jour'].'/'.$data['mois'].'/'.$data['annee'],
            'hireDate' => $data['jourE'].'/'.$data['moisE'].'/'.$data['anneeE'],
            'working' => 'off',
            'picture' => '/storage/images/users/default.jpg',
            'verify' => str_replace('/', '', Hash::make(str_shuffle(str_random(100)))),
            'commune_id' => $data['commune_id'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $months = Month::all();
        $communes = DB::table('communes')->orderBy('name')->get();
        return view('maire.auth.register', compact('months', 'communes'));
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('maire');
    }

    public function register(MaireRequest $request)
    {
        //On s'assure que le mail ou l'ide
        $maireIdForCommune = Maire::where('commune_id', $request->input('commune_id'))->where('working', 'on')->first();
        if ($maireIdForCommune != null){
            Flashy::error('La commune a déjà un maire en fonction. Merci de vous addresser au Directeur du DO-CAD de votre préfecture.');
            session()->flash('emailConfirmationError', 'La commune a déjà un maire en fonction. Merci de vous addresser au Directeur du DO-CAD de votre préfecture.');
            return redirect()->back()->withInput();
        }

        $rootEmails = Root::where('email', $request->input('email'))->first();
        if ($rootEmails !=null){
            Flashy::error('L\'email en question est detenu par le root');
            return redirect(url('/maire/register'))->withInput();
        }

        $superadminEmails = Superadmin::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        if ($superadminEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale ou encore le telephone en question est detenu par un super administrateur ');
            return redirect(url('/maire/register'))->withInput();
        }

        $adminEmails = Admin::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        if ($adminEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale ou encore le telephone en question est detenu par un maire ');
            return redirect(url('/maire/register'))->withInput();
        }

        $chefDeQuartierEmails = ChefQuartiers::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        if ($chefDeQuartierEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale ou encore le telephone en question est detenu par un chef de quartier ');
            return redirect(url('/maire/register'))->withInput();
        }

        $chefSecteurEmails = ChefSecteurs::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        if ($chefSecteurEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale ou encore le telephone en question est detenu par un chef de secteur ');
            return redirect(url('/maire/register'))->withInput();
        }
        $request->all();

        event(new Registered($user = $this->create($request->all())));
        $user->notify(new MaireRegisterNotification());
        session()->flash('emailConfirmation', 'Votre compte a été crée avec succès. Confirmez votre email SVP');
        Flashy::success('Votre compte a été crée avec succès. Confirmez votre email SVP');
        return redirect('/maire/login');
    }


    public function confirm($id, $token){
        $user = Maire::where('id', $id)->where('verify', $token)->first();

        if ($user){
            $user->update(
                [
                    'verify' => null
                ]
            );
            //$this->guard()->login($user);
            Flashy::info("Une dernière chose faite-vous confirmez par l'administrateur de votre prefecture");
            session()->flash('emailConfirmationInfo', 'Une dernière chose faite-vous confirmez par l\'administrateur de votre prefecture.');
            return redirect('/maire/login');
        }else{
            Flashy::error('Ce lien ne semble pas valide. Merci de bien verifier authenticité de ce mail.');
            return redirect('/maire/login');
        }
    }
}
