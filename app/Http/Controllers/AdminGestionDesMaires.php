<?php

namespace App\Http\Controllers;

use App\Commune;
use App\Maire;
use App\Notifications\ConfirmationMaireNotification;
use App\Notifications\RejetMaireRegistrationNotification;
use function compact;
use function dd;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use function is;
use MercurySeries\Flashy\Flashy;
use function redirect;
use function view;

class AdminGestionDesMaires extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $communes = Commune::where('prefecture_id', Auth::user()->prefecture_id)->get();
        $maires = Maire::where('verify', null)->
        where('working', 'off')->get();


        return view('gestions_des_administrateurs.admins.listMaires', compact('communes', 'maires'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexConfirm()
    {
        $communes = Commune::where('prefecture_id', Auth::user()->prefecture_id)->get();
        $maires = Maire::where('verify', null)->
        where('working', 'on')->get();

        $confirm = true;

        return view('gestions_des_administrateurs.admins.listMaires', compact('communes', 'maires', 'confirm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if ($id != Auth::user()->commune_id) {

        }
        if (Auth::user()->prefecture_id == 1) {

        }

        $maire = Maire::findOrFail($id);
        return view('gestions_des_administrateurs.admins.profiles.index', compact('maire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $maire = Maire::findOrFail($id);
        $communes = Commune::where('prefecture_id', Auth::user()->prefecture_id)->orderBy('name')->get();

        return view('gestions_des_administrateurs.admins.edit_maire_from_admins', compact('maire', 'communes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $maire = Maire::findOrFail($id);
        if(isset($request->retirerAcces)){
            $maire->working = 'off';
            $maire->save();
            Flashy::error("L'access à la base de donnée à été retiré pour ce maire");
            return redirect()->back();
        }
        else if (isset($request->confirm)){
            $maire->working = "on";
            $maire->save();

            event(new Registered($user = $maire));
            $user->notify(new ConfirmationMaireNotification());
            Flashy::success("Le maire à été confirmé avec succès.");
            return redirect()->back();
        }else if(isset($request->profileUpdate)){
            $picture = $request->file('picture');
            $filename = $maire->email.'.'.$picture->getClientOriginalExtension();
            Image::make($picture)->resize(300, 300)->save('storage/images/users/maires/profile_img/'.$filename);
            $maire->picture = 'storage/images/users/maires/profile_img/'.$filename;
            $maire->save();
            Flashy::success("La photo du profile de cet maire à été mise à jour");
            return redirect()->back();
        }else if(is($request->trueUpdate)){
            $this->validate($request, [
                'firstName' => 'bail|required|max:255',
                'lastName' => 'bail|required|max:255',
                'email' => 'bail|required|email|max:255',
                'nationalIdentity' => 'bail|required|max:10',
                'telephone' => 'bail|required|max:12',
            ]);
            $maire->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'nationalIdentity' => $request->nationalIdentity,
                'telephone' => $request->telephone,
                'email' => $request->email,
                'dateOfBirth' => $request->dateOfBirth,
                'hireDate' => $request->hireDate,
                'prefecture_id' => $request->prefecture_id,
            ]);

            Flashy::success("La mise à jour du profile du maire à été mise à jour");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $maire = Maire::findOrFail($id);
        $maire->delete();

        event(new Registered($user = $maire));
        $user->notify(new RejetMaireRegistrationNotification());
        Flashy::error("Cette inscription a été suprimer avec succès");
        return redirect()->back();
    }

}


