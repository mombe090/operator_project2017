<?php

namespace App\Http\Controllers;

use App\Quartier;
use Auth;
use function compact;
use function dd;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;
use function view;

class MaireGestionDesQuartiers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quartiers = Quartier::where('commune_id', Auth::user()->commune->id)->paginate(5);

     return view('gestions_des_administrateurs.maires.quartiers.list_quartiers', compact('quartiers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('gestions_des_administrateurs.maires.quartiers.add_quartier');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'bail|required|min:3'
        ]);
        $quartiers = Quartier::all();
        $isUsed = false;
        $count = Quartier::where('commune_id', Auth::user()->commune->id)->count();


        foreach ($quartiers as $quartier){
            if ($quartier->commune->id==Auth::user()->commune->id){
                if ($quartier->name==$request->name){
                    $isUsed = true;
                }
            }
        }

        if ($isUsed==true){
            Flashy::error("Ce nom de quartier existe déjà dans votre commune.");
            return redirect()->back()->withInput();
        }
        Quartier::create([
          'code' => Auth::user()->commune->name.'__'.$request->name.'_'.($count+1),
           'name' =>$request->name,
            'commune_id' => Auth::user()->commune->id
       ]);
        Flashy::success("Le quartier à été ajouter dans votre commune avec succès");
        return redirect(url('/maire/home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
