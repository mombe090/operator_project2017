<?php

namespace App\Http\Controllers;

use App\Acheteur;
use App\Month;
use App\Proprietaire;
use App\Terrain;
use function dd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;
use function compact;
use function redirect;
use function session;

use function view;

class ChefSecteurGestionDesVentes extends Controller
{
    public function index(){

        $terrains = DB::table('secteurs')
            ->join('terrains', 'secteurs.id',  'terrains.secteur_id')
            ->join('proprietaires', 'proprietaires.id',  'terrains.proprietaire_id' )
            ->where('secteurs.id', Auth::user()->secteur->id)
            ->where('terrains.verifiedByChefQuartier', 'yes')
            ->where('terrains.onSale', '1')
            ->paginate(5);

        return view('gestions_des_administrateurs.chef_secteurs.ventes.list_terrain_ventes', compact('terrains'));
    }

    public function choice(){
        return view('gestions_des_administrateurs.chef_secteurs.acheteurs.had_or_not_land_befor');
    }
    public function create(Request $request){
        $months = Month::orderBy('id')->get();
        $type = $request->choice;

        $terrains = DB::table('secteurs')
            ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
            ->join('proprietaires', 'terrains.proprietaire_id', '=', 'proprietaires.id')
            ->join('chefsecteurs', 'chefsecteurs.secteur_id', '=', 'terrains.secteur_id')
            ->where('chefsecteurs.id', Auth::user()->id)
            ->where('terrains.verifiedByChefQuartier', 'yes')
            ->where('terrains.onSale', '1')
            ->select('terrains.id as terrainID', 'terrains.*', 'proprietaires.*', 'proprietaires.id as vendeur')
            ->paginate(5);

        return view('gestions_des_administrateurs.chef_secteurs.acheteurs.select_terrain_to_sel', compact('type', 'terrains'));

    }



    public function selectAcheteur(Request $request){
        $type = $request->type;
        $months = Month::orderBy('id')->get();
        $terrainID = $request->terrain;

        return view('gestions_des_administrateurs.chef_secteurs.acheteurs.add_acheteur', compact( 'months', 'type', 'terrainID'));
    }



    public function store(Request $request){

        if (isset($request->newAcheteur)){
            $this->validate($request, [
                'nationalIdentity' =>'bail|required|unique:proprietaires',
                'firstName' => 'bail|required|min:3',
                'lastName' => 'bail|required|min:3',
                'email' => 'bail|required|email|unique:proprietaires',
                'telephone' => 'bail|required|unique:proprietaires',
                'picture' => 'bail|required|image',
            ]);
            $dateOfBirth = $request->jour.'/'.$request->mois.'/'.$request->annee;
           $acheteur = Acheteur::create([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'nationalIdentity' => $request->nationalIdentity,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'dateOfBirth' => $dateOfBirth,
               'picture' => '/img/avatar5.png',
                'terrain_id' => session('terrainEnVente')

            ]);
            $picture = $request->file('picture');
            $filename = str_slug($acheteur->email, '_') . '_' . str_shuffle(str_random(10)) . '.' . $picture->getClientOriginalExtension();
            Image::make($picture)->resize(900, 750)->save('storage/images/users/acheteurs/' . $filename);
            $acheteur->picture = 'storage/images/users/acheteurs/' . $filename;
            $acheteur->save();
            session()->put('acheteur', $acheteur->id);

            Flashy::success('L\'acheteur à été ajouter avec succèss.');
            return redirect()->route('chefsecteur.gestion_des_temoins.create');
        }

        $nationalIdentityFound = Proprietaire::where('nationalIdentity', $request->identifiant)->first();
        $emailFound = Proprietaire::where('email', $request->identifiant)->first();
        $telephoneFound= Proprietaire::where('telephone', $request->identifiant)->first();


        if ($nationalIdentityFound==null AND $emailFound==null AND $telephoneFound==null){
            Flashy::error("Aucune correspondance n'a été trouvé dans la base de donnéé pour ce proprietaire");
            session()->flash('emailConfirmationError', "Aucune correspondance n'a été trouvé dans la base de donnéé pour ce proprietaire. S'il n'existe pas veillez l'inscrire d'abord.");
            return redirect()->back()->withInput();
        }
        if ($nationalIdentityFound!=null){
            $acheteurs = $nationalIdentityFound;
        }else if($emailFound!=null){
            $acheteurs = $emailFound;
        }else if($telephoneFound!=null){
            $acheteurs = $telephoneFound;
        }
        $acheteur = Acheteur::create([
            'firstName' => $acheteurs->firstName,
            'lastName' => $acheteurs->lastName,
            'nationalIdentity' => $acheteurs->nationalIdentity,
            'email' => $acheteurs->email,
            'picture' => $acheteurs->picture,
            'telephone' => $acheteurs->telephone,
            'dateOfBirth' => $acheteurs->dateOfBirth,
            'terrain_id' => session('terrainEnVente')

        ]);
        session()->put('acheteur', $acheteur->id);
        Flashy::info("L'acheteur à été rajouter avec succès.");
        session()->put('nbr', 1);
        return redirect()->route('chefsecteur.gestion_des_temoins.create');
    }

    public function update(Request $request, $id){

        $terrain = Terrain::findOrFail($id);
        if (isset($request->demandeConfirmation)){
            $type = $request->type;
            $terrain->onSaleSend = '1';
            $terrain->save();
            Flashy::info("La requête de validation de la vente à été envoyé au propriétaire.");
            session()->put('terrainEnVente', $terrain->id);
            return redirect()->route('chefsecteur.select_the_land', compact('type'));
        }else if(isset($request->retireConfirmation)){
            $terrain->onSaleSend = '0';
            $terrain->save();
          /*  $acheteur = Acheteur::where('terrain_id', $terrain->id)->first();
            $acheteur->delete();*/

            Flashy::info("La requête de validation de la vente à été annuler");
            return redirect()->route('chefsecteur.home');

        }
    }

    public function indexAttente(){
        $terrains = DB::table('secteurs')
            ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
            ->join('proprietaires', 'terrains.proprietaire_id', '=', 'proprietaires.id')
            ->join('chefsecteurs', 'chefsecteurs.secteur_id', '=', 'terrains.secteur_id')
            ->where('chefsecteurs.id', Auth::user()->id)
            ->where('terrains.verifiedByChefQuartier', 'yes')
            ->where('terrains.onSale', '1')
            ->where('terrains.onSaleSend', '1')
            ->where('terrains.ValidSale', '0')
            ->select('terrains.*', 'proprietaires.*', 'terrains.id as terrainID')
            ->paginate(5);

        return view('gestions_des_administrateurs.chef_secteurs.ventes.list_terrain_ventes_en_confirmation_proprietaire', compact('terrains'));
    }

    public function indexConfirme(){
 /*       $terrains = DB::table('secteurs')
            ->join('terrains', 'terrain.id', '=', 'terrains.secteur_id')
            ->join('proprietaires', 'terrains.proprietaire_id', '=', 'proprietaires.id')
            ->where('secteurs.id', Auth::user()->secteur->id)
            ->where('terrains.verifiedByChefQuartier', 'yes')
            ->where('terrains.onSale', '0')
            ->where('terrains.onSaleSend', '0')
            ->where('terrains.ValidSale', '1')
            ->paginate(5);*/


        $terrains = DB::table('secteurs')
            ->join('terrains', 'secteurs.id',  'terrains.secteur_id')
            ->join('proprietaires', 'proprietaires.id',  'terrains.proprietaire_id' )
            ->where('secteurs.id', Auth::user()->secteur->id)
            ->where('terrains.verifiedByChefQuartier', 'yes')
            ->where('terrains.onSale', '0')
            ->where('terrains.onSaleSend', '0')
            ->where('terrains.ValidSale', '1')
            ->paginate(5);

        return view('gestions_des_administrateurs.chef_secteurs.ventes.list_terrain_ventes_confirme_par_proprietaire', compact('terrains'));
    }
}
