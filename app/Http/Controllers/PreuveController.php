<?php

namespace App\Http\Controllers;

use App\Preuve;
use App\Terrain;
use function compact;
use function dd;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;
use function str_slug;
use function view;

class PreuveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $terrain = Terrain::findOrFail($request->terrain_id);
        /*
                $this->validate($request, [
                  'attestationAchat' => 'bail|required|file',
                   'planDeMasse' => 'bail|required|file',
                   'titreFoncier' => 'bail|required|file'
               ]);*/





        $attestationAchat = $request->file('attestationAchat');
        $filename = str_slug($terrain->codeSecteur, '_') . '_' . str_shuffle(str_random(10)) . '.' . $attestationAchat->getClientOriginalExtension();
        Image::make($attestationAchat)->resize(900, 750)->save('storage/terrains/attestations/' . $filename);

        $planDeMasse = $request->file('planDeMasse');
        $filename1 =  str_slug($terrain->codeSecteur, '_') . '_' . str_shuffle(str_random(10)) . '.' . $planDeMasse->getClientOriginalExtension();
        Image::make($planDeMasse)->resize(900, 750)->save('storage/terrains/planDeMasses/' . $filename1);

        $titreFoncier = $request->file('titreFoncier');
        $filename2 =  str_slug($terrain->codeSecteur, '_') . '_' . str_shuffle(str_random(10)) . '.' . $titreFoncier->getClientOriginalExtension();
        Image::make($titreFoncier)->resize(900, 750)->save('storage/terrains/titreFonciers/' . $filename2);


       $preuve =  Preuve::create([
            'attestationAchat' => '/storage/terrains/attestations/' . $filename,
            'planDeMasse' => '/storage/terrains/planDeMasses/' . $filename1,
            'titreFoncier' => '/storage/terrains/titreFonciers/' . $filename2,
            'terrain_id' => $request->terrain_id
        ]);

        Flashy::success("Les documents ont été ajoutés avec succès");
        return redirect()->route('chefsecteur.preuve_de_propriete.show', $preuve);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Preuve  $preuve
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $preuves = Preuve::findOrFail($id);



        return view('gestions_des_administrateurs.chef_secteurs.proprietaires.show_preuv', compact('preuves'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Preuve  $preuve
     * @return \Illuminate\Http\Response
     */
    public function edit(Preuve $preuve)
    {
        //
    }

    /**
     * Update the specified resource in /storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Preuve  $preuve
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Preuve $preuve)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Preuve  $preuve
     * @return \Illuminate\Http\Response
     */
    public function destroy(Preuve $preuve)
    {
        //
    }
}
