<?php

namespace App\Http\Controllers;

use App\Admin;
use App\ChefQuartiers;
use App\ChefSecteurs;
use App\Commune;
use App\Maire;
use App\Prefecture;
use App\Quartier;
use App\Region;
use App\Secteur;
use App\Superadmin;
use App\Terrain;
use Auth;
use function compact;
use function dd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;
use function nl2br;
use const null;
use const PHP_EOL;
use function redirect;
use function str_random;
use function str_replace_last;
use function str_shuffle;
use function view;

class SuperAdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('superadmin');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home(){

        $users[] = Auth::user();
        $users[] = Auth::guard()->user();
        $users[] = Auth::guard('superadmin')->user();

        $prefectures = DB::table('regions')
            ->join('prefectures', 'prefectures.region_id', '=', 'regions.id')
            ->where('regions.id', Auth::user()->region->id)
            ->count();

        $admins = DB::table('regions')
            ->join('prefectures', 'prefectures.region_id', '=', 'regions.id')
            ->join('admins', 'admins.prefecture_id', '=', 'prefectures.id')
            ->where('regions.id', Auth::user()->region->id)
            ->get();

        $communes = DB::table('regions')
            ->join('prefectures', 'prefectures.region_id', '=', 'regions.id')
            ->join('communes', 'communes.prefecture_id', '=', 'prefectures.id')
            ->where('regions.id', Auth::user()->region->id)
            ->count();

        $maires = DB::table('regions')
            ->join('prefectures', 'prefectures.region_id', '=', 'regions.id')
            ->join('communes', 'communes.prefecture_id', '=', 'prefectures.id')
            ->join('maires', 'maires.commune_id', '=', 'communes.id')
            ->where('regions.id', Auth::user()->region->id)
            ->get();

        $quartiers = DB::table('regions')
            ->join('prefectures', 'prefectures.region_id', '=', 'regions.id')
            ->join('communes', 'communes.prefecture_id', '=', 'prefectures.id')
            ->join('quartiers', 'quartiers.commune_id', '=', 'communes.id')
            ->where('regions.id', Auth::user()->region->id)
            ->count();

        $chefQuartiers = DB::table('regions')
            ->join('prefectures', 'prefectures.region_id', '=', 'regions.id')
            ->join('communes', 'communes.prefecture_id', '=', 'prefectures.id')
            ->join('quartiers', 'quartiers.commune_id', '=', 'communes.id')
            ->join('chefquartiers', 'chefquartiers.quartier_id', '=', 'quartiers.id')
            ->where('regions.id', Auth::user()->region->id)
            ->get();


        $secteurs = DB::table('regions')
            ->join('prefectures', 'prefectures.region_id', '=', 'regions.id')
            ->join('communes', 'communes.prefecture_id', '=', 'prefectures.id')
            ->join('quartiers', 'quartiers.commune_id', '=', 'communes.id')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->where('regions.id', Auth::user()->region->id)
            ->count();

        $chefSecteurs = DB::table('regions')
            ->join('prefectures', 'prefectures.region_id', '=', 'regions.id')
            ->join('communes', 'communes.prefecture_id', '=', 'prefectures.id')
            ->join('quartiers', 'quartiers.commune_id', '=', 'communes.id')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('chefsecteurs', 'chefsecteurs.secteur_id', '=', 'secteurs.id')
            ->where('regions.id', Auth::user()->region->id)
            ->get();

        $parcelles= DB::table('regions')
            ->join('prefectures', 'prefectures.region_id', '=', 'regions.id')
            ->join('communes', 'communes.prefecture_id', '=', 'prefectures.id')
            ->join('quartiers', 'quartiers.commune_id', '=', 'communes.id')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
            ->where('regions.id', Auth::user()->region->id)
            ->get();

        return view('superadmin.home', compact( 'region', 'admins', 'prefectures', 'communes', 'maires', 'quartiers', 'chefQuartiers', 'secteurs', 'chefSecteurs', 'parcelles'
        ));

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id != Auth::user()->id){
            Flashy::error("Vous ne pouvez pas voir/ni modifier le profile d'un collègue super administrateur");
            return redirect()->back();
        }
        $superAdmin = Superadmin::findOrFail($id);
        $regions = Region::orderBy('name')->get();
        return view('gestions_des_administrateurs.super_admins.profiles.super_admin_profile', compact('superAdmin', 'regions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($id != Auth::user()->id){
            Flashy::error("Vous ne pouvez pas voir/ni modifier le profile d'un collègue super administrateur");
            return redirect()->back();
        }

        $superAdmin = Superadmin::findOrFail($id);
        if ($request->picture !=null){
            $picture = $request->file('picture');
            $filename = $superAdmin->email.'.'.str_shuffle(str_random(10)).'.'.$picture->getClientOriginalExtension();
            Image::make($picture)->resize(300, 300)->save('storage/images/users/superadmins/profile_img/'.$filename);
            $superAdmin->picture = 'storage/images/users/superadmins/profile_img/'.$filename;
            $superAdmin->save();
            Flashy::success("Votre photo de profile à été mise à jour avect succès");
            return redirect()->back();
        }
        else  if (isset($request->isBio)){
            $this->validate($request, [
                'bio' => 'bail|required|min:32',
            ]);
            $superAdmin->bio = nl2br(str_replace('<br />', PHP_EOL, $request->bio));
            $superAdmin->save();

            Flashy::success("Votre biographie a été  mise à jour avec succès.");
            return redirect()->back();
        }else if(isset($request->allUp)) {
            $superAdmin->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'nationalIdentity' => $request->nationalIdentity,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'dateOfBirth' => $request->dateOfBirth,
                'hireDate' => $request->hireDate,
                'region_id' => $request->region_id,
            ]);

            Flashy::success("Les informations ont été mise à jour avec succès");
            return redirect()->back();
        }else{

            $superAdmin->working = "on";
            $superAdmin->save();

            Flashy::success("L'administrateur a été confirmé avec succèss et pourra bénéfié des privilèges accordés");
            return redirect()->back();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
