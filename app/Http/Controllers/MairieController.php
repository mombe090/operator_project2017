<?php

namespace App\Http\Controllers;

use App\Mairie;
use Illuminate\Http\Request;

class MairieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mairie  $mairie
     * @return \Illuminate\Http\Response
     */
    public function show(Mairie $mairie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mairie  $mairie
     * @return \Illuminate\Http\Response
     */
    public function edit(Mairie $mairie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mairie  $mairie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mairie $mairie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mairie  $mairie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mairie $mairie)
    {
        //
    }
}
