<?php

namespace App\Http\Controllers;

use App\Mail\WriteUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MercurySeries\Flashy\Flashy;

class FeedBack extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'name' => 'bail|required|min:3' ,
            'email' => 'bail|email|required',
            'msg' => 'bail|required|min:20'
        ]);

      $mail =  DB::table('mails')->insert([
           'name' => $request->name,
           'email' => $request->email,
           'message' => $request->msg,
        ]);
      $mailable = new WriteUs($request->name, $request->email, $request->msg);
        \Mail::to('yayamombeya090@gmail.com')->send($mailable);
        session()->flash('success', 'Votre message à été envoyé à l\'administrateur merci de nous quand vous voulez.');
        Flashy::success('Merci de nous avoir écrit, on vous repondre dans le plus bref delai');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
