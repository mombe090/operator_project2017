<?php

namespace App\Http\Controllers\ChefquartierAuth;

use App\Chefquartier;
use App\Http\Controllers\Controller;
use function array_merge;
use function dd;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hesto\MultiAuth\Traits\LogsoutGuard;
use MercurySeries\Flashy\Flashy;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, LogsoutGuard {
        LogsoutGuard::logout insteadof AuthenticatesUsers;
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/chefquartier/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('chefquartier.guest', ['except' => 'logout']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('chefquartier.auth.login');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('chefquartier');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function credentials(Request $request)
    {
        return array_merge(
            $request->only($this->username(), 'password'),
            ['verify' => null],
            ['working' => 'on']
        );

    }
    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        Flashy::info('Vous êtes connecté avec succès en tant que chef de quartier. ');
        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $sendEmail = Chefquartier::where('email', $request->email)->first();
        $sendEmailNonVerifier = Chefquartier::where('verify', null)->first();

        if ($sendEmail==null){
            $errors =  [$request->email  => $request->email. " n'existe pas dans notre base de donnée. Veillez vous inscrire"];
        }else if($sendEmailNonVerifier){
            $errors =  [$request->email  => $request->email. "existe bien dans notre base de donnée mais pas encore confirmé. Veillez contacter le chef de quartier"];
        }else{
            $errors =  [$request->email  => $request->email. " existe bien dans notre base de donnée mais vous ne l'avez pas encore confirmer. Connecter sur votre compte email on vous a envoyé un email de confirmation"];
        }

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }
}
