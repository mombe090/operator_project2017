<?php

namespace App\Http\Controllers\ChefquartierAuth;

use App\Admin;
use App\Chefquartier;
use App\Chefsecteur;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChefQuartierRequest;
use App\Maire;
use App\Month;
use App\Notifications\ChefDeQuartierRegisterNotification;
use App\Quartier;
use App\Root;
use App\Superadmin;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use MercurySeries\Flashy\Flashy;
use Validator;
use function compact;
use function redirect;
use function session;
use function url;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any addi, $request ->quartier_idtional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/chefquartier/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('chefquartier.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:chefquartiers',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     *
     * @return \App\Chefquartier
     */
    protected function create(array $data)
    {
        return Chefquartier::create([
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'nationalIdentity' => $data['nationalIdentity'],
            'telephone' => $data['telephone'],
            'email' => $data['email'],
            'dateOfBirth' => $data['jour'].'/'.$data['mois'].'/'.$data['annee'],
            'hireDate' => $data['jourE'].'/'.$data['moisE'].'/'.$data['anneeE'],
            'working' => 'off',
            'picture' => '/storage/images/users/default.jpg',
            'verify' => str_replace('/', '', Hash::make(str_shuffle(str_random(100)))),
            'quartier_id' => $data['quartier_id'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function afficherRegistrationForm(Request $request){
        if (empty($request->all())){
           return redirect(url('chefquartier/chooseCommuneForChefQuartier'));
        }
        $months = Month::all();
        $quartiers = Quartier::where('commune_id', $request->commune)->get();


        return view('chefquartier.auth.register', compact('months', 'quartiers'));
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('chefquartier');
    }

    public function register(ChefQuartierRequest $request)
    {
        //On s'assure que le mail ou l'ide

        $quartierIdForChefQuartier = Chefquartier::where('quartier_id', $request->input('quartier_id'))->where('working', 'on')->first();
        if ($quartierIdForChefQuartier != null){
            Flashy::error('Le quartier a déjà un chef de quartier en fonction. Merci de vous addresser au maire de la commune.');
            session()->flash('emailConfirmationError', 'Le quartier a déjà un chef de quartier en fonction. Merci de vous addresser au maire de la commune.');
            return redirect()->back()->withInput();
        }

        $rootEmails = Root::where('email', $request->input('email'))->first();
        if ($rootEmails !=null){
            Flashy::error('L\'email en question est detenu par le root');
            session()->flash('emailConfirmationError', 'L\'email en question est detenu par le root');
            return redirect()->back()->withInput();
        }

        $superadminEmails = Superadmin::where('email', $request->input('email'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        if ($superadminEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale en question est detenu par un super administrateur ');
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale en question est detenu par un super administrateur ');
            return redirect()->back()->withInput();
        }

        $adminEmails = Admin::where('email', $request->input('email'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        if ($adminEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale en question est detenu par un administrateur ');
            session()->flash('emailConfirmationError','L\'email ou le numero d\'identité nationale en question est detenu par un administrateur ');
            return redirect()->back()->withInput();
        }

        $maireEmails = Maire::where('email', $request->input('email'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        if ($maireEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale en question est detenu par un maire ');
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale en question est detenu par un maire ');
            return redirect()->back()->withInput();
        }

        $chefSecteurEmails = Chefsecteur::where('email', $request->input('email'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        if ($chefSecteurEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale en question est detenu par un chef de secteur ');
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale en question est detenu par un chef de secteur ');
            return redirect()->back()->withInput();
        }
        $request->all();

        event(new Registered($user = $this->create($request->all())));
        $user->notify(new ChefDeQuartierRegisterNotification());
        Flashy::success('Votre compte a été crée avec succès. Confirmez votre email SVP');
        session()->flash('emailConfirmation', 'Votre compte a été crée avec succès. Confirmez votre email SVP');
        return redirect('/chefquartier/login');
    }


    public function confirm($id, $token){
        $user = Chefquartier::where('id', $id)->where('verify', $token)->first();

        if ($user){
            $user->update(
                [
                    'verify' => null
                ]
            );
            //$this->guard()->login($user);
            Flashy::info("Une dernière chose faite-vous confirmez par le chef de quartier");
            return redirect('/chefquartier/login');
        }else{
            Flashy::error('Ce lien ne semble pas valide ou à déjà été confirmé. Merci de bien verifier authenticité de ce mail.');
            return redirect('/chefquartier/login');
        }
    }

}
