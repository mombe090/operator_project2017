<?php

namespace App\Http\Controllers;

use App\Month;
use App\Proprietaire;
use App\Terrain;
use Auth;
use function dd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;
use const null;
use function compact;
use function redirect;
use function session;
use function view;

class ChefSecteurGestionDesProprietairesTerriens extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terrains = Terrain::where('verifiedByChefQuartier', 'yes')->where('secteur_id', Auth::user()->secteur->id)->paginate(5);

        return view('gestions_des_administrateurs.chef_secteurs.proprietaires.index', compact('terrains'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $months = Month::orderBy('id')->get();
        $terrains = Terrain::orderBy('id', 'desc')->where('secteur_id', Auth::user()->secteur->id)->get();


        return view('gestions_des_administrateurs.chef_secteurs.proprietaires.add_proprietaire', compact('months', 'terrains'));
    }

    public function createExiste()
    {

        return view('gestions_des_administrateurs.chef_secteurs.proprietaires.add_proprietaire_existant');
    }
    public function searchProprietaire(Request $request)
    {
        $nationalIdentityFound = Proprietaire::where('nationalIdentity', $request->identifiant)->first();
        $emailFound = Proprietaire::where('email', $request->identifiant)->first();
        $telephoneFound= Proprietaire::where('telephone', $request->identifiant)->first();

        if ($nationalIdentityFound==null AND $emailFound==null AND $telephoneFound==null){
            Flashy::error("Aucune correspondance n'a été trouvé dans la base de donnéé");
            return redirect()->back()->withInput();
        }else{
            if ($nationalIdentityFound!=null){
                $request->session()->forget('idProprietaire');
                session()->put('idProprietaire', $nationalIdentityFound->id);
                return redirect()->route('chefsecteur.add_terrain_proprietaire');
            }else if($emailFound!=null){
                $request->session()->forget('idProprietaire');
                session()->put('idProprietaire', $emailFound->id);
                return redirect()->route('chefsecteur.add_terrain_proprietaire');
            }else if($telephoneFound!=null){
                $request->session()->forget('idProprietaire');
                session()->put('idProprietaire', $telephoneFound->id);
                return redirect()->route('chefsecteur.add_terrain_proprietaire');
            }

        }

    }

    public function addTerrain()
    {
        $terrains = Terrain::orderBy('id', 'desc')->where('secteur_id', Auth::user()->secteur->id)->get();
        return view('gestions_des_administrateurs.chef_secteurs.proprietaires.add_terrain_du_proprietaire', compact('terrains'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return $this
     */
    public function addPreuve(Request $request){
        $exist = Proprietaire::where('terrain_id', $request->terrain_id)->first();


        if ($exist!=null){
            Flashy::error('Ce terrain est detenu par '. $exist->firstName.' '.$exist->lastName.' contactez-le pour clarifier les choses : '.$exist->telephone);

            session()->flash('emailConfirmationError', 'Ce terrain est detenu par '. $exist->firstName.' '.$exist->lastName.' contactez-le pour clarifier les choses : '.$exist->telephone);
            return redirect()->back()->withInput();
        }
        Flashy::success('Le propriétaire à été ajouter avec succèss. Veillez rajoutez les preuves scanner au format pdf, jpg ou png');

        return redirect()->route('chefsecteur.add_preuve_proprietaire');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nationalIdentity' =>'bail|required|unique:proprietaires',
            'firstName' => 'bail|required|min:3',
            'lastName' => 'bail|required|min:3',
            'email' => 'bail|required|email|unique:proprietaires',
            'telephone' => 'bail|required|unique:proprietaires',
            'picture' => 'bail|required|image',

        ]);

            $dateOfBirth = $request->jour.'/'.$request->mois.'/'.$request->annee;
            $proprietaire = Proprietaire::create([
                'nationalIdentity' => $request->nationalIdentity,
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'dateOfBirth' => $dateOfBirth,
                'verify' => str_replace('/', '', Hash::make(str_shuffle(str_random(100))))
            ]);

        $picture = $request->file('picture');
        $filename = str_slug($proprietaire->email, '_') . '_' . str_shuffle(str_random(10)) . '.' . $picture->getClientOriginalExtension();
        Image::make($picture)->resize(900, 750)->save('storage/images/users/proprietaires/' . $filename);
        $proprietaire->picture = 'storage/images/users/proprietaires/' . $filename;
        $proprietaire->save();

        Flashy::success('Le propriétaire à été ajouter avec succèss.');
         return redirect()->route('chefsecteur.gestion_chefs_secteur_terrains.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
