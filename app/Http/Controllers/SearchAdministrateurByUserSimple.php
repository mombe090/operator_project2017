<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Maire;
use App\Superadmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchAdministrateurByUserSimple extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       dd('fdsfdqfq');
    }

    public function liste(Request $request)
    {
        if ($request->admin == 'superadmins') {
            if (isset($request->region_id) AND $request->region_id != '') {
                $resultats = Superadmin::where('region_id', $request->region_id)->where('working', 'on')->get();
                $user = 'superadmins';
                return view('search.resultSearch', compact('resultats', 'user'));
            } else {
                $resultats = Superadmin::where('working', 'on')->get();

                $user = 'superadmins';
                return view('search.resultSearch', compact('resultats', 'user'));
            }
        } elseif ($request->admin == 'admins') {
            if (isset($request->region_id) AND $request->region_id != '') {
                if ($request->prefecture_id == '') {
                    $resultats = DB::table('regions')
                        ->join('prefectures', 'prefectures.region_id', 'regions.id')
                        ->join('admins', 'admins.prefecture_id', 'prefectures.id')
                        ->where('regions.id', $request->region_id)
                        ->get();

                    $user = 'admins';
                    return view('search.resultSearch', compact('resultats', 'user'));
                } else {
                    $resultats = DB::table('prefectures', 'prefectures.region_id', 'regions.id')
                        ->join('admins', 'admins.prefecture_id', 'prefectures.id')
                        ->where('admins.prefecture_id', $request->prefecture_id)
                        ->get();

                    $user = 'admins';
                    return view('search.resultSearch', compact('resultats', 'user'));
                }

            }
        } elseif ($request->admin == 'maires') {
            if ($request->region_id == '' AND $request->prefecture_id=='' AND $request->commune_id == '') {
                $resultats = Maire::where('working', 'on')->orderBy('id') ->get();

                $user = 'maires';

                return view('search.resultSearch', compact('resultats', 'user'));
            } else if($request->region_id != '' AND $request->prefecture_id=='' AND $request->commune_id == ''){
                $resultats = DB::table('regions')
                    ->join('prefectures', 'prefectures.region_id', 'regions.id')
                    ->join('communes', 'communes.prefecture_id', 'prefectures.id')
                    ->join('maires', 'maires.commune_id', 'communes.id')
                    ->where('regions.id', $request->region_id)
                    ->where('maires.working', 'on')
                    ->get();
                $user = 'maires';
                $noEloquent = true;
                return view('search.resultSearch', compact('resultats', 'user', 'noEloquent'));
            }else if($request->region_id != '' AND $request->prefecture_id!='' AND $request->commune_id == ''){
                $resultats = DB::table('regions')
                    ->join('prefectures', 'prefectures.region_id', 'regions.id')
                    ->join('communes', 'communes.prefecture_id', 'prefectures.id')
                    ->join('maires', 'maires.commune_id', 'communes.id')
                    ->where('regions.id', $request->region_id)
                    ->where('prefectures.id', $request->prefecture_id)
                    ->where('maires.working', 'on')
                    ->get();
                dd($resultats);
                $user = 'maires';
                $noEloquent = true;
                return view('search.resultSearch', compact('resultats', 'user', 'noEloquent'));
            }
        }


        elseif ($request->admin == 'Chefquartiers') {
            dd('Chefquartiers');
        } elseif ($request->admin == 'Chefsecteurs') {
            dd('Chefsecteurs');
        }
    }

    public function terrainCherche(Request $request){
      if (isset($request->name)){
          $terrains = DB::table('regions')
              ->join('prefectures', 'prefectures.region_id', 'regions.id')
              ->join('communes', 'communes.prefecture_id', 'prefectures.id')
              ->join('quartiers', 'quartiers.commune_id', 'communes.id')
              ->join('secteurs', 'secteurs.quartier_id', 'quartiers.id')
              ->join('terrains', 'terrains.secteur_id', 'secteurs.id')
              ->join('proprietaires', 'proprietaires.id', 'terrains.proprietaire_id')
              ->where('regions.name', 'like', '%'.$request->name.'%')
              ->orWhere('prefectures.nom', 'like', '%'.$request->name.'%')
              ->orWhere('communes.name', 'like', '%'.$request->name.'%')
              ->orWhere('secteurs.name', 'like', '%'.$request->name.'%')
              ->where('terrains.onSale', '1')
              ->select('quartiers.name as quartier', 'terrains.*', 'prefectures.*', 'proprietaires.*')
              ->limit(5)
              ->get();

         return view('vendor.adminlte.layouts.landing', compact('terrains'));
      }


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
