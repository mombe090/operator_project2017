<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Chefquartier;
use App\Chefsecteur;
use App\Commune;
use App\Maire;
use App\Notifications\ConfirmationSuperAdminNotification;
use App\Prefecture;
use App\Quartier;
use App\Region;
use App\Secteur;
use App\Superadmin;
use App\Terrain;
use DB;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;
use const null;
use const true;
use function compact;
use function dd;
use function redirect;
use function url;
use function view;


class RootController extends Controller
{

    /**
     * RootController constructor.
     */
    public function __construct()
    {
        $this->middleware('root');
    }

    public function home(){

        /*  $users[] = Auth::user();
          $users[] = Auth::guard()->user();
          $users[] = Auth::guard('root')->user();*/


        $superAdmins = Superadmin::where('verify', null)->get();


        $Admins = Admin::where('verify', null)->get();

        $Maires = Maire::where('verify', null)->get();


        $ChefQuartiers = Chefquartier::where('verify', null)->get();

        $ChefSecteurs= Chefsecteur::where('verify', null)->get();

        $Proprietaires= Terrain::where('verifiedByAdmin', 'yes')->get();

        $regions = Region::count();

        $prefectures = Prefecture::count();

        $communes = Commune::count();

        $quartiers = Quartier::count();

        $secteurs = Secteur::count();

        $terrains = Terrain::count();

        return view('root.home', compact('superAdmins', 'Admins', 'AdminsNonVerifier', 'Maires', 'ChefSecteurs', 'ChefQuartiers', 'ChefQuartiersNonVerifies',  'Proprietaires', 'regions', 'prefectures', 'communes', 'quartiers', 'secteurs', 'terrains', 'superAdminsNonVerifies', 'MairesNonVerifies', 'ChefSecteursNonVerifies'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function confirmationSuperAmins(){

        $superAdmins = Superadmin::where('working', 'off')
            ->where('verify', null)
            ->orderBy('id')
            ->paginate(15);

        return view('root.gestions.listSuperAdmins', compact('superAdmins'));
    }

    // Affiche les compte confirmés
    public function SuperAminsConfirmes(){

        $superAdmins = Superadmin::where('working', 'on')
            ->where('verify', null)
            ->orderBy('id')
            ->paginate(15);
        $confirm = true;

        return view('root.gestions.listSuperAdmins', compact('superAdmins', 'confirm'));
    }

    //Method chargé d'afficher le profile du super admin
    public function showSuperAdminsInformations($id){
        $superAdmin = Superadmin::findOrFail($id);

        $prefectureNumbers = DB::table('prefectures')
            ->join('regions', 'prefectures.region_id', '=', 'regions.id')
            ->join('superadmins', 'superadmins.region_id', '=', 'regions.id')
            ->join('admins', 'admins.prefecture_id', '=', 'prefectures.id')
            ->count();


        return view('superadmin.gestions.profiles.index', compact('superAdmin', 'prefectureNumbers'));
    }

    //Mise à jour des informations du super administrateur


    public function updateSuperAdminsInformations(Request $request ,$id){
        $superAdmin = Superadmin::findOrFail($id);
        $region = Region::findOrFail($request->region_id);

        dd($request->all());

        Flashy::info("Les informations ont été les mise à jour.");
        // return redirect(url('/root/edit/profile/superadmins/', $id));
    }

    //Modifie les informations du super administrateur

    public function editSuperAdminsInformations($id){
        $superAdmin = Superadmin::findOrFail($id);
        $regions = Region::orderBy('name')->get();
        return view('root.gestions.edit_superadmins', compact('superAdmin', 'regions'));
    }

    //Suppression d'un super administrateurs
    public function deleteSuperAdminsInformations($id){

        $superAdmin = Superadmin::findOrFail($id);
        $superAdmin->delete();

        Flashy::error("La suppression du super administrateur est effectué avec succès");
        return redirect()->back();
    }


    //Methode chargé de confirmer un super administrateur
    public function confirmSuperAdminsInformations($id){
        $superAdmin = Superadmin::findOrFail($id);

        $superAdmin->working = 'on';
        $superAdmin->save();

        event(new Registered($user = $superAdmin));
        $user->notify(new ConfirmationSuperAdminNotification());

        Flashy::success("Confirmation bien effectuées");
        return redirect(url("/root/home/supadmins"));
    }
}
