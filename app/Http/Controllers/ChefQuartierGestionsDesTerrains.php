<?php

namespace App\Http\Controllers;

use App\Terrain;
use Auth;
use function compact;
use function count;
use function dd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MercurySeries\Flashy\Flashy;
use function redirect;
use function url;
use function var_dump;
use function view;

class ChefQuartierGestionsDesTerrains extends Controller
{
    /**
     * ChefQuartierGestionsDesTerrains constructor.
     */
    public function __construct()
    {
        $this->middleware('chefquartier');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terrains = DB::table('quartiers')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
            ->join('chefquartiers', 'chefquartiers.quartier_id', '=', 'quartiers.id')
            ->join('proprietaires', 'proprietaires.id', '=', 'terrains.proprietaire_id')
            ->where('quartiers.id', Auth::user()->quartier->id)
            ->where('terrains.verifiedByChefQuartier', 'no')
            ->paginate(3);

    return view('gestions_des_administrateurs.chef_quarttiers.terrains.list_land_not_validate', compact('terrains'));
    }
/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexConfirm()
    {
        $terrains = DB::table('quartiers')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
            ->join('chefquartiers', 'chefquartiers.quartier_id', '=', 'quartiers.id')
            ->join('proprietaires', 'proprietaires.id', '=', 'terrains.proprietaire_id')
            ->where('quartiers.id', Auth::user()->quartier->id)
            ->where('terrains.verifiedByChefQuartier', 'yes')
            ->paginate(3);

        return view('gestions_des_administrateurs.chef_quarttiers.terrains.list_land_validate', compact('terrains'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $terrain = Terrain::where('verifiedByChefQuartier', 'no')->where('id', $id)->first();


        return view('gestions_des_administrateurs.chef_secteurs.show_land', compact('terrain'));
    }
    public function showTwo($id)
    {

        $terrain = Terrain::where('verifiedByChefQuartier', 'yes')->where('id', $id)->first();


        return view('gestions_des_administrateurs.chef_secteurs.show_land', compact('terrain'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $terrain = Terrain::findOrFail($id);
        $dommaines = DB::table('dommaines')->select()->get();
        $usages = DB::table('usages')->select()->get();
        if ($terrain->verifiedByChefQuartier=='no'){
            return view('gestions_des_administrateurs.chef_secteurs.edit_lang_not_verified_by_chef_quartier', compact('terrain', 'dommaines', 'usages'));
        }else{
            return view('gestions_des_administrateurs.chef_secteurs.edit_lang_not_verified_by_chef_quartier', compact('terrain', 'dommaines', 'usages'));
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $terrain = Terrain::findOrFail($id);

        if (isset($request->allUp)){
            $terrain->update([
                'forme' => $request->forme,
                'longer' => $request->longer,
                'larger' => $request->larger,
                'dommaine' => $request->dommaine,
                'usage' => $request->usage,
                'build' => $request->build,
                'codeSecteur' => $request->codeSecteur,

            ]);

            Flashy::success("Les modifications de ce terrains ont été effectué avec succès.");
            return redirect(route('chefquartier.home'));
        }
        $terrain->verifiedByChefQuartier = 'yes';
        $terrain->codeQuartier = Auth::user()->quartier->commune->name.'__'.Auth::user()->quartier->name.'_'.$terrain->codeSecteur;
        $terrain->save();

        Flashy::success("La confirmation de ce terrains est effectué avec succès.");
        return redirect(url('/chefquartier/home'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {



        Flashy::error("Cette parcelle ne peut pas être supprimer par vous consulter le maire de la commune");
        return redirect()->back();
    }
}
