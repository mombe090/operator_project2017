<?php

namespace App\Http\Controllers;

use App\Proprietaire;
use App\Terrain;
use Auth;
use function compact;
use DB;
use function dd;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;
use const null;
use function redirect;
use function route;
use function session;
use const true;
use function url;
use function view;


class ChefSecteurGestionTerrain extends Controller
{
    public function __construct()
    {
        return $this->middleware('chefsecteur');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terrains = Terrain::where('secteur_id', Auth::user()->secteur_id)->where('verifiedByChefQuartier', 'no')->paginate(5);


        return view('gestions_des_administrateurs.chef_secteurs.list_land_not_validate_by_chef_quartier', compact('terrains'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexConfirm()
    {

        $terrains = Terrain::where('secteur_id', Auth::user()->secteur_id)->where('verifiedByChefQuartier', 'yes')->paginate(5);


        return view('gestions_des_administrateurs.chef_secteurs.list_land_validate_by_chef_quartier', compact('terrains'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dommaines = DB::table('dommaines')->select()->get();
        $usages = DB::table('usages')->select()->get();

        return view('gestions_des_administrateurs.chef_secteurs.add_land', compact('dommaines', 'usages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $terrain = Terrain::where('codeSecteur', Auth::user()->secteur->quartier->name.'__'.Auth::user()->secteur->name.'_'.$request->codeSecteur)->first();
        if ($terrain!=null){
            session()->flash('emailConfirmationError', 'Le numero de parcelle est déjà pris. Choisir un autre.');
            return redirect()->back()->withInput();
        }

        $this->validate($request, [
            'longer' => 'bail|required|numeric|min:5',
            'larger' => 'bail|required|numeric|min:5',
            'larger' => 'bail|required|numeric|min:5',
            'codeSecteur' => 'bail|required|min:6',
            'identifiant' => 'bail|required',
            'attestationAchat' => 'bail|required|image',
            'planDeMasse' => 'image',
            'titreFoncier' => '|image'

        ]);
        $nationalIdentityFound = Proprietaire::where('nationalIdentity', $request->identifiant)->first();
        $emailFound = Proprietaire::where('email', $request->identifiant)->first();
        $telephoneFound= Proprietaire::where('telephone', $request->identifiant)->first();

        if ($nationalIdentityFound==null AND $emailFound==null AND $telephoneFound==null){
            Flashy::error("Aucune correspondance n'a été trouvé dans la base de donnéé pour ce proprietaire");
            session()->flash('emailConfirmationError', "Aucune correspondance n'a été trouvé dans la base de donnéé pour ce proprietaire. S'il n'existe pas veillez l'inscrire d'abord.");
            return redirect()->back()->withInput();
        }
        if ($nationalIdentityFound!=null){
            $proprietaire_id = $nationalIdentityFound->id;
        }else if($emailFound!=null){
            $proprietaire_id = $emailFound->id;
        }else if($telephoneFound!=null){
            $proprietaire_id = $telephoneFound->id;
        }
        $terrain =Terrain::create([
            'forme' => $request->forme,
            'longer' => $request->longer,
            'larger' => $request->larger,
            'dommaine' => $request->dommaine,
            'usage' => $request->usage,
            'build' => $request->build,
            'codeSecteur' =>Auth::user()->secteur->quartier->name.'__'.Auth::user()->secteur->name.'_'.$request->codeSecteur,
            'secteur_id' => Auth::user()->secteur->id,
            'proprietaire_id' => $proprietaire_id
        ]);


        $attestationAchat = $request->file('attestationAchat');
        $filename = str_slug($terrain->codeSecteur, '_') . '_' . str_shuffle(str_random(10)) . '.' . $attestationAchat->getClientOriginalExtension();
        Image::make($attestationAchat)->resize(900, 750)->save('storage/terrains/attestations/' . $filename);
        $terrain->attestationAchat = 'storage/terrains/attestations/' . $filename;
                if ($request->planDeMasse!=null){
                    $planDeMasse = $request->file('planDeMasse');
                    $filename1 =  str_slug($terrain->codeSecteur, '_') . '_' . str_shuffle(str_random(10)) . '.' . $planDeMasse->getClientOriginalExtension();
                    Image::make($planDeMasse)->resize(900, 750)->save('storage/terrains/planDeMasses/' . $filename1);
                    $terrain->planDeMasse = 'storage/terrains/planDeMasses/' . $filename1;
                 }
                 if ($request->titreFoncier!=null){
                     $titreFoncier = $request->file('titreFoncier');
                     $filename2 =  str_slug($terrain->codeSecteur, '_') . '_' . str_shuffle(str_random(10)) . '.' . $titreFoncier->getClientOriginalExtension();
                     Image::make($titreFoncier)->resize(900, 750)->save('storage/terrains/titreFonciers/' . $filename2);
                     $terrain->titreFoncier = 'storage/terrains/titreFonciers/' . $filename2;
                 }
        $terrain->save();



        Flashy::success("Le terrain à été ajouté avec succès et est en attente de validation de la part du chef de quartier");
        return redirect(url('/chefsecteur/home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $terrain = Terrain::where('secteur_id', Auth::user()->secteur_id)
                        ->where('verifiedByChefQuartier', 'no')
                        ->where('id', $id)->first();
        return view('gestions_des_administrateurs.chef_secteurs.show_land', compact('terrain'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $terrain = Terrain::where('secteur_id', Auth::user()->secteur_id)->where('verifiedByChefQuartier', 'no')->where('id', $id)->first();
        $dommaines = DB::table('dommaines')->select()->get();
        $usages = DB::table('usages')->select()->get();

        return view('gestions_des_administrateurs.chef_secteurs.edit_lang_not_verified_by_chef_quartier', compact('terrain', 'dommaines', 'usages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $terrain = Terrain::findOrFail($id);

        $nationalIdentityFound = Proprietaire::where('nationalIdentity', $request->identifiant)->first();
        $emailFound = Proprietaire::where('email', $request->identifiant)->first();
        $telephoneFound= Proprietaire::where('telephone', $request->identifiant)->first();
        if ($nationalIdentityFound==null AND $emailFound==null AND $telephoneFound==null){
            Flashy::error("Aucune correspondance n'a été trouvé dans la base de donnéé pour ce proprietaire");
            session()->flash('emailConfirmationError', "Aucune correspondance n'a été trouvé dans la base de donnéé pour ce proprietaire. S'il n'existe pas veillez l'inscrire d'abord.");
            return redirect()->back()->withInput();
        }
        if ($nationalIdentityFound!=null){
            $proprietaire_id = $nationalIdentityFound->id;
        }else if($emailFound!=null){
            $proprietaire_id = $emailFound->id;
        }else if($telephoneFound!=null){
            $proprietaire_id = $telephoneFound->id;
        }
        $terrain->update([
            'forme' => $request->forme,
            'longer' => $request->longer,
            'larger' => $request->larger,
            'dommaine' => $request->dommaine,
            'usage' => $request->usage,
            'build' => $request->build,
            'secteur_id' => Auth::user()->id,
            'proprietaire_id' => $proprietaire_id
        ]);


        if ($request->attestationAchat != null){
            $attestationAchat = $request->file('attestationAchat');
            $filename = str_slug($terrain->codeSecteur, '_') . '_' . str_shuffle(str_random(10)) . '.' . $attestationAchat->getClientOriginalExtension();
            Image::make($attestationAchat)->resize(900, 750)->save('storage/terrains/attestations/' . $filename);
            $terrain->attestationAchat = 'storage/terrains/attestations/' . $filename;
        }
        if ($request->planDeMasse!=null){
            $planDeMasse = $request->file('planDeMasse');
            $filename1 =  str_slug($terrain->codeSecteur, '_') . '_' . str_shuffle(str_random(10)) . '.' . $planDeMasse->getClientOriginalExtension();
            Image::make($planDeMasse)->resize(900, 750)->save('storage/terrains/planDeMasses/' . $filename1);
            $terrain->planDeMasse = 'storage/terrains/planDeMasses/' . $filename1;
        }
        if ($request->titreFoncier!=null){
            $titreFoncier = $request->file('titreFoncier');
            $filename2 =  str_slug($terrain->codeSecteur, '_') . '_' . str_shuffle(str_random(10)) . '.' . $titreFoncier->getClientOriginalExtension();
            Image::make($titreFoncier)->resize(900, 750)->save('storage/terrains/titreFonciers/' . $filename2);
            $terrain->titreFoncier = 'storage/terrains/titreFonciers/' . $filename2;
        }
        $terrain->save();

        Flashy::info("Le/la terrain/parcelle à été mis à jour avec succèss");
        return redirect(route('chefsecteur.gestion_chefs_secteur_terrains.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $terrain = Terrain::findOrFail($id);
        $terrain->delete();

        Flashy::error("Le terrain à été supprimé avec succès.");
        return redirect(url('/chefsecteur/home'));
    }
}
