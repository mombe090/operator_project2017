<?php

namespace App\Http\Controllers;

use App\Commune;
use function dd;
use Illuminate\Http\Request;
use function redirect;
use function route;
use function url;
use function view;

class ConnexionTypeController extends Controller
{
        public function index(){
            return view('connexion.connexionType');
        }

        public function choose(Request $request){
            if ($request->role=='Root'){
                return redirect(url('/root/login'));
            }else if($request->role=='SuperAdmin'){
                return redirect(url('/superadmin/login'));
            }else if($request->role=='Admin'){
                return redirect(url('/admin/login'));
            }else if($request->role=='Maire'){
                return redirect(url('/maire/login'));
            }else if($request->role=='ChefQuartier'){
                return redirect(url('/chefquartier/login'));
            }else if($request->role=='ChefSecteur'){
                return redirect(url('/chefsecteur/login'));
            }else if($request->role=='landOwner'){
                return redirect(url('/proprietaire/login'));
            }
        }
}
