<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Notifications\ConfirmationAdminNotification;
use App\Prefecture;
use App\Region;
use Auth;
use function compact;
use function dd;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;
use const null;
use function redirect;
use function route;
use const true;
use function view;

class SuperAdminGestionDesAdmins extends Controller
{
    public function __construct()
    {
        $this->middleware('superadmin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    $prefectures = Prefecture::where('region_id', Auth::user()->region_id)->get();
        $admins = Admin::where('verify', null)->
        where('working', 'off')->get();


        return view('gestions_des_administrateurs.super_admins.listAdmins', compact('admins', 'prefectures'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexConfirm()
    {
        $prefectures = Prefecture::where('region_id', Auth::user()->region_id)->get();
        $admins = Admin::where('verify', null)->
        where('working', 'on')->get();
        $confirm = true;

        return view('gestions_des_administrateurs.super_admins.listAdmins', compact('prefectures', 'admins', 'confirm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Acheteurs  $acheteurs
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = Admin::findOrFail($id);

        return view('gestions_des_administrateurs.super_admins.profiles.index', compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Acheteurs
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::findOrFail($id);
        $prefectures = Prefecture::orderBy('nom')->get();
        return view('gestions_des_administrateurs.super_admins.edit_admin_from_superadmins' ,compact('admin', 'prefectures'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Acheteurs  $acheteurs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $admin = Admin::findOrFail($id);
        if ($request->picture !=null){
            $picture = $request->file('picture');
            $filename = $admin->email.'.'.$picture->getClientOriginalExtension();
            Image::make($picture)->resize(300, 300)->save('storage/images/users/admins/profile_img/'.$filename);
           $admin->picture = '/storage/images/users/admins/profile_img/'.$filename;
           $admin->save();
            Flashy::success("La photo du profile de cet administrateur à mise à jour");
            return redirect()->back();
        }
          else  if (isset($request->trueUpdate)){
            $this->validate($request, [
                'firstName' => 'bail|required|max:255',
                'lastName' => 'bail|required|max:255',
                'email' => 'bail|required|email|max:255',
                'nationalIdentity' => 'bail|required|max:10',
                'telephone' => 'bail|required|max:12',
            ]);
            $admin->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'nationalIdentity' => $request->nationalIdentity,
                'telephone' => $request->telephone,
                'email' => $request->email,
                'dateOfBirth' => $request->dateOfBirth,
                'hireDate' => $request->hireDate,
                'prefecture_id' => $request->prefecture_id,
                ]);

            Flashy::success("La mise à jour du profile de l'administrateur à été mise à jour");
            return redirect()->back();
        }else{

            $admin->working = "on";
            $admin->save();

              event(new Registered($user = $admin));
              $user->notify(new ConfirmationAdminNotification());

            Flashy::success("L'administrateur a été confirmé avec succèss et pourra bénéfié des privilèges accordés");
            return redirect()->back();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Acheteurs  $acheteurs
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = Admin::findOrFail($id);
        $admin->delete();

        Flashy::success("Cet Administrateur à été supprimé avec succès");
        return redirect()->back();
    }
}
