<?php

namespace App\Http\Controllers\SuperadminAuth;

use App\Admin;
use App\ChefQuartiers;
use App\ChefSecteurs;
use App\Http\Requests\SuperAdminRequest;
use App\Maire;
use App\Month;
use App\Notifications\SuperAdminInscriptionNotification;
use App\Region;
use App\Root;
use App\Superadmin;
use function compact;
use function dd;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use MercurySeries\Flashy\Flashy;
use const null;
use function redirect;
use function retry;
use function route;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/superadmin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('superadmin.guest');
    }

    protected function registered(Request $request, $user)
    {
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Superadmin
     */
    protected function create(array $data)
    {

        return Superadmin::create([
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'nationalIdentity' => $data['nationalIdentity'],
            'email' => $data['email'],
            'telephone' => $data['telephone'],
            'dateOfBirth' => $data['jour'].'/'.$data['mois'].'/'.$data['annee'],
            'hireDate' => $data['jourE'].'/'.$data['moisE'].'/'.$data['anneeE'],
            'working' => 'off',
            'picture' => '/storage/images/users/default.jpg',
            'verify' => str_replace('/', '', Hash::make(str_shuffle(str_random(100)))),
            'region_id' => $data['region_id'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $months = Month::all();
        $regions = Region::orderBy('name')->get();
        return view('superadmin.auth.register', compact('months', 'regions'));
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('superadmin');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(SuperAdminRequest $request)
    {
        //On s'assure que le mail ou l'ide
        $rootEmails = Root::where('email', $request->input('email'))->first();
        $adminEmails = Admin::where('email', $request->input('email'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->orWhere('telephone', $request->input('telephone')) ->first();
        $maireEmails = Maire::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        $chefDeQuartierEmails = ChefQuartiers::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();
        $chefSecteurEmails = ChefSecteurs::where('email', $request->input('email'))->orWhere('telephone', $request->input('telephone'))->orWhere('nationalIdentity', $request->input('nationalIdentity'))->first();

        if ($rootEmails !=null){
            Flashy::error('L\'email en question est detenu par le root');
            session()->flash('emailConfirmationError', 'L\'email en question est detenu par le root');
            return redirect(route('superadminRegister'))->withInput();
        }
        if ($adminEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale ou encore le numero de téléphone en question est detenu par un administrateur ');
            session()->flash('emailConfirmationError','L\'email ou le numero d\'identité nationale ou encore le numero de téléphone en question est detenu par un administrateur ');
            return redirect(route('superadminRegister'))->withInput();
        }
        if ($maireEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale ou encore le numero de téléphone en question est detenu par un maire ');
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale ou encore le numero de téléphone en question est detenu par un maire ');
            return redirect(route('superadminRegister'))->withInput();
        }
        if ($chefDeQuartierEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale ou encore le numero de téléphone en question est detenu par un chef de quartier ');
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale ou encore le numero de téléphone en question est detenu par un chef de quartier ');
            return redirect(route('superadminRegister'))->withInput();
        }
        if ($chefSecteurEmails!=null){
            Flashy::error('L\'email ou le numero d\'identité nationale ou encore le numero de téléphone en question est detenu par un chef de secteur ');
            session()->flash('emailConfirmationError', 'L\'email ou le numero d\'identité nationale ou encore le numero de téléphone en question est detenu par un chef de secteur ');
            return redirect(route('superadminRegister'))->withInput();
        }

        $regionIdForSuperAdmin = Superadmin::where('region_id', $request->input('region_id'))->where('working', 'on')->first();


        if ($regionIdForSuperAdmin != null){
            Flashy::error('Le region a déjà un Directeur du BCF en fonction. Merci de vous addresser au Directeur du DATU');
            session()->flash('emailConfirmationError','Le region a déjà un Directeur du BCF en fonction. Merci de vous addresser au Directeur du DATU');
            return redirect()->back()->withInput();
        }

        if (Superadmin::where('working', 'on')->count() >= 8){
            Flashy::error('Les Directeurs Généraux des Bureaux de Conservation Foncière sont déjà aux complet merci de contacter le DG du DATU');
            session()->flash('emailConfirmationError','Les Directeurs Généraux des Bureaux de Conservation Foncière sont déjà aux complet merci de contacter le DG du DATU');
            return redirect()->back()->withInput();
        }


        $request->all();

        event(new Registered($user = $this->create($request->all())));
        $user->notify(new SuperAdminInscriptionNotification());
        Flashy::success('Votre compte a été crée avec succès. Consulter votre mail pour confirmer');
        session()->flash('emailConfirmation', 'Votre compte a été crée avec succès. Consulter votre mail pour confirmer');

        return redirect('/superadmin/login');
    }
    public function confirm($id, $token){
        $user = Superadmin::where('id', $id)->where('verify', $token)->first();

        if ($user){
            $user->update(
                [
                    'verify' => null
                ]
            );
            //$this->guard()->login($user);
            Flashy::info("Une dernière chose faite-vous confirmez par le DG du DATU");
            session()->flash('emailConfirmationInfo', 'Une dernière chose faite-vous confirmez par le DG du DATU l\'administrateur nationale');
            return redirect('/superadmin/login');
        }else{
            session()->flash('emailConfirmationError', 'Ce lien ne semble pas valide. Merci de bien verifier authenticité de ce mail.');
            Flashy::error('Ce lien ne semble pas valide. Merci de bien verifier authenticité de ce mail.');
            return redirect('/superadmin/login');
        }
    }
}
