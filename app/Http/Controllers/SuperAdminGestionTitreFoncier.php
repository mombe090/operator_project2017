<?php

namespace App\Http\Controllers;

use App\Terrain;
use function compact;
use DB;
use function dd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;
use const null;
use TerrainTableSeeder;
use const true;
use function view;

class SuperAdminGestionTitreFoncier extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terrains = DB::table('regions')
            ->join('superadmins', 'regions.id', 'superadmins.region_id')
            ->join('prefectures', 'prefectures.region_id', 'regions.id')
            ->join('communes', 'communes.prefecture_id', 'prefectures.id')
            ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
            ->join('proprietaires', 'proprietaires.id', '=', 'terrains.proprietaire_id')
            ->orderBy('prefectures.nom')
            ->orderBy('terrains.created_at')
            ->where('regions.id', Auth::user()->region->id)
            ->where('terrains.verifiedByAdmin', 'yes')
            ->where('terrains.titreFoncier', null)
            ->select('terrains.*', 'quartiers.name as quartierName', 'terrains.id as terrainID', 'proprietaires.*')
            ->paginate(10);



        return view('gestions_des_administrateurs.super_admins.titres_fonciers.en_attente', compact('terrains'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexConfirm()
    {
        $terrains = DB::table('regions')
            ->join('superadmins', 'regions.id', 'superadmins.region_id')
            ->join('prefectures', 'prefectures.region_id', 'regions.id')
            ->join('communes', 'communes.prefecture_id', 'prefectures.id')
            ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
            ->join('proprietaires', 'proprietaires.id', '=', 'terrains.proprietaire_id')
            ->orderBy('prefectures.nom')
            ->orderBy('terrains.created_at')
            ->where('regions.id', Auth::user()->region->id)
            ->where('terrains.verifiedByAdmin', 'yes')
            ->where('terrains.titreFoncier', '!=', null)
            ->select('terrains.*', 'quartiers.name as quartierName', 'terrains.id as terrainID', 'proprietaires.*')
            ->paginate(10);




        return view('gestions_des_administrateurs.super_admins.titres_fonciers.verified_titre_foncier', compact('terrains'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
            $terrain = Terrain::findOrFail($id);

            return view('gestions_des_administrateurs.super_admins.titres_fonciers.show_land', compact('terrain'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
       $terrain = Terrain::findOrFail($id);
       if (isset($request->modifyTitre)){
           return view('gestions_des_administrateurs.super_admins.titres_fonciers.edit_titre_foncier', compact('terrain'));
       }

       return view('gestions_des_administrateurs.super_admins.titres_fonciers.add_titre_foncier', compact('terrain'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $terrain = Terrain::findOrFail($id);

        $this->validate($request, [
            'titreFoncier' => 'bail|required|image'
        ]);
        if (isset($request->editTitre)){
            $titreFoncier = $request->file('titreFoncier');
            $filename = $terrain->codeSecteur.'.'.str_shuffle(str_random(10)).'.'.$titreFoncier->getClientOriginalExtension();
            Image::make($titreFoncier)->resize(900, 700)->save('storage/terrains/titreFonciers/'.$filename);
            $terrain->titreFoncier = 'storage/terrains/titreFonciers/'.$filename;
            $terrain->save();
            Flashy::success("Le titre foncier à été modifier avec succès.");
            return redirect()->back();
        }



        //   if ($request->hasFile('titreFoncier')){
        $titreFoncier = $request->file('titreFoncier');
        $filename = $terrain->codeSecteur.'.'.str_shuffle(str_random(10)).'.'.$titreFoncier->getClientOriginalExtension();
        Image::make($titreFoncier)->resize(900, 700)->save('storage/terrains/titreFonciers/'.$filename);
        $terrain->titreFoncier = 'storage/terrains/titreFonciers/'.$filename;
        $terrain->save();
        Flashy::success("Le titre foncier à été ajouter avec succès.");
        return redirect()->back();

    //   }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
