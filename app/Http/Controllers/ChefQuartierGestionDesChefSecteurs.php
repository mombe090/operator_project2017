<?php

namespace App\Http\Controllers;

use App\ChefQuartiers;
use App\ChefSecteurs;
use App\Notifications\ConfirmationRegistrationChefSecteur;
use App\Notifications\RejetRegistrationChefSecteur;
use App\Quartier;
use App\Secteur;
use Auth;
use function compact;
use function dd;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;
use function view;

class ChefQuartierGestionDesChefSecteurs extends Controller
{

    /**
     * ChefQuartierGestionDesChefSecteurs constructor.
     */
    public function __construct()
    {
        $this->middleware('chefquartier');

    }

    public function  index(){

        $quartiers = Quartier::where('commune_id', Auth::user()->quartier->commune_id)->orderBy('name')->get();
        $chefSecteurs = ChefSecteurs::where('verify', null)->
        where('working', 'off')->get();



    return view('gestions_des_administrateurs.chef_quarttiers.listChefsSecteurs', compact('quartiers', 'chefSecteurs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexConfirm()
    {
        $secteurs = Secteur::where('quartier_id', Auth::user()->id)->get();

        $chefSecteurs = ChefSecteurs::where('verify', null)->
        where('working', 'on')->get();



        $confirm = true;
        return view('gestions_des_administrateurs.chef_quarttiers.listChefsSecteurs', compact('secteurs', 'chefSecteurs', 'confirm'));

    }

    public function  create(){


    }
    public function show ($id){
        $chefSecteur = ChefSecteurs::findOrFail($id);

        return view('gestions_des_administrateurs.chef_quarttiers.profiles.index', compact('chefSecteur'));
    }


    public function update (Request $request,  $id){
        $chefsecteur = ChefSecteurs::findOrFail($id);
        if(isset($request->retirerAcces)){
            $chefsecteur->working = 'off';
            $chefsecteur->save();
            Flashy::error("L'access à la base de donnée à été retiré pour ce chef de quartier");
            return redirect()->back();
        }
        else if (isset($request->confirm)){
            $chefsecteur->working = "on";
            $chefsecteur->save();

            event(new Registered($user = $chefsecteur));
            $user->notify(new ConfirmationRegistrationChefSecteur());
            Flashy::success("Le chef secteur à été confirmé avec succès.");
            return redirect()->back();
        }
        else if(isset($request->profileUpdate)){
            $picture = $request->file('picture');
            $filename = $chefsecteur->email.'.'.$picture->getClientOriginalExtension();
            Image::make($picture)->resize(300, 300)->save('storage/images/users/chefSecteurs/profile_img/'.$filename);
            $chefsecteur->picture = 'storage/images/users/chefSecteurs/profile_img/'.$filename;
            $chefsecteur->save();
            Flashy::success("La photo du profile de ce chef secteur à été mise à jour avec succès.");
            return redirect()->back();
        }
        else if(is($request->trueUpdate)){
            $this->validate($request, [
                'firstName' => 'bail|required|max:255',
                'lastName' => 'bail|required|max:255',
                'email' => 'bail|required|email|max:255',
                'nationalIdentity' => 'bail|required|max:10',
                'telephone' => 'bail|required|max:12',
            ]);
            $chefsecteur->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'nationalIdentity' => $request->nationalIdentity,
                'telephone' => $request->telephone,
                'email' => $request->email,
                'dateOfBirth' => $request->dateOfBirth,
                'hireDate' => $request->hireDate,
                'secteur_id' => $request->secteur_id,
            ]);

            Flashy::success("La mise à jour du profile du maire à été mise à jour");
            return redirect()->back();
        }
    }


    public function  edit($id){
        $chefSecteur = ChefSecteurs::findOrFail($id);
        $secteurs = Secteur::where('quartier_id', Auth::user()->quartier_id)->orderBy('name')->get();

        return view('gestions_des_administrateurs.chef_quarttiers.edit_chefs_secteurs_from_chef_quartier', compact('chefSecteur', 'secteurs'));
    }


    public function destroy ($id){
        $chefSecteur = ChefSecteurs::findOrFail($id);
        $chefSecteur->delete();

        event(new Registered($user = $chefSecteur));
        $user->notify(new RejetRegistrationChefSecteur());
        Flashy::error("Cette inscription a été suprimer avec succès");
        return redirect()->back();
    }
}
