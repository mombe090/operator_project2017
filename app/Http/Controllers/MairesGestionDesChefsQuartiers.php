<?php

namespace App\Http\Controllers;

use App\ChefQuartiers;
use App\Notifications\ConfirmationChefQuartier;
use App\Quartier;
use function compact;
use function dd;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;
use function view;

class MairesGestionDesChefsQuartiers extends Controller
{
    public  function __construct()
    {
        $this->middleware('maire');
    }

    public function index(){

         $chefQuartiers = ChefQuartiers::where('verify', null)->
        where('working', 'off')->get();


       return view('gestions_des_administrateurs.maires.listChefsQuartiers', compact( 'chefQuartiers'));

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexConfirm()
    {
        $quartiers = Quartier::where('commune_id', Auth::user()->commune_id)->get();
        $chefQuartiers = ChefQuartiers::where('verify', null)->
        where('working', 'on')->get();



        $confirm = true;
        return view('gestions_des_administrateurs.maires.listChefsQuartiers', compact('quartiers', 'chefQuartiers', 'confirm'));

    }

    public function show($id){
        $chefQuartier = ChefQuartiers::findOrFail($id);

        return view('gestions_des_administrateurs.maires.profiles.index', compact('chefQuartier'));
    }
    public function edit($id){
        $chefQuartier = ChefQuartiers::findOrFail($id);
        $quartiers = Quartier::where('commune_id', Auth::user()->commune_id)->orderBy('name')->get();

        return view('gestions_des_administrateurs.maires.edit_chefs_quartiers_from_maire', compact('chefQuartier', 'quartiers'));
    }
    public  function update(Request $request, $id){
        $chefQuartier = ChefQuartiers::findOrFail($id);
        if(isset($request->retirerAcces)){
            $chefQuartier->working = 'off';
            $chefQuartier->save();
            Flashy::error("L'access à la base de donnée à été retiré pour ce chef de quartier");
            return redirect()->back();
        }
        else if (isset($request->confirm)){
            $chefQuartier->working = "on";
            $chefQuartier->save();

            event(new Registered($user = $chefQuartier));
            $user->notify(new ConfirmationChefQuartier());
            Flashy::success("Le maire à été confirmé avec succès.");
            return redirect()->back();
        }else if(isset($request->profileUpdate)){
            $picture = $request->file('picture');
            $filename = $chefQuartier->email.'.'.$picture->getClientOriginalExtension();
            Image::make($picture)->resize(300, 300)->save('storage/images/users/chefQuartiers/profile_img/'.$filename);
            $chefQuartier->picture = 'storage/images/users/chefQuartiers/profile_img/'.$filename;
            $chefQuartier->save();
            Flashy::success("La photo du profile de ce chef de quartier à été mise à jour avec succès.");
            return redirect()->back();
        }else if(is($request->trueUpdate)){
            $this->validate($request, [
                'firstName' => 'bail|required|max:255',
                'lastName' => 'bail|required|max:255',
                'email' => 'bail|required|email|max:255',
                'nationalIdentity' => 'bail|required|max:10',
                'telephone' => 'bail|required|max:12',
            ]);
            $chefQuartier->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'nationalIdentity' => $request->nationalIdentity,
                'telephone' => $request->telephone,
                'email' => $request->email,
                'dateOfBirth' => $request->dateOfBirth,
                'hireDate' => $request->hireDate,
                'quartier_id' => $request->quartier_id,
            ]);

            Flashy::success("La mise à jour du profile du maire à été mise à jour");
            return redirect()->back();
        }
    }
    public function destroy($id){
        $chefQuartier = ChefQuartiers::findOrFail($id);
        $chefQuartier->delete();

        event(new Registered($user = $chefQuartier));
        $user->notify(new \App\Notifications\RejetMairesConfirmationDesChefsQuartiers());
        Flashy::error("Cette inscription a été suprimer avec succès");
        return redirect()->back();
    }
}
