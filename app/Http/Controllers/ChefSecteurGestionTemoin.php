<?php

namespace App\Http\Controllers;

use App\Month;
use App\Temoin;
use App\TemoinV;
use App\Terrain;
use function compact;
use function dd;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use function is;
use MercurySeries\Flashy\Flashy;
use function redirect;
use function session;
use function view;

class ChefSecteurGestionTemoin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $months = Month::orderBy('id')->get();
        return view('gestions_des_administrateurs.chef_secteurs.temoins.add_temoin', compact('months'));
    }

    public function create2(Request $request)
    {

        $months = Month::orderBy('id')->get();
        return view('gestions_des_administrateurs.chef_secteurs.temoins.add_temoinAcheteur2', compact('months'));
    }


    public function create3(Request $request)
    {

        $months = Month::orderBy('id')->get();
        return view('gestions_des_administrateurs.chef_secteurs.temoins.add_temoinAcheteur3', compact('months'));
    }

    public function create4(Request $request)
    {

        $months = Month::orderBy('id')->get();
        return view('gestions_des_administrateurs.chef_secteurs.temoins.vendeur.add_temoin_vendeur1',   compact('months'));
    }


    public function create5(Request $request)
    {

        $months = Month::orderBy('id')->get();
        return view('gestions_des_administrateurs.chef_secteurs.temoins.vendeur.add_temoinAcheteur_vendeur2',   compact('months'));
    }


    public function create6(Request $request)
    {

        $months = Month::orderBy('id')->get();
        return view('gestions_des_administrateurs.chef_secteurs.temoins.vendeur.add_temoin_vendeur3',   compact('months'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nationalIdentity' =>'bail|required|unique:proprietaires',
            'firstName' => 'bail|required|min:3',
            'lastName' => 'bail|required|min:3',
            'email' => 'bail|required|email|unique:proprietaires',
            'telephone' => 'bail|required|unique:proprietaires',
            'picture' => 'bail|required|image',

        ]);

        if (isset($request->numeroTemoin)  AND ($request->numeroTemoin>=3 AND $request->numeroTemoin<=5 )){

            $dateOfBirth = $request->jour.'/'.$request->mois.'/'.$request->annee;
            $temoin = TemoinV::create([
                'nationalIdentity' => $request->nationalIdentity,
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'dateOfBirth' => $dateOfBirth,
                'terrain_id' => session('terrainEnVente'),
                'vendeur_id' => session('acheteur')
            ]);

            $picture = $request->file('picture');
            $filename = str_slug($temoin->email, '_') . '_' . str_shuffle(str_random(10)) . '.' . $picture->getClientOriginalExtension();
            Image::make($picture)->resize(900, 750)->save('storage/images/users/temoins/' . $filename);
            $temoin->picture = 'storage/images/users/temoins/' . $filename;
            $temoin->save();


          if (isset($request->numeroTemoin) AND $request->numeroTemoin==3){

                return redirect()->route('chefsecteur.temoin_vendeur_1');
            }else if (isset($request->numeroTemoin) AND $request->numeroTemoin==4){
                return redirect()->route('chefsecteur.temoin_vendeur_2');
            }else if (isset($request->numeroTemoin) AND $request->numeroTemoin==5){
                return redirect()->route('chefsecteur.temoin_vendeur_3');
            }

            return redirect()->route('chefsecteur.terrains_attentes_pro');

        }

          $dateOfBirth = $request->jour.'/'.$request->mois.'/'.$request->annee;
          $temoin = Temoin::create([
              'nationalIdentity' => $request->nationalIdentity,
              'firstName' => $request->firstName,
              'lastName' => $request->lastName,
              'email' => $request->email,
              'telephone' => $request->telephone,
              'dateOfBirth' => $dateOfBirth,
              'terrain_id' => session('terrainEnVente'),
              'acheteur_id' => session('acheteur')
          ]);

          $picture = $request->file('picture');
          $filename = str_slug($temoin->email, '_') . '_' . str_shuffle(str_random(10)) . '.' . $picture->getClientOriginalExtension();
          Image::make($picture)->resize(900, 750)->save('storage/images/users/temoins/' . $filename);
          $temoin->picture = 'storage/images/users/temoins/' . $filename;
          $temoin->save();




        if (isset($request->numeroTemoin) AND $request->numeroTemoin==1){
            Flashy::info('Le premier temoin à été rajouter avec succès. Rajouter un second');
            session()->flash('emailConfirmationInfo', 'Le premier temoin à été rajouter avec succès. Rajouter un second');
            return redirect()->route('chefsecteur.temoin_acheteur_2');
        }
        elseif (isset($request->numeroTemoin) AND $request->numeroTemoin==2){
            Flashy::info('Le deuxième temoin à été rajouter avec succès. Rajouter un troisième');
            session()->flash('emailConfirmationInfo', 'Le deuxième temoin à été rajouter avec succès. Rajouter un troisième');
            return redirect()->route('chefsecteur.temoin_acheteur_3');
        }else if (isset($request->numeroTemoin) AND $request->numeroTemoin==3){
            Flashy::info('Le deuxième temoin à été rajouter avec succès. Rajouter un troisième');
            session()->flash('emailConfirmationInfo', 'Le deuxième temoin à été rajouter avec succès. Rajouter un troisième');
            return redirect()->route('chefsecteur.temoin_vendeur_1');
        }else if (isset($request->numeroTemoin) AND $request->numeroTemoin==4){
            return redirect()->route('chefsecteur.temoin_vendeur_2');
        }else if (isset($request->numeroTemoin) AND $request->numeroTemoin==5){
            return redirect()->route('chefsecteur.temoin_vendeur_3');
        }

        return redirect()->route('chefsecteur.terrains_attentes_pro');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
