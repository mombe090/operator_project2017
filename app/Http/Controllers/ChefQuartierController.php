<?php

namespace App\Http\Controllers;

use App\ChefQuartiers;
use App\Quartier;
use Auth;
use Illuminate\Http\Request;
use function compact;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;
use function view;

class ChefQuartierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChefQuartiers $chefQuartier
     *
     * @return \Illuminate\Http\Response
     */
    public function show(ChefQuartiers $chefQuartier, $id)
    {
        if ($id != Auth::user()->id){
            Flashy::error("Vous ne pouvez pas voir/ni modifier le profile d'un collègue chef de quartier");
            return redirect()->back();
        }
        $chefQuartier = $chefQuartier::findOrFail($id);
        $quartiers = Quartier::where('commune_id', Auth::user()->quartier->commune->id)->get();


        return view('gestions_des_administrateurs.chef_quarttiers.profiles.chef_quartiers_profile', compact('chefQuartier', 'quartiers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChefQuartiers $chefQuartier
     * @return \Illuminate\Http\Response
     */
    public function edit(ChefQuartiers $chefQuartier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\ChefQuartiers       $chefQuartier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        if ($id != Auth::user()->id){
            Flashy::error("Vous ne pouvez pas voir/ni modifier le profile d'un collègue chef de quartier");
            return redirect()->back();
        }

        $chefQuartier = ChefQuartiers::findOrFail($id);
        if ($request->picture !=null){
            $picture = $request->file('picture');
            $filename = $chefQuartier->email.'.'.str_shuffle(str_random(10)).'.'.$picture->getClientOriginalExtension();
            Image::make($picture)->resize(300, 300)->save('storage/images/users/chefQuartiers/profile_img/'.$filename);
            $chefQuartier->picture = 'storage/images/users/chefQuartiers/profile_img/'.$filename;
            $chefQuartier->save();
            Flashy::success("Votre photo de profile à été mise à jour avect succès");
            return redirect()->back();
        }
        else  if (isset($request->isBio)){
            $this->validate($request, [
                'bio' => 'bail|required|min:10',
            ]);
            $chefQuartier->bio = nl2br(str_replace('<br />', PHP_EOL, $request->bio));
            $chefQuartier->save();

            Flashy::success("Votre biographie a été  mise à jour avec succès.");
            return redirect()->back();
        }else if(isset($request->allUp)) {
            if ($request->quartier_id != $chefQuartier->quartier->id){
                Flashy::error("Vous ne pouvez pas acceder aux informations comme ça. Allez chez le maire pour qu'il verifie");
                return redirect()->back();
            }
            $chefQuartier->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'nationalIdentity' => $request->nationalIdentity,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'dateOfBirth' => $request->dateOfBirth,
                'hireDate' => $request->hireDate,
                'quartier_id' => $request->quartier_id,
            ]);

            Flashy::success("Les informations ont été mise à jour avec succès");
            return redirect()->back();
        }else{

            $chefQuartier->working = "on";
            $chefQuartier->save();

            Flashy::success("L'administrateur a été confirmé avec succèss et pourra bénéfié des privilèges accordés");
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChefQuartiers $chefQuartier
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChefQuartiers $chefQuartier)
    {
        //
    }
}
