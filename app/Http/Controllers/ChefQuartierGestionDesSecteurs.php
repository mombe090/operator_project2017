<?php

namespace App\Http\Controllers;

use App\Secteur;
use Auth;
use function compact;
use function dd;
use const false;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;
use function redirect;
use function str_slug;
use const true;
use function url;
use function view;

class ChefQuartierGestionDesSecteurs extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('gestions_des_administrateurs.chef_quarttiers.secteurs.add_secteur');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'name' => 'bail|required|min:3'
        ]);
        $secteurs = Secteur::all();
        $isUsed = false;
        $count = Secteur::where('quartier_id', Auth::user()->id)->count();


        foreach ($secteurs as $secteur){
            if ($secteur->quartier->id==Auth::user()->id){
                if ($secteur->name==$request->name){
                    $isUsed = true;
                }
            }
        }

        if ($isUsed==true){
            Flashy::error("Ce nom de secteur existe déjà dans votre quartier.");
            return redirect()->back()->withInput();
        }
         Secteur::create([
            'code' => str_slug(Auth::user()->quartier->name, '_').'_secteur_'.$request->name.'__'.($count+1),
            'name' => $request->name,
             'quartier_id' => Auth::user()->quartier->id
        ]);
        Flashy::success("Le secteur à été ajouter dans votre quartier avec succès");
        return redirect(url('/chefquartier/home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $secteurs = Secteur::where('quartier_id', Auth::user()->quartier->id)->get();




         return view('gestions_des_administrateurs.chef_quarttiers.secteurs.list_secteurs', compact('secteurs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
