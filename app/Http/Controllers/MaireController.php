<?php

namespace App\Http\Controllers;

use App\Commune;
use App\Maire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;

class MaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id != Auth::user()->id){
            Flashy::error("Vous ne pouvez pas voir/ni modifier le profile d'un collègue maire.");
            return redirect()->back();
        }
        $maire = Maire::findOrFail($id);
        $communes = Commune::where('prefecture_id', Auth::user()->commune->prefecture_id)->orderBy('name')->get();
        return view('gestions_des_administrateurs.maires.profiles.maire_profile', compact('maire', 'communes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($id != Auth::user()->id){
            Flashy::error("Vous ne pouvez pas voir/ni modifier le profile d'un collègue maire");
            return redirect()->back();
        }

        $maire = Maire::findOrFail($id);
        if ($request->picture !=null){
            $picture = $request->file('picture');
            $filename = $maire->email.'.'.str_shuffle(str_random(10)).'.'.$picture->getClientOriginalExtension();
            Image::make($picture)->resize(300, 300)->save('storage/images/users/maires/profile_img/'.$filename);
            $maire->picture = 'storage/images/users/maires/profile_img/'.$filename;
            $maire->save();
            Flashy::success("Votre photo de profile à été mise à jour avect succès");
            return redirect()->back();
        }
        else  if (isset($request->isBio)){
            $this->validate($request, [
                'bio' => 'bail|required|min:10',
            ]);
            $maire->bio = nl2br(str_replace('<br />', PHP_EOL, $request->bio));
            $maire->save();

            Flashy::success("Votre biographie a été  mise à jour avec succès.");
            return redirect()->back();
        }else if(isset($request->allUp)) {
            $maire->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'nationalIdentity' => $request->nationalIdentity,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'dateOfBirth' => $request->dateOfBirth,
                'hireDate' => $request->hireDate,
                'commune_id' => $request->commune_id,
            ]);

            Flashy::success("Les informations ont été mise à jour avec succès");
            return redirect()->back();
        }else{

            $maire->working = "on";
            $maire->save();

            Flashy::success("L'maireistrateur a été confirmé avec succèss et pourra bénéfié des privilèges accordés");
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
