<?php

namespace App\Http\Controllers;

use App\Terrain;
use Auth;
use function compact;
use function dd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use MercurySeries\Flashy\Flashy;
use const null;
use function view;

class MaireGestionDesTerrains extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexConfirm()
    {
        $terrains = DB::table('communes')
            ->join('maires', 'maires.commune_id', 'communes.id')
            ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
            ->join('proprietaires', 'proprietaires.id', '=', 'terrains.proprietaire_id')
            ->where('communes.id', Auth::user()->commune->id)
            ->where('terrains.verifiedByMaire', 'yes')->where('terrains.verifiedByChefQuartier', 'yes')
            ->select('quartiers.name as quartier', 'terrains.*', 'proprietaires.*', 'terrains.id as terrainID')
            ->orderBy('terrains.updated_at')
            ->paginate(5);
        return view('gestions_des_administrateurs.maires.terrains.list_land_validate', compact('terrains'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function planDeMasse()
    {
        $terrains = DB::table('communes')
            ->join('maires', 'maires.commune_id', 'communes.id')
            ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
            ->join('proprietaires', 'proprietaires.id', '=', 'terrains.proprietaire_id')
            ->where('communes.id', Auth::user()->commune->id)
            ->where('terrains.verifiedByMaire', 'yes')->where('terrains.verifiedByChefQuartier', 'yes')
            ->where('terrains.planDeMasse', null)
            ->select('quartiers.name as quartier', 'terrains.*', 'proprietaires.*', 'terrains.id as terrainID')
            ->orderBy('terrains.updated_at')
            ->paginate(5);

;
        return view('gestions_des_administrateurs.maires.terrains.plan_de_masse', compact('terrains'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function planDeMasseUpdate()
    {
        $terrains = DB::table('communes')
            ->join('maires', 'maires.commune_id', 'communes.id')
            ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
            ->join('proprietaires', 'proprietaires.id', '=', 'terrains.proprietaire_id')
            ->where('communes.id', Auth::user()->commune->id)
            ->where('terrains.verifiedByMaire', 'yes')->where('terrains.verifiedByChefQuartier', 'yes')
            ->where('terrains.planDeMasse', '!=', null)
            ->select('quartiers.name as quartier', 'terrains.*', 'proprietaires.*', 'terrains.id as terrainID')
            ->orderBy('terrains.updated_at', 'desc')
            ->paginate(5);

        return view('gestions_des_administrateurs.maires.terrains.plan_de_masseUpdate', compact('terrains'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terrains = DB::table('communes')
            ->join('maires', 'maires.commune_id', 'communes.id')
            ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
            ->join('proprietaires', 'proprietaires.id', '=', 'terrains.proprietaire_id')
            ->where('communes.id', Auth::user()->commune->id)
            ->where('terrains.verifiedByMaire', 'no')->where('terrains.verifiedByChefQuartier', 'yes')
            ->select('quartiers.name as quartier', 'terrains.*', 'proprietaires.*', 'terrains.id as terrainID')
            ->orderBy('terrains.updated_at')
            ->paginate(5);

        return view('gestions_des_administrateurs.maires.terrains.list_land_not_validate', compact('terrains'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $terrain = Terrain::findOrFail($id);

        return view('gestions_des_administrateurs.maires.terrains.show_land', compact('terrain'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $terrain = Terrain::findOrFail($id);
        $dommaines = DB::table('dommaines')->select()->get();
        $usages = DB::table('usages')->select()->get();
        if ($terrain->verifiedByMaire=='no' AND $terrain->verifiedByChefQuartier=='yes' ){
            return view('gestions_des_administrateurs.maires.terrains.edit_land_verified_by_chef_quartier', compact('terrain', 'dommaines', 'usages'));
        }else if($terrain->verifiedByMaire=='yes' AND $terrain->verifiedByChefQuartier=='yes' ){
            return view('gestions_des_administrateurs.maires.terrains.edit_land_verified_by_maire', compact('terrain', 'dommaines', 'usages'));
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $terrain = Terrain::findOrFail($id);

        if (isset($request->allUp)){
            if (isset($request->old)){
                $terrain->update([
                    'forme' => $request->forme,
                    'longer' => $request->longer,
                    'larger' => $request->larger,
                    'dommaine' => $request->dommaine,
                    'usage' => $request->usage,
                    'build' => $request->build,
                    'codeCommune' => $request->codeCommune,

                ]);
            }else{
                $terrain->update([
                    'forme' => $request->forme,
                    'longer' => $request->longer,
                    'larger' => $request->larger,
                    'dommaine' => $request->dommaine,
                    'usage' => $request->usage,
                    'build' => $request->build,
                    'codeQuartier' => $request->codeQuartier,

                ]);
            }
            Flashy::success("Les modifications de ce terrains ont été effectué avec succès.");
            return redirect(route('maire.home'));
        }else if(isset($request->planDeMasse)){

            $planDeMasse = $request->file('planDeMasse');
            $filename = $terrain->codeSecteur.'_'.str_shuffle(str_random(10)).'.'.$planDeMasse->getClientOriginalExtension();
            Image::make($planDeMasse)->resize(900, 800)->save('storage/images/planDeMasses/'.$filename);
            $terrain->planDeMasse = 'storage/images/planDeMasses/'.$filename;
            $terrain->save();
            Flashy::success("Le Plan de masse à été ajouter avec succès pour cette parcelle.");
            return redirect()->back();
        }

        $terrain->verifiedByMaire = 'yes';
        $terrain->codeCommune = Auth::user()->commune->prefecture->nom.'__'.Auth::user()->commune->name.'_'.$terrain->codeQuartier;
        $terrain->save();

        Flashy::success("La confirmation de ce terrains est effectué avec succès.");
        return redirect(url('/maire/home'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $terrain = Terrain::findOrFail($id);
        if ($terrain->verifiedByChefQuartier=='yes' AND $terrain->verifiedByMaire=='yes'){
            Flashy::error("Vous ne pouvez pas supprimé une parcelle que le chef de quartier et vous avez confirmer.");
            return redirect()->back();
        }
        $terrain->delete();

        Flashy::error("Cette parcelle à été supprimer avec succès en tant que maire de la commune");
        return redirect()->back();
    }
}
