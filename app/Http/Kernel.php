<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,

    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'root' => \App\Http\Middleware\RedirectIfNotRoot::class,
        'root.guest' => \App\Http\Middleware\RedirectIfRoot::class,
        'proprietaire' => \App\Http\Middleware\RedirectIfNotProprietaire::class,
        'proprietaire.guest' => \App\Http\Middleware\RedirectIfProprietaire::class,
        'maire' => \App\Http\Middleware\RedirectIfNotMaire::class,
        'maire.guest' => \App\Http\Middleware\RedirectIfMaire::class,
        'chefquartier' => \App\Http\Middleware\RedirectIfNotChefquartier::class,
        'chefquartier.guest' => \App\Http\Middleware\RedirectIfChefquartier::class,
        'chefsecteur' => \App\Http\Middleware\RedirectIfNotChefsecteur::class,
        'chefsecteur.guest' => \App\Http\Middleware\RedirectIfChefsecteur::class,
        'superadmin' => \App\Http\Middleware\RedirectIfNotSuperadmin::class,
        'superadmin.guest' => \App\Http\Middleware\RedirectIfSuperadmin::class,
        'admin' => \App\Http\Middleware\RedirectIfNotAdmin::class,
        'admin.guest' => \App\Http\Middleware\RedirectIfAdmin::class,
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.admin' => \App\Http\Middleware\CheckIfAdminAuthenticated::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'guest.admin' => \App\Http\Middleware\RedirectIfAdminAuthenticated::class,
    ];
}
