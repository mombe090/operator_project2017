<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapRootRoutes();

        $this->mapProprietaireRoutes();

        $this->mapMaireRoutes();

        $this->mapChefquartierRoutes();

        $this->mapChefsecteurRoutes();

        $this->mapSuperadminRoutes();

        $this->mapAdminRoutes();

        //
    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        Route::group([
            'middleware' => ['web', 'admin', 'auth:admin'],
            'prefix' => 'admin',
            'as' => 'admin.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/admin.php');
        });
    }

    /**
     * Define the "superadmin" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapSuperadminRoutes()
    {
        Route::group([
            'middleware' => ['web', 'superadmin', 'auth:superadmin'],
            'prefix' => 'superadmin',
            'as' => 'superadmin.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/superadmin.php');
        });
    }

    /**
     * Define the "chefsecteur" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapChefsecteurRoutes()
    {
        Route::group([
            'middleware' => ['web', 'chefsecteur', 'auth:chefsecteur'],
            'prefix' => 'chefsecteur',
            'as' => 'chefsecteur.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/chefsecteur.php');
        });
    }

    /**
     * Define the "chefquartier" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapChefquartierRoutes()
    {
        Route::group([
            'middleware' => ['web', 'chefquartier', 'auth:chefquartier'],
            'prefix' => 'chefquartier',
            'as' => 'chefquartier.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/chefquartier.php');
        });
    }

    /**
     * Define the "maire" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapMaireRoutes()
    {
        Route::group([
            'middleware' => ['web', 'maire', 'auth:maire'],
            'prefix' => 'maire',
            'as' => 'maire.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/maire.php');
        });
    }

    /**
     * Define the "proprietaire" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapProprietaireRoutes()
    {
        Route::group([
            'middleware' => ['web', 'proprietaire', 'auth:proprietaire'],
            'prefix' => 'proprietaire',
            'as' => 'proprietaire.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/proprietaire.php');
        });
    }

    /**
     * Define the "root" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapRootRoutes()
    {
        Route::group([
            'middleware' => ['web', 'root', 'auth:root'],
            'prefix' => 'root',
            'as' => 'root.',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/root.php');
        });
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
