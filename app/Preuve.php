<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preuve extends Model
{
    protected $fillable = [
        'attestationAchat',
        'planDeMasse',
        'titreFoncier',
        'terrain_id'
    ];
    public  function terrain(){
        return $this->belongsTo('App\Terrain');
    }
}
