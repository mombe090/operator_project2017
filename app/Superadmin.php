<?php

namespace App;

use App\Notifications\SuperadminResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Superadmin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName',
        'lastName',
        'nationalIdentity',
        'telephone',
        'email',
        'dateOfBirth',
        'hireDate',
        'picture',
        'working',
        'role',
        'region_id',
        'verify',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new SuperadminResetPassword($token));
    }
    public function region(){
        return $this->belongsTo('App\Region');
    }


}
