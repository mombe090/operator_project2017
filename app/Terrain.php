<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terrain extends Model
{
    protected $fillable = [
        'forme',
        'longer',
        'larger',
        'dommaine',
        'usage',
        'build',
        'attestationAchat',
        'planDeMasse',
        'titreFoncier',
        'picture',
        'codeSecteur',
        'codeQuartier',
        'codeCommune',
        'codeVille',
        'codeRegion',
        'codeNational',
        'onSale',
        'verifiedByChefQuartier',
        'verifiedByMaire',
        'verifiedByAdmin',
        'secteur_id',
        'proprietaire_id'
    ];

    public function secteur(){
        return $this->belongsTo('App\Secteur');
    }
    public function preuve(){
        return $this->hasOne('App\Preuve');
    }
    public function proprietaire(){
        return $this->belongsTo('App\Proprietaire');
    }
}
