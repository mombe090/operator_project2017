@extends('adminlte::layouts.app')

@section('htmlheader_title')
    MTN Profile du super utilisateur
@endsection
@section('contentheader_title')
    Vous êtes connecté en tant que {{Auth::user()->lastName}} {{Auth::user()->firstName}} propriétaire de terrain.
@endsection

@section('main-content')

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->

        <div class="row">

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Liste de vos propriétés.</h3>

                            <div class="box-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr class="bg-light-blue">
                                    <th>code secteur</th>
                                    <th>Position</th>
                                    <th>forme</th>
                                    <th>Dimensions</th>
                                    <th>Utilisation</th>
                                    <th>Construit</th>
                                    <th>Attestation d'acquisition</th>
                                    <th>Plan de masse</th>
                                    <th>Titre foncier</th>
                                    <th>Vendre</th>
                                </tr>
                                @foreach($parcelles as $parcelle)
                                    <tr>
                                        <td>{{$parcelle->codeSecteur}} </td>
                                        <td>{{$parcelle->secteur->name}}--{{$parcelle->secteur->quartier->name}}</td>
                                        <td>{{$parcelle->forme}}</td>
                                        <td>{{$parcelle->longer}}*{{$parcelle->larger}} <i class="text-red">m*m</i></td>
                                        <td>{{$parcelle->usage}}</td>
                                        <td>{{$parcelle->build}}</td>
                                        <td><a href="{{asset($parcelle->attestationAchat)}}"><img src="{{asset($parcelle->attestationAchat)}}" alt="" class="img-md img-thumbnail"></a></td>
                                        <td><a href="{{asset($parcelle->planDeMasse)}}"><img src="{{asset($parcelle->planDeMasse)}}" alt="" class="img-md img-thumbnail"></a></td>
                                        <td><a href="{{asset($parcelle->titreFoncier)}}"><img src="{{asset($parcelle->titreFoncier)}}" alt="" class="img-md img-thumbnail"></a></td>
                                        <td>
                                            <form action="{{route('proprietaire.gestion_personnel_des_parcelle.update', $parcelle->id)}}" method="post" class="margin-bottom {{$parcelle->onSale=='1' ? 'hidden' : '' }}">
                                                {{csrf_field()}}
                                                {{method_field('put')}}
                                                <input type="hidden" name="vente" id="">
                                                <input type="submit" name="validator" onclick="return confirm('Etes-vous sûre de vouloir de mettre ce terrain en vente.')" class="btn btn-danger" value="Mettre en vente">
                                            </form>
                                            <form action="{{route('proprietaire.gestion_personnel_des_parcelle.update', $parcelle->id)}}" method="post"  class="margin-bottom {{$parcelle->onSale=='0' ? 'hidden' : '' }}">
                                                {{csrf_field()}}
                                                {{method_field('put')}}
                                                <input type="hidden" name="retire" id="">
                                                <input type="submit" name="validator" onclick="return confirm('Etes-vous sûre de vouloir de retirer ce terrain de la vente.')" class="btn btn-success" value="Retirer la vente">
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            {{$parcelles->links()}}
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>

        </div>


    </section>
@endsection
