<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="#"></a><b>Mombesoft</b></a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="#">Mombesoft</a>.</strong> {{ trans('adminlte_lang::message.createdby') }} <a href="#">Mamadou Yaya DIALLO</a>.
</footer>
