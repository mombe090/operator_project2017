<!-- Main Header -->
<header class="main-header">
@php $url =''; @endphp
@if(Auth::user()->role=='root')
        @php $url = '/root/home' @endphp
@elseif(Auth::user()->role=='superadmin')
    @php $url = '/superadmin/home' @endphp
@elseif(Auth::user()->role=='admin')
    @php $url = '/admin/home' @endphp
@elseif(Auth::user()->role=='maire')
    @php $url = '/maire/home' @endphp
@elseif(Auth::user()->role=='chefQuartier')
    @php $url = '/chefquartier/home' @endphp
@elseif(Auth::user()->role=='chefSecteur')
    @php $url = '/chefsecteur/home' @endphp
@elseif(Auth::user()->role=='proprietaire')
    @php $url = '/proprietaire/home' @endphp
@endif
    <!-- Logo -->
    <a href="{{$url}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>M</b>TN</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>{{session('email')}}</b>MTN Yello Care</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">4</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">{{ trans('adminlte_lang::message.tabmessages') }}</li>
                        <li>
                            <!-- inner menu: contains the messages -->
                            <ul class="menu">
                                <li><!-- start message -->
                                    <a href="#">
                                        <div class="pull-left">
                                            <!-- User Image -->
                                            <img src="{{$user->role =='root' ? '/storage/images/users/default.jpg' : $user->picture}}" class="img-circle" alt="User Image"/>
                                        </div>
                                        <!-- Message title and timestamp -->
                                        <h4>
                                            {{ trans('adminlte_lang::message.supteam') }}
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <!-- The message -->
                                        <p>{{ trans('adminlte_lang::message.awesometheme') }}</p>
                                    </a>
                                </li><!-- end message -->
                            </ul><!-- /.menu -->
                        </li>
                        <li class="footer"><a href="#">c</a></li>
                    </ul>
                </li><!-- /.messages-menu -->

                <!-- Notifications Menu -->
                <li class="dropdown notifications-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning">10</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">{{ trans('adminlte_lang::message.notifications') }}</li>
                        <li>
                            <!-- Inner Menu: contains the notifications -->
                            <ul class="menu">
                                <li><!-- start notification -->
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> {{ trans('adminlte_lang::message.newmembers') }}
                                    </a>
                                </li><!-- end notification -->
                            </ul>
                        </li>
                        <li class="footer"><a href="#">{{ trans('adminlte_lang::message.viewall') }}</a></li>
                    </ul>
                </li>
                <!-- Tasks Menu -->
                <li class="dropdown tasks-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                        <span class="label label-danger">9</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">{{ trans('adminlte_lang::message.tasks') }}</li>
                        <li>
                            <!-- Inner menu: contains the tasks -->
                            <ul class="menu">
                                <li><!-- Task item -->
                                    <a href="#">
                                        <!-- Task title and progress text -->
                                        <h3>
                                            {{ trans('adminlte_lang::message.tasks') }}
                                            <small class="pull-right">20%</small>
                                        </h3>
                                        <!-- The progress bar -->
                                        <div class="progress xs">
                                            <!-- Change the css width attribute to simulate progress -->
                                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">20% {{ trans('adminlte_lang::message.complete') }}</span>
                                            </div>
                                        </div>
                                    </a>
                                </li><!-- end task item -->
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">{{ trans('adminlte_lang::message.alltasks') }}</a>
                        </li>
                    </ul>
                </li>

                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
            @else
                <!-- User Account Menu -->
                    <li class="dropdown user user-menu" id="user_menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{$user->role =='root' ? '/storage/images/users/default.jpg' : asset(Auth::user()->picture)}}" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{$user->role =='root' ? 'ROOT' : $user->lastName}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{$user->role =='root' ? '/storage/images/users/default.jpg' : asset(Auth::user()->picture)}}" class="img-circle" alt="User Image" />
                                <p>

                                    <small>MTN Yello Care Challenge la solution aux problèmes de terrains en Guinée </small>
                                </p>
                            </li>

                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="col-xs-4 text-center">

                                </div>
                                <div class="col-xs-4 text-center">

                                </div>

                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    @php $route=""; @endphp
                                    @if(Auth::user()->role=='root')
                                        @php $route = 'root.home' @endphp
                                    @elseif(Auth::user()->role=='superadmin')
                                        @php $route = 'superadmin.gestion_own_superadmins.show' @endphp
                                    @elseif(Auth::user()->role=='admin')
                                        @php $route = 'admin.gestion_own_admins.show' @endphp
                                    @elseif(Auth::user()->role=='maire')
                                        @php $route = 'maire.gestion_own_maires.show' @endphp
                                    @elseif(Auth::user()->role=='chefQuartier')
                                        @php $route = 'chefquartier.gestion_own_chef_quartiers.show' @endphp
                                    @elseif(Auth::user()->role=='chefSecteur')
                                        @php $route = 'chefsecteur.home' @endphp
                                    @elseif(Auth::user()->role=='proprietaire')
                                        @php $route = 'proprietaire.home' @endphp
                                    @endif
                                        <a href="{{route($route, Auth::user()->id)}}" class="btn btn-default btn-flat">{{ trans('adminlte_lang::message.profile') }}</a>
                                </div>
                                <div class="pull-right">
                                    @php

                                        if ($user->role=='root'){
                                            $url = "/root/logout";
                                        }else if($user->role=='superadmin'){
                                            $url = "/superadmin/logout";
                                        }else if($user->role=='admin'){
                                            $url = "/admin/logout";
                                        }else if($user->role=='maire'){
                                            $url = "/maire/logout";
                                        }else if($user->role=='chefQuartier'){
                                            $url = "/chefquartier/logout";
                                        }else if($user->role=='chefSecteur'){
                                            $url = "/chefsecteur/logout";
                                        }else if($user->role=='proprietaire'){
                                            $url = "/proprietaire/logout";
                                        }
                                    @endphp

                                    <a href="{{url($url)}}" class="btn btn-default btn-flat" id="logout"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        {{ trans('adminlte_lang::message.signout') }}
                                    </a>

                                    <form id="logout-form" action="{{url($url)}}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="submit" value="logout" style="display: none;">
                                    </form>

                                </div>
                            </li>
                        </ul>
                    </li>
            @endif

            <!-- Control Sidebar Toggle Button -->

            </ul>
        </div>
    </nav>
</header>
