<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        @if (!Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{$user->role =='root' ? '/storage/images/users/default.jpg' : asset($user->picture)}}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{$user->role =='root' ? 'ROOT' : $user->lastName}}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif
    <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        @if($user->role=='root')
        <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Panel du root</li>

                <li class="treeview">
                    <a href="#"><i class='fa   fa-hand-stop-o '></i> <span>Confirmation de comptes<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('/root/home/supadmins')}}">DG BCF</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class='fa  fa-thumbs-o-up '></i> <span>Comptes confirmés<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('/root/home/supadmins/confirmes')}}">DG BCF</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class='fa  fa-home '></i> <span>Gestions des aglomérations<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="#">Régions</a></li>
                        <li><a href="#">Préfecture/Ville</a></li>
                        <li><a href="#">Communes Urbaines</a></li>
                        <li><a href="#">Quartier</a></li>
                        <li><a href="#">Secteur</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class='fa  fa-bank '></i> <span>Gestions des terrains<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="#">Patrimoine de l'Etat</a></li>
                        <li><a href="#">Particulier</a></li>
                        <li><a href="#">Ambassade et consulat</a></li>
                        <li><a href="#">Vente par particulier</a></li>
                        <li><a href="#">Vente par état</a></li>
                    </ul>
                </li>

            </ul><!-- /.sidebar-menu -->
        @endif
        @if($user->role=='superadmin')
        <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Panel du super administrateur</li>
                <li><a href="{{route('superadmin.gestion_own_superadmins.show', Auth::user())}}"><span class="fa fa-home"></span> Mon profile</a></li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-square '></i> <span>DG DOCAD<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('superadmin.gestion_admins.index')}}">Confirmation d'administrateurs</a></li>
                        <li><a href="{{route('superadmin.adminsConfirmes')}}"> Administrateurs préfectorales <span class="fa fa-thumbs-o-up"></span></a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-square-o '></i> <span>Titre Foncier<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('superadmin.bureau_conservation_fonciere.index')}}">Liste d'attente</a></li>
                        <li><a href="{{route('superadmin.superAdminConfirmTerrain')}}"> Titre confirmé <span class="fa fa-thumbs-o-up"></span></a></li>
                    </ul>
                </li>


            </ul><!-- /.sidebar-menu -->
        @endif
        @if($user->role=='admin')
        <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Panel de l'administrateur {{Auth::user()->prefecture_id==1 ? 'de la Capitale' : 'Prefectoral'}}</li>

                <li><a href="{{route('admin.gestion_own_admins.show', Auth::user())}}"><span class="fa fa-home"></span> Mon profile</a></li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-square '></i> <span>Maires<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('admin.gestion_maires.index')}}">Confirmation de maires</a></li>
                        <li><a href="{{route('admin.mairesConfirmes')}}"> <span class="fa fa-thumbs-o-up"></span> Maires communales</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-square-o '></i> <span>Terrains<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('admin.domaines_cadastre.index')}}"><span class="fa fa-hand-o-up"></span> Verifier un terrain</a></li>
                        <li><a href="{{route('admin.adminConfirmTerrain')}}"> <span class="fa fa-thumbs-o-up"></span> Terrain Verifiés</a></li>
                    </ul>
                </li>

            </ul>
        @endif

        @if($user->role=='maire')
        <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Panel du maire {{Auth::user()->prefecture_id==1 ? 'de la Capitale' : 'Prefectoral'}}</li>
                <li><a href="{{route('maire.gestion_own_maires.show', Auth::user())}}"><span class="fa fa-home"></span> Mon profile</a></li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-square '></i> <span>Quartiers<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('maire.mairie_quartier.create')}}"><span class="fa fa-hand-o-up"></span> Ajouter un quartier</a></li>
                        <li><a href="{{route('maire.mairie_quartier.index')}}"> <span class="fa fa-thumbs-o-up"></span>Liste des Quartiers</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-square '></i> <span>Chef Quartiers<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('maire.gestion_chefs_de_quartiers.index')}}"><span class="fa fa-hand-o-up"></span> Confirmer un chef de quartier</a></li>
                        <li><a href="{{route('maire.chefsQuartierConfirmes')}}"> <span class="fa fa-thumbs-o-up"></span>Les Chefs de Quartiers</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-square '></i> <span>Terrains/Parcelles<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('maire.mairie_terrains.index')}}"><span class="fa fa-hand-o-up"></span> Confirmer une parcelle</a></li>
                        <li><a href="{{route('maire.maireConfirmesTerrains')}}"> <span class="fa fa-thumbs-o-up"></span>Liste des parcelles</a></li>
                        <li><a href="{{route('maire.mairePlanDeMasse')}}"> <span class="fa fa-thumbs-o-up"></span>Ajouter un plan de masse</a></li>
                        <li><a href="{{route('maire.mairePlanDeMasseModify')}}"> <span class="fa fa-thumbs-o-up"></span>Modifier un plan de masse</a></li>
                    </ul>
                </li>

            </ul><!-- /.sidebar-menu -->
        @endif

        @if($user->role=='chefQuartier')
        <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Panel du chef de quartier </li>
                <li><a href="{{route('chefquartier.gestion_own_chef_quartiers.show', Auth::user())}}"><span class="fa fa-home"></span> Mon profile</a></li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-square '></i> <span>Secteurs<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('chefquartier.gestion_des_secteurs.create')}}"><span class="fa fa-hand-o-up"></span> Ajouter un secteur</a></li>
                        <li><a href="{{route('chefquartier.gestion_des_secteurs.show', Auth::user())}}"><span class="fa fa-thumbs-o-up"></span> Liste de vos secteur</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-circle '></i> <span>Chef Secteurs<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('chefquartier.gestion_chefs_de_secteurs.index')}}"><span class="fa fa-hand-o-up"></span> Confirmer un chef de secteur</a></li>
                        <li><a href="{{route('chefquartier.chefsSecteurConfirmes')}}"> <span class="fa fa-thumbs-o-up"></span>Les Chefs de Secteurs</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-square-o '></i> <span>Terrains/Parcelles<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('chefquartier.gestion_terrains_chef_quartiers.index')}}"> <span class="fa fa-hand-o-up"></span>Confirmer un terrain</a></li>
                        <li><a href="{{route('chefquartier.chefsQuartierConfirmesTerrains')}}"> <span class="fa fa-thumbs-o-up"></span>Liste des parcelles.</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-square-o '></i> <span>Ventes terrains<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('chefquartier.chf_q_gestion_des_ventes.index')}}"> <span class="fa fa-hand-o-up"></span>Confirmer une vente</a></li>
                    </ul>
                </li>
            </ul><!-- /.sidebar-menu -->
        @endif

        @if($user->role=='chefSecteur')
        <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Panel du chef de quartier </li>
                <li><a href="{{route('chefsecteur.gestion_own_chef_secteurs.show', Auth::user())}}"><span class="fa fa-home"></span> Mon profile</a></li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-circle '></i> <span>Terrains<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('chefsecteur.gestion_chefs_secteur_terrains.create')}}"><span class="fa  fa-hand-o-right"></span> Ajouter un terrain</a></li>
                        <li><a href="{{route('chefsecteur.gestion_chefs_secteur_terrains.index')}}"><span class="fa  fa-hand-o-right"></span> Liste des terrain en attente</a></li>
                        <li><a href="{{route('chefsecteur.chefsSecteurTerrainsConfirmes')}}"><span class="fa  fa-hand-o-right"></span> Liste des terrain validé.</a></li>
                    </ul>
                </li>
                
                 <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-circle '></i> <span>Proprietaires<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('chefsecteur.secteur_proprietaires.create')}}"><span class="fa  fa-hand-o-right"></span> Ajouter un nouveau proprietaire.</a></li>
                        <li><a href="{{route('chefsecteur.secteur_proprietaires.index')}}"><span class="fa  fa-hand-o-right"></span> Liste des proprietaires</a></li>
                    </ul>
                </li>
                
                 <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-circle '></i> <span>Vente terrains<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('chefsecteur.select_type_acheteur')}}"><span class="fa  fa-hand-o-right"></span> Ajouter un acheteur</a></li>
                        <li><a href="{{route('chefsecteur.gestion_des_ventes.index')}}"><span class="fa  fa-hand-o-right"></span> Liste des terrains en vente</a></li>
                        <li><a href="{{route('chefsecteur.terrains_attentes_pro')}}"><span class="fa  fa-hand-o-right"></span> Propriétaire confirmation en attente</a></li>
                        <li><a href="{{route('chefsecteur.terrains_confirm_pro')}}"><span class="fa  fa-hand-o-right"></span> Propriétaire ayant confirmés</a></li>
                    </ul>
                </li>
            </ul>
        @endif

        @if($user->role=='proprietaire')
        <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Panel du propriétaire </li>
                <li><a href="{{route('proprietaire.gestion_own_proprietaire_terrien.show', Auth::user())}}"><span class="fa fa-home"></span> Mon profile</a></li>

                <li class="treeview">
                    <a href="#"><i class='fa  fa-plus-circle '></i> <span>Terrains<i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('proprietaire.gestion_own_proprietaire_terrien.index')}}"><span class="fa  fa-hand-o-right"></span> Confirmer la vente</a></li>
                        <li><a href="{{route('chefsecteur.gestion_chefs_secteur_terrains.index')}}"><span class="fa  fa-hand-o-right"></span> Heritiés</a></li>
                    </ul>
                </li>
            </ul>
        @endif
    </section>
    </section>
    <!-- /.sidebar -->
</aside>
