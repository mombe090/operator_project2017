<!DOCTYPE html>
<!--
Landing page based on Pratt: http://blacktie.co/demo/pratt/
-->
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Adminlte-laravel - {{ trans('adminlte_lang::message.landingdescription') }} ">
    <meta name="author" content="Sergi Tur Badenas - acacha.org">

    {{--  <meta property="og:title" content="Adminlte-laravel" />
      <meta property="og:type" content="website" />
      <meta property="og:description" content="Adminlte-laravel - {{ trans('adminlte_lang::message.landingdescription') }}" />
      <meta property="og:url" content="http://demo.adminlte.acacha.org/" />
      <meta property="og:image" content="http://demo.adminlte.acacha.org/img/AcachaAdminLTE.png" />
      <meta property="og:image" content="http://demo.adminlte.acacha.org/img/AcachaAdminLTE600x600.png" />
      <meta property="og:image" content="http://demo.adminlte.acacha.org/img/AcachaAdminLTE600x314.png" />
      <meta property="og:sitename" content="demo.adminlte.acacha.org" />
      <meta property="og:url" content="http://demo.adminlte.acacha.org" />
  --}}
    {{--  <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@acachawiki" />
      <meta name="twitter:creator" content="@acacha1" />--}}

    <title>MTN Yello Care Challenge.</title>

    <!-- Custom styles for this template -->
    <link href="{{ asset('/css/all-landing.css') }}" rel="stylesheet">
    @include('adminlte::layouts.partials.htmlheader')

    {{-- <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
     <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>--}}

</head>

<body data-spy="scroll" data-target="#navigation" data-offset="50">
<?php
use App\Commune;use App\Prefecture;use App\Region;

$regions = Region::orderBy('name')->get();
$prefectures = Prefecture::orderBy('nom')->get();
$communes = Commune::orderBy('name')->get();
?>
<div id="app" v-cloak>
    <!-- Fixed navbar -->
    <div id="navigation" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href=""><b>MTN</b></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#home" class="smoothScroll">{{ trans('adminlte_lang::message.home') }}</a></li>
                    <li><a href="#desc" class="smoothScroll">Trouver un administrateur.</a></li>
                    <li><a href="#showcase" class="smoothScroll">Chercher un terrain.</a></li>
                    <li><a href="#contact" class="smoothScroll">Nous écrire</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{ route('connexionType') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                        <li><a href="{{ route('InscriptionType') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    @else
                        <li><a href="/home">{{ Auth::user()->name }}</a></li>
                    @endif
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>


    <section id="home" name="home">
        <div id="headerwrap">
            <div class="container">
                <div class="row centered">
                    <div class="col-lg-12">
                        <h1>MTN <b><a href="{{env('APP_URL')}}">Yello Care Challenge</a></b></h1>
                        <h3>Est <a href="{{env('APP_URL')}}">une application révelotionnaire</a> de gestion des terrains/parcelles appartenant aux particuliers ou à l'Etat dans toute la République
                            de Guinée (de Kaloum à Lola).
                        </h3>
                    </div>
                    <div class="col-lg-2">
                        <h5>Plus de conflits dommaniales en 224</h5>
                        <p>Gérer vos terrains de manière très sécurisés.</p>
                        <img class="hidden-xs hidden-sm hidden-md" src="{{ asset('/img/arrow1.png') }}">
                    </div>
                    <div class="col-lg-8">
                        <img class="img-responsive" src="{{ asset('/img/app-bg.png') }}" alt="">
                    </div>
                    <div class="col-lg-2">
                        <br>
                        <img class="hidden-xs hidden-sm hidden-md" src="{{ asset('/img/arrow2.png') }}">
                        <h5>Personne ne pourra vendre plus d'une fois un terrain en Guinée.</h5>
                        <p>Consulter  <a href="{{env('APP_URL')}}"> tous vos</a> terrains à travers toute<a href="{{env('APP_URL')}}"> la guinée</a>et ce depuis n'importe quel endroit au monde.</p>
                    </div>
                </div>
            </div> <!--/ .container -->
        </div><!--/ #headerwrap -->
    </section>

    <section id="desc" name="desc">
        <!-- INTRO WRAP -->
        <div id="intro">
            <div class="container">
                <div class="row centered">
                    <h1>Conçu pour vous, l'application vous permet de retrouver les informations de n'importe qu'elle administrateur.</h1>
                    <br>
                    <br>
                    <form action="{{route('seachAdmins')}}" method="get">
                        {{csrf_field()}}
                        <div class="form-group col-lg-2">
                            <label for="">Administrateurs</label>
                            <select name="admin" id="" class="form-control">
                                <option value="superadmins">Directeurs BCF</option>
                                <option value="admins">Directeurs DO-CAD</option>
                                <option value="maires">Maires</option>
                                <option value="Chefquartiers">Chef Quariters</option>
                                <option value="Chefsecteurs">Chef Secteurs</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-2">
                            <label for="">Regions</label>
                            <select name="region_id" id="" class="form-control">
                                <option value="">Faites-le choix</option>
                                @foreach($regions as $region)
                                    <option value="{{$region->id}}">{{$region->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-lg-2">
                            <label for="">Préfectures</label>
                            <select name="prefecture_id" id="" class="form-control">
                                <option value="">Faites le choix</option>
                                @foreach($prefectures as $prefecture)
                                    <option value="{{$prefecture->id}}">{{$prefecture->nom}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-lg-2">
                            <label for="">Communes</label>
                            <select name="prefecture_id" id="" class="form-control">
                                <option value="">Faites le choix</option>
                                @foreach($communes as $commune)
                                    <option value="{{$commune->id}}">{{$commune->name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group col-lg-2">
                            <label for="">Rechercher <span class="fa fa-search"></span></label>
                            <input type="submit" name=" " value="Trouver le" id="" class="btn btn-primary form-control fa fa-search">
                        </div>
                    </form>
                </div>
                <br>
                <hr>
            </div> <!--/ .container -->
        </div><!--/ #introwrap -->

        <!-- FEATURES WRAP -->
        <div id="features">
            <div class="container">
                <div class="row">
                    <h1 class="centered">La révolution numérique la Guinée s'y met aussi.</h1>
                    <br>
                    <br>
                    <div class="col-lg-6 centered">
                        <img class="centered" src="{{ asset('/img/mobile.png') }}" alt="">
                    </div>

                    <div class="col-lg-6">
                        <h3>{{ trans('adminlte_lang::message.features') }}</h3>
                        <br>
                        <!-- ACCORDION -->
                        <div class="accordion ac" id="accordion2">
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                        Diminuer les conflits dommaniaux en Guinée, éviter les occupations anarchique des domaines de l'Etat, empêcher que des familles soient mise à la rue du jour au lendemain etc...
                                    </a>
                                </div><!-- /accordion-heading -->
                                <div id="collapseOne" class="accordion-body collapse in">
                                    <div class="accordion-inner">
                                        <p>
                                            Aujourd'hui dans notre pays, on remarque la vente multiple d'un terrain à plusieurs personnes par des propriétaires verreux,
                                            même à la vente des dommaines de l'Etat, les tribunaux sont innondés de dossiers de conflits liés à ces parcelles et si avec
                                            la révolution numérique on pouvais stoppé célà ?
                                        </p>
                                    </div><!-- /accordion-inner -->
                                </div><!-- /collapse -->
                            </div><!-- /accordion-group -->
                            <br>

                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                        Fonctionnalité qui pourrons être rajouter
                                    </a>
                                </div>
                                <div id="collapseTwo" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <p>
                                        <h3>
                                            Une fois que la base de données est mise en place tous les administrateurs sont connus du ministère jusqu'au petit chef secteur,
                                            d'autres fonctionnalités pourrons êtres rajouter.
                                        </h3>
                                        Par exemple : à chaque décès ou naissances dans un secteur, le chef secteur informé rajoute à la base de données et les statistiques sont reçu à tous les niveau du
                                        secteur jusqu'au niveau national.
                                        </p>
                                    </div><!-- /accordion-inner -->
                                </div><!-- /collapse -->
                            </div><!-- /accordion-group -->
                            <br>


                        </div><!-- Accordion -->
                    </div>
                </div>
            </div><!--/ .container -->
        </div><!--/ #features -->
    </section>

    <section id="showcase" name="showcase">
        <div id="showcase">
            <div class="container">
                <div class="row">
                    <h1 class="centered">Trouver un terrain en vente dans une localité .</h1>
                    <br>
                    <div class="col-lg-6 col-lg-offset-3">
                        <div id="carousel-example-generic" class="carousel slide">
                            <form action="{{route('seachTerrains')}}" method="post">
                                {{csrf_field()}}
                                <div class="form-group col-md-8">
                                    <input type="text" name="name" id="" class="form-control" placeholder="Donner le nom de la localité">
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="submit" name="" id="" class="form-control btn btn-info fa fa-search" value="Rechercher">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    @if(isset($terrains))
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr class="bg-light-blue">
                                    <th>code secteur</th>
                                    <th>Position</th>
                                    <th>forme</th>
                                    <th>Dimensions</th>
                                    <th>Utilisation</th>
                                    <th>Construit</th>
                                    <th>Proprietaire</th>
                                    <th>Telephone</th>

                                </tr>
                                @if($terrains->count()==0)
                                            <h3 class="text-red text-center bg-danger">Aucun résultat correspond à votre recherche.</h3>
                                        @else
                                    @foreach($terrains as $terrain)
                                        <tr class="" style="background-color: #ffffff">
                                            <td>{{$terrain->codeSecteur}}</td>
                                            <td>{{$terrain->nom}} dans le quartiers {{$terrain->quartier}}</td>
                                            <td>{{$terrain->forme}}</td>
                                            <td>{{$terrain->longer}} * {{$terrain->larger}} <span class="text-red">m*m</span></td>
                                            <td>{{$terrain->usage}}</td>
                                            <td>{{$terrain->build=='yes' ? 'OUI' : 'NON'}}</td>
                                            <td>{{$terrain->lastName}} {{$terrain->firstName}}</td>
                                            <td>{{$terrain->telephone}}</td>
                                        </tr>
                                    @endforeach
                                @endif

                            </table>
                            {{--  {{$terrains->links()}}--}}
                        </div>
                    @endif
                </div>
                <br>
                <br>
                <br>
            </div><!-- /container -->
        </div>
    </section>

    <section id="contact" name="contact">
        <div id="footerwrap">
            <div class="container">
                <div class="col-lg-5">
                    <h3>{{ trans('adminlte_lang::message.address') }}</h3>
                    <p>
                        Av. Greenville 987,<br/>
                        New York,<br/>
                        90873<br/>
                        United States
                    </p>
                </div>

                <div class="col-lg-7">
                    <h3>{{ trans('adminlte_lang::message.dropus') }}</h3>
                    <br>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                           {{session('success')}}
                        </div>
                    @endif
                    <form action="{{route('send_feed_back')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group ">
                            <label for="name1">{{ trans('adminlte_lang::message.yourname') }}</label>
                            <input type="name" name="name" class="form-control" id="name1" placeholder="{{ trans('adminlte_lang::message.yourname') }}">
                        </div>
                        <div class="form-group">
                            <label for="email1">{{ trans('adminlte_lang::message.emailaddress') }}</label>
                            <input type="email" name="email" class="form-control" id="email1" placeholder="{{ trans('adminlte_lang::message.enteremail') }}">
                        </div>
                        <div class="form-group">
                            <label>{{ trans('adminlte_lang::message.yourtext') }}</label>
                            <textarea class="form-control" name="msg" rows="3"></textarea>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-large btn-success">{{ trans('adminlte_lang::message.submit') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div id="c">
            <div class="container">
                <p>
                    <strong>Copyright &copy; 2015 <a href="http://acacha.org">Acacha.org</a>.</strong> Developpé par <b>Diallo</b> Mamadou Yaya
                </p>

            </div>
        </div>
    </footer>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ url (mix('/js/app-landing.js')) }}"></script>
<script>
    $('.carousel').carousel({
        interval: 3500
    })
</script>
@include('flashy::message')
</body>
</html>
