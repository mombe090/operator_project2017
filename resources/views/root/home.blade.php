@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    Tableau de Board du Root
@endsection
@section('contentheader_title')
    <span class="text-red">Vous êtes connecté en tant que ROOT, faites gaf.</span>
@endsection

@section('main-content')
    <!-- Main content -->
    <section class="content">
        <!-- Affichers les utilisateurs -->
        <div class="row margin-bottom">
            <div class="col-lg-2 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$superAdmins->where('working', 'on')->count()}}</h3>

                        <p>Directeurs BCF Inscrit et Verifiés</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="" class="small-box-footer">Afficher la liste <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$Admins->where('working', 'on')->count()}}</h3>

                        <p>DG DOCAD Inscrits et verifés</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$Maires->where('working', 'on')->count()}}</h3>

                        <p>Liste des Maires Inscrits et verifés</p>
                    </div>
                    <div class="icon">
                        <i class="glyphicon glyphicon-user"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$ChefQuartiers->where('working', 'on')->count()}}</h3>

                        <p>Chef Quartiers Inscrits et verifés</p>
                    </div>
                    <div class="icon">
                        <i class="glyphicon glyphicon-user"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>{{$ChefSecteurs->where('working', 'on')->count()}}</h3>

                        <p>Chef Secteurs Inscrits et verifés</p>
                    </div>
                    <div class="icon">
                        <i class="glyphicon glyphicon-user"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-2 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-maroon-active">
                    <div class="inner">
                        <h3>{{$Proprietaires->where('titreFoncier', '!=', null)->count()}}</h3>

                        <p>Propriétaires ayant un titre.</p>
                    </div>
                    <div class="icon">
                        <i class="glyphicon glyphicon-user"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <div class="row margin-bottom">
            <div class="col-lg-2 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$superAdmins->where('working', 'off')->count()}}</h3>

                        <p>Directeurs BCF Inscrit et non veirifiés</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="" class="small-box-footer">Afficher la liste <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$Admins->where('working', 'off')->count()}}</h3>

                        <p>DG DOCAD Inscrits et non verifés</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$Maires->where('working', 'off')->count()}}</h3>

                        <p>Liste des Maires Inscrits non verifés</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$ChefQuartiers->where('working', 'off')->count()}}</h3>

                        <p>Chef Quartiers Inscrits non verifés</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$ChefSecteurs->where('working', 'off')->count()}}</h3>

                        <p>Chef Secteurs Inscrits et verifés</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-2 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-maroon-active">
                    <div class="inner">
                        <h3>{{$Proprietaires->where('titreFoncier',  null)->count()}}</h3>

                        <p>Propriétaires n'ayant pas de titre</p>
                    </div>
                    <div class="icon">
                        <i class="glyphicon glyphicon-user"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>

        {{--Afficher les aglomeration . mombesoft--}}
        <div class="row margin-top">
            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Regions Administratives</span>
                        <span class="info-box-number">{{$regions}}</span>
                    </div>
                    <p class="text-center">
                        <a href="">Afficher la liste</a>
                    </p>
                    <!-- /.info-box-content -->
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Préféctures</span>
                        <span class="info-box-number">{{$prefectures -1 }} + 1</span>
                    </div>
                    <p class="text-center">
                        <a href="">Afficher la liste</a>
                    </p>
                    <!-- /.info-box-content -->
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Communes</span>
                        <span class="info-box-number">{{$communes}}</span>
                    </div>
                    <p class="text-center">
                        <a href="">Afficher la liste</a>
                    </p>
                    <!-- /.info-box-content -->
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Quartiers</span>
                        <span class="info-box-number">{{$quartiers}}</span>
                    </div>
                    <p class="text-center">
                        <a href="">Afficher la liste</a>
                    </p>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Secteurs</span>
                        <span class="info-box-number">{{$secteurs}}</span>
                    </div>
                    <p class="text-center">
                        <a href="">Afficher la liste</a>
                    </p>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-home"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Terrains</span>
                        <span class="info-box-number">{{$terrains}}</span>
                    </div>
                    <p class="text-center">
                        <a href="">Afficher la liste</a>
                    </p>
                    <!-- /.info-box-content -->
                </div>
            </div>
        </div>


    </section>






    <!-- /.content -->
@endsection