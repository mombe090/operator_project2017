@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    <span class="text-info">Modification des information du super administrateur <i class="text-warning">{{$superAdmin->lastName}} {{$superAdmin->firstName }}</i></span>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-11 col-md-offset-1">

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-5 col-xs-12">
                            <!-- general form elements -->
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Informations actuellement stockées</h3>
                                    <p class="center-block">
                                        <a href="{{asset($superAdmin->picture)}}">
                                            <img class="profile-user-img img-md " src="{{asset($superAdmin->picture)}}" alt="Image du profile">
                                        </a>
                                    </p>
                                </div>
                                <div class="box-body">
                                    <p>
                                        <b class="text-primary">Nom : </b> <span class="text-uppercase">{{$superAdmin->lastName}}</span>
                                    </p>
                                    <hr>
                                    <p>
                                        <b class="text-primary">Prénom : </b> <span class="text-capitalize">{{$superAdmin->firstName}}</span>
                                    </p>
                                    <hr>

                                    <p>
                                        <b class="text-primary">N° Carte d'identité national : </b> <span class="text-uppercase">{{$superAdmin->nationalIdentity}}</span>
                                    </p>
                                    <hr>
                                    <p>
                                        <b class="text-primary">email : </b> <span class="text-capitalize">{{$superAdmin->email}}</span>
                                    </p>
                                    <hr>

                                    <p>
                                        <b class="text-primary">Telephone : </b> <span class="text-uppercase">{{$superAdmin->telephone}}</span>
                                    </p>
                                    <hr>
                                    <p>
                                        <b class="text-primary">Date de naissance : </b> <span class="text-capitalize">{{$superAdmin->dateOfBirth}}</span>
                                    </p>
                                    <hr>
                                    <p>
                                        <b class="text-primary">Date d'embauchement au compte de la région de  <b class="text-primary text-uppercase">{{$superAdmin->region->name}}</b> : </b> <span class="text-capitalize">{{$superAdmin->hireDate}}</span>
                                    </p>
                                </div>
                            </div>

                        </div>
                        <!--/.col (left) -->
                        <!-- right column -->
                        <div class="col-md-7 col-xs-12">
                            <!-- Horizontal Form -->
                            <div class="box box-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Mise à jour.</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->

                                <form action="{{url('root/update/profile/superadmins', $superAdmin->id)}}" class="form-horizontal" method="post">
                                    {{method_field('put')}}
                                    {{csrf_field()}}

                                    <div class="box-body">
                                        <div class="form-group {{ $errors->has('lastName') ? ' has-error' : '' }}">
                                            <label for="lastName" class="col-sm-4 text-primary control-label">Nom</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" id="lastName" value="{{old('lastName') ? old('lastName') : $superAdmin->lastName}}">
                                                @if ($errors->has('lastName'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('lastName') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('firstName') ? ' has-error' : '' }}">
                                            <label for="firstName" class="col-sm-4 text-primary control-label">Prénom</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" id="firstName" value="{{old('firstName') ? old('firstName') : $superAdmin->firstName}}">
                                                @if ($errors->has('firstName'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('firstName') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group {{ $errors->has('nationalIdentity') ? ' has-error' : '' }}">
                                            <label for="nationalIdentity" class="col-sm-4 text-primary control-label">N° D'identité</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" id="nationalIdentity" value="{{old('nationalIdentity') ? old('nationalIdentity') : $superAdmin->nationalIdentity}}">
                                                @if ($errors->has('nationalIdentity'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('nationalIdentity') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-sm-4 text-primary control-label">Email</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" id="email" value="{{old('email') ? old('email') : $superAdmin->email}}">
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('email') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                            <label for="telephone" class="col-sm-4 text-primary control-label">N° Telephone</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" id="telephone" value="{{old('telephone') ? old('telephone') : $superAdmin->telephone}}">
                                                @if ($errors->has('telephone'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('telephone') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('dateOfBirth') ? ' has-error' : '' }}">
                                            <label for="dateOfBirth" class="col-sm-4 text-primary control-label">Date Naissance</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" id="dateOfBirth" value="{{old('dateOfBirth') ? old('dateOfBirth') : $superAdmin->dateOfBirth}}">
                                                @if ($errors->has('dateOfBirth'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('dateOfBirth') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('hireDate') ? ' has-error' : '' }}">
                                            <label for="hireDate" class="col-sm-4 text-primary control-label">Début de travail</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" id="hireDate" value="{{old('hireDate') ? old('hireDate') : $superAdmin->hireDate}}">
                                                @if ($errors->has('hireDate'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('hireDate') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group {{ $errors->has('region_id') ? ' has-error' : '' }}">
                                            <label for="region_id" class="col-sm-4 text-primary control-label">Régions</label>

                                            <div class="col-sm-8">
                                                <select name="region_id" id="region_id" class="form-control">
                                                    @foreach($regions as $region)

                                                        <option value="{{$region->id}}"  {{$superAdmin->region->id == $region->id ? 'selected' : ''}}>{{$region->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('region_id'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('region_id') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        @if(Auth::user()->role='root')
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-8">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Remember me
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">

                                        <button type="submit" class="btn btn-info pull-right">Valider</button>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>

                        </div>
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->

            </div>
        </div>
    </div>
@endsection
