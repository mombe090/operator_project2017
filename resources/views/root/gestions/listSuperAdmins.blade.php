@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    <span class="text-info">Liste des super administrateurs {{isset($confirm) ? 'Confirmés' : 'en attente de votre confirmation.'}}</span>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-success">
                                    <h3 class="box-title text-center text-bold">Concernés</h3>
                                </div>
                                <!-- /.box-header -->
                                @if($superAdmins->count()==0)

                                    <H3 class="text-center text-danger">Il n'y a pas d'inscription de Directeurs du BCF en attente de confirmation.</H3>

                                @else
                                    <div class="box-body">
                                        <table id="example2" class="datatables table table-bordered table-hover table-responsive">
                                            <thead>
                                            <tr>
                                                <th class="">NOM</th>
                                                <th>Prénom</th>
                                                <th class="hidden-xs">email</th>
                                                <th class="hidden-xs">N° D'identité</th>
                                                <th class="hidden-xs">Region</th>
                                                <th>Photo</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            @foreach($superAdmins as $superAdmin)
                                                <tr>
                                                    <td class="text-uppercase">{{$superAdmin->lastName}}</td>
                                                    <td>{{$superAdmin->firstName}}</td>

                                                    <td class="hidden-xs">{{$superAdmin->email}}</td>
                                                    <td class="hidden-xs">{{$superAdmin->nationalIdentity}}</td>
                                                    <td class="hidden-xs">{{$superAdmin->region->name}}</td>
                                                    <td>
                                                        <p>
                                                            <a href="{{asset($superAdmin->picture)}}" title="Afficher la photo du super administrateur."> <img src="{{asset($superAdmin->picture)}}" alt="Image de super utilisateur" class="img-sm img-circle img-bordered-sm"></a>
                                                        </p>
                                                    </td>
                                                    <td>

                                                        <a href="{{url("/root/show/profile/superadmins", $superAdmin->id)}}" class="btn btn-xs btn-info" title="Afficher toutes les informations du super utilisateur"><span class="fa fa-eye"></span></a>


                                                        @if(isset($confirm))
                                                            <a href="{{ url("root/edit/profile/superadmins", $superAdmin->id)}}" class="btn btn-xs btn-warning" title="confirmation  du super utilisateur"><span class="fa fa-edit"></span></a>
                                                            @else
                                                            <a href="{{ url("/root/confirm/superadmins", $superAdmin->id)}}" class="btn btn-xs btn-success" title="confirmation  du super utilisateur"><span class="fa fa-thumbs-o-up"></span></a>
                                                        @endif


                                                        <a href="{{url("/root/delete/profile/superadmins", $superAdmin->id)}}" class="btn btn-xs btn-danger" title="Supression du super utilisateur" onclick="return confirm('Etes-vous sûre de vouloir supprimer ce super administrateur ?')"><span class="fa fa-eye"></span></a>


                                                        {{--
                                                        <a href="{{url("/root/show/profile/superadmins", $superAdmin->id)}}" class="btn btn-xs btn-info" title="Afficher toutes les informations du super utilisateur"><span class="fa fa-eye"></span></a>

                                                        <a href="{{url("/root/edit/profile/superadmins", $superAdmin->id)}}" class="btn btn-xs btn-warning" title="Editer les informations du super utilisateur"><span class="fa fa-eye"></span></a>

                                                        <a href="{{url("/root/delete/profile/superadmins", $superAdmin->id)}}" class="btn btn-xs btn-danger" title="Supression du super utilisateur" onclick="return confirm('Etes-vous sûre de vouloir supprimer ce super administrateur ?')"><span class="fa fa-eye"></span></a>
                                                        --}}

                                                    </td>

                                                </tr>
                                            @endforeach
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                            {{ $superAdmins->links() }}
                                            </tfoot>
                                        </table>
                                    </div>
                            @endif

                            <!-- /.box-body -->
                            </div>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->

            </div>
        </div>
    </div>
@endsection
