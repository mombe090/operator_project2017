@extends('adminlte::layouts.app')

@section('htmlheader_title')
    MTN Profile de l'administrateur.
@endsection
@section('contentheader_title')
    Vous êtes connecté en tant que Directeur Général de la DOCAD de la ville {{Auth::user()->prefecture->nom}}
@endsection

@section('main-content')

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            @if(Auth::user()->prefecture->code=='capitale')
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-black-active">
                        <div class="inner">
                            <h3>{{$communes}}</h3>

                            <p>Communes</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red-gradient">
                        <div class="inner">
                            <h3>{{$mairesVerifies ->where('working', 'on')->count()}}</h3>

                            <p>Maires Confirmés</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3>{{$mairesVerifies ->where('working', 'off')->count()}}</h3>

                            <p>Maires en Attente </p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            @endif
        </div>
        <div class="row">

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$quartiers}}</h3>

                        <p>Quartiers</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$chefQuartiers->where('working', 'on')->count()}}</h3>

                        <p>Chefs quartiers verifiés</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-maroon-active">
                    <div class="inner">
                        <h3>{{$chefQuartiers->where('working', 'off')->count()}}</h3>

                        <p>Chefs quartiers en attente.</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>
                            {{$parcelles->where('verifiedByAdmin', 'yes')->count() }}
                        </h3>

                        <p>Parcelles verifiés</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow-active">
                    <div class="inner">
                        <h3>
                            {{$parcelles->where('verifiedByAdmin', 'no')->count() }}
                        </h3>

                        <p>Parcelles en attente</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>  {{$parcelles->where('verifiedByAdmin', 'yes')->where('dommaine', 'etat')->count() }} </h3>

                        <p>Domaines de l'Etat</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>



        </div>
        <div class="row">

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$parcelles->where('verifiedByAdmin', 'yes')->where('dommaine', 'ambassade')->count() }}</h3>

                        <p>Embassades</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-maroon-active">
                    <div class="inner">
                        <h3>{{$parcelles->where('verifiedByAdmin', 'yes')->where('dommaine', 'prive')->where('usage', 'habitation')->count() }}</h3>

                        <p>Habitation Privé</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>


            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-fuchsia-active">
                    <div class="inner">
                        <h3>{{$parcelles->where('verifiedByAdmin', 'yes')->where('dommaine', 'etat')->where('usage', 'hopital')->count() }}</h3>

                        <p>Hôpitaux</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$parcelles->where('verifiedByAdmin', 'yes')->where('dommaine', 'etat')->where('usage', 'ecole')->count() }}</h3>

                        <p>Ecoles Publiques</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-teal-active">
                    <div class="inner">
                        <h3>{{$parcelles->where('verifiedByAdmin', 'yes')->where('dommaine', 'prive')->where('usage', 'ecole')->count() }}</h3>

                        <p>Ecoles Privées</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3>
                            {{$parcelles->where('verifiedByAdmin', 'yes')->where('dommaine', 'etat')->where('usage', 'mosque')->count() }}
                        </h3>

                        <p>Mosquées</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-purple-active">
                    <div class="inner">
                        <h3>{{$parcelles->where('verifiedByAdmin', 'yes')->where('dommaine', 'etat')->where('usage', 'eglise')->count() }}</h3>

                        <p>Eglises</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>



        </div>


    </section>
@endsection
