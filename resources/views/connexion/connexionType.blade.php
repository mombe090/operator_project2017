@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
    <body class="hold-transition login-page">
    <div id="app" v-cloak>
        <div class="login-box">
            <div class="login-logo">
                <a href="{{ url('/home') }}"><b>MTN</b>Yello Care Challenge</a>
            </div><!-- /.login-logo -->



            <div class="login-box-body">
                <p class="login-box-msg"> Choisissez votre type de compte </p>
                <form action="{{ route('connexionChoosed') }}" method="get">

                   {{csrf_field()}}

                    <div class="form-group {{ $errors->has('role') ? ' has-error' : '' }}" >
                        <label for="role">Connectez-vous en tant que :</label>
                        <select name="role" id="role" class="form-control">
                            <option value="Root">DG DATU </option>
                            <option value="SuperAdmin">DG BCF </option>
                            <option value="Admin">DG DO-CAD </option>
                            <option value="Maire">Maire</option>
                            <option value="ChefQuartier">Chef de quartier</option>
                            <option value="ChefSecteur">Chef secteur</option>
                            <option value="landOwner">Propriétaire Terrien</option>
                        </select>
                        @if ($errors->has('role'))
                            <span class="help-block">
                                <strong>{{ $errors->first('role') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="row">


                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Continuer</button>
                        </div><!-- /.col -->

                    </div>
                </form>



            </div><!-- /.login-box-body -->

        </div><!-- /.login-box -->
    </div>
    @include('adminlte::layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    @include('flashy::message')
    </body>

@endsection