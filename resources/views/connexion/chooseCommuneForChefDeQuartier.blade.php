@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
    <body class="hold-transition login-page">
    <div id="app" v-cloak>
        <div class="login-box">
            <div class="login-logo">
                <a href="{{ url('/home') }}"><b>MTN</b>Yello Care Challenge</a>
            </div><!-- /.login-logo -->



            <div class="login-box-body">
                <p class="login-box-msg"> Veillez Choisir la commune SVP </p>
                <form action="{{  url('chefquartier/register/add') }}" method="get">

                   {{csrf_field()}}

                    <div class="form-group {{ $errors->has('role') ? ' has-error' : '' }}" >
                        <label for="role">Ma commune est : </label>
                        <select name="commune" id="role" class="form-control">
                            @foreach($communes as $commune)
                                <option value="{{$commune->id}}" >{{$commune->name}} </option>
                            @endforeach
                        </select>

                        @if ($errors->has('role'))
                            <span class="help-block">
                                <strong>{{ $errors->first('role') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="row">


                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Continuer</button>
                        </div><!-- /.col -->

                    </div>
                </form>



            </div><!-- /.login-box-body -->

        </div><!-- /.login-box -->
    </div>
    @include('adminlte::layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    @include('flashy::message')
    </body>

@endsection