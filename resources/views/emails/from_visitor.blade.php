@component('mail::message')
Cher admin un utilisateur vien de vous envoyé un message depuis l'application.

Nom : {{$name}} <br>
e-mail : {{$email}}
Contenu
<p>
    {{$msg}}
</p>

{{--@component('mail::button', ['url' => ''])
Button Text
@endcomponent--}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
