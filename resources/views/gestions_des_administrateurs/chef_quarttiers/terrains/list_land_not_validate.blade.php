@php use Carbon\Carbon; @endphp
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que chef secteur.
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="2">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Terrains/Parcelles</h3>
                        <div class="box-tools pull-right ">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">

                                    @if(count($terrains)!=0)
                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">Liste des Parcelles en attente de validation par vous M. <span class="text-bold text-uppercase">{{Auth::user()->lastName}}</span> {{Auth::user()->firstName}}</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="box-body table-responsive no-padding">
                                                    <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th class=" ">Code</th>
                                                            <th class=" ">Propriétaire</th>
                                                            <th class=" ">Dimensions</th>
                                                            <th class=" ">Forme</th>
                                                            <th class="">Dommaine</th>
                                                            <th class="">Utilisation</th>
                                                            <th class=" ">Construit</th>
                                                            <th class=" ">Preuves</th>
                                                            <th class="">Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($terrains as $terrain)
                                                            <tr>
                                                                <td class="text-uppercase text-bold  ">{{$terrain->codeSecteur}} </td>
                                                                <td class="text-uppercase text-bold  ">
                                                                    <a href="{{asset($terrain->picture)}}">
                                                                        <img src="{{asset($terrain->picture)}}" alt="" class="img-sm img-circle">
                                                                    </a>{{$terrain->lastName}} {{$terrain->firstName}} </td>
                                                                <td class=" ">{{$terrain->forme}}</td>
                                                                <td class=" ">{{$terrain->longer * $terrain->larger}} <span class="text-red">m*m</span></td>
                                                                <td class="">{{$terrain->dommaine}}</td>
                                                                <td class="">{{$terrain->usage}}</td>
                                                                <td class=" ">{{$terrain->build=='yes' ? 'Oui' : 'Non'}}</td>
                                                                <td class=" ">
                                                                    <a href="{{asset($terrain->attestationAchat)}}">
                                                                        <img src="{{asset($terrain->attestationAchat)}}" alt="" class="img-sm img-circle" title="Attestation d'Achat">
                                                                    </a>
                                                                    @if($terrain->planDeMasse)
                                                                        <a href="{{asset($terrain->planDeMasse)}}">
                                                                            <img src="{{asset($terrain->planDeMasse)}}" alt="" class="img-sm img-circle" title="Plan de masse">
                                                                        </a>
                                                                    @endif

                                                                    @if($terrain->titreFoncier)
                                                                        <a href="{{asset($terrain->titreFoncier)}}">
                                                                            <img src="{{asset($terrain->titreFoncier)}}" alt="" class="img-sm img-circle" title="Titre Foncier">
                                                                        </a>
                                                                    @endif
                                                                </td>
                                                                <td class="">

                                                                    <a href="{{route('chefquartier.gestion_terrains_chef_quartiers.show', $terrain->id)}}" title="Modifier une terrain/parcelle" class="btn btn-xs btn-info  fa fa-eye"></a>


                                                                    <form style="display: inline-block" action="{{route('chefquartier.gestion_terrains_chef_quartiers.update', $terrain->id)}}" method="post" class="form-inline">
                                                                        {{csrf_field()}}
                                                                        {{method_field('put')}}
                                                                        <input type="hidden" name="confirm">
                                                                        <button type="submit"  title="Supprimer la terrain/parcelle" class="btn btn-xs btn-success fa fa-thumbs-o-up" onclick="return confirm('Voulez-vous vraiment confirmé l\'enregistrement de ce terrain ?')"></button>
                                                                    </form>

                                                                    <form style="display: inline-block" action="{{route('chefquartier.gestion_terrains_chef_quartiers.destroy', $terrain->id)}}" method="post" class="form-inline">
                                                                        {{csrf_field()}}
                                                                        {{method_field('delete')}}
                                                                        <input type="hidden" name="delete_land_not_validate_by_chef_quartier">
                                                                        <button type="submit"  title="Supprimer la terrain/parcelle" class="btn btn-xs btn-danger fa fa-remove" onclick="return confirm('Voulez-vous vraiment suppremer l\'enregistrement de ce terrain ?')"></button>
                                                                    </form>
                                                                </td>

                                                            </tr>
                                                        @endforeach
                                                        </tbody>


                                                    </table>
                                                    <div class="pull-right">
                                                        {{$terrains->links()}}
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    @else
                                        <h2 class="text-red text-center">Il n'y pas de parcelle en attente de votre confirmation.</h2>
                                @endif
                                <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
