@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    <span class="text-info">Modification des information du chef secteur par le chef de quartier M.
        <i class="text-warning">
            <span class="text-uppercase text-bold">{{$chefSecteur->secteur->quartier->chefquartier->lastName}}</span>
            {{$chefSecteur->secteur->quartier->chefquartier->firstName}}
        </i>
    </span>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-11 col-md-offset-1">

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-5 col-xs-12">
                            <!-- general form elements -->
                            <div class="box box-danger">
                                <div class="box-header with-border">
                                    <h3 class="box-title">mise à jour de la photo de profile du maire</h3>
                                </div>

                                <div class="box-body">

                                    <p class="text-center text-red">
                                        Retirez l'accès de ce maire.
                                    </p>
                                    <form role="form" method="post" autocomplete="off" action="{{route('chefquartier.gestion_chefs_de_secteurs.update', $chefSecteur)}}" >
                                        {{csrf_field()}}
                                        {{method_field('put')}}
                                        <input type="hidden" name="retirerAcces" id="">
                                        <div class="box-body">
                                            <input type="submit" class="btn btn-md btn-danger form-control" value="Retirez">
                                        </div>
                                    </form>
                                </div>
                                <div class="box-body">

                                    <form role="form" enctype="multipart/form-data" method="post" autocomplete="off" action="{{route('chefquartier.gestion_chefs_de_secteurs.update', $chefSecteur)}}" >
                                        {{csrf_field()}}
                                        {{method_field('put')}}
                                        <input type="hidden" name="profileUpdate" id="">
                                        <div class="box-body">
                                            <div class="form-group {{ $errors->has('picture') ? ' has-error' : '' }}" >
                                                <input type="file"  id="picture" name="picture" value="" class="form-control" required>
                                                @if ($errors->has('picture'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('picture') }}</strong>
                                                       </span>
                                                @endif
                                            </div>

                                            <input type="submit" class="btn btn-md btn-info form-control" value="Modifer">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Informations actuellement stockées</h3>
                                    <p class="center-block">
                                        <a href="{{asset($chefSecteur->picture)}}">
                                            <img class="profile-user-img img-md " src="{{asset($chefSecteur->picture)}}" alt="Image du profile">
                                        </a>
                                    </p>

                                </div>

                                <div class="box-body">
                                    <p>
                                        <b class="text-primary">Nom : </b> <span class="text-uppercase">{{$chefSecteur->lastName}}</span>
                                    </p>
                                    <hr>
                                    <p>
                                        <b class="text-primary">Prénom : </b> <span class="text-capitalize">{{$chefSecteur->firstName}}</span>
                                    </p>
                                    <hr>

                                    <p>
                                        <b class="text-primary">N° Carte d'identité national : </b> <span class="text-uppercase">{{$chefSecteur->nationalIdentity}}</span>
                                    </p>
                                    <hr>
                                    <p>
                                        <b class="text-primary">email : </b> <span class="text-capitalize">{{$chefSecteur->email}}</span>
                                    </p>
                                    <hr>

                                    <p>
                                        <b class="text-primary">Telephone : </b> <span class="text-uppercase">{{$chefSecteur->telephone}}</span>
                                    </p>
                                    <hr>
                                    <p>
                                        <b class="text-primary">Date de naissance : </b> <span class="text-capitalize">{{$chefSecteur->dateOfBirth}}</span>
                                    </p>
                                    <hr>
                                    <p>
                                        <b class="text-primary">Prise de fonction  au compte du secteur de  <b class="text-primary text-uppercase">{{$chefSecteur->secteur->name}}</b> : </b> <span class="text-capitalize">{{$chefSecteur->hireDate}}</span>
                                    </p>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-7 col-xs-12">
                            <!-- Horizontal Form -->
                            <div class="box box-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Mise à jour.</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->

                                <form action="{{route('chefquartier.gestion_chefs_de_secteurs.update', $chefSecteur)}}" class="form-horizontal" method="post">
                                    {{method_field('put')}}
                                    {{csrf_field()}}
                                    <input type="hidden" name="trueUpdate" value="true">

                                    <div class="box-body">
                                        <div class="form-group {{ $errors->has('lastName') ? ' has-error' : '' }}">
                                            <label for="lastName" class="col-sm-4 text-primary control-label">Nom</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" name="lastName" id="lastName" value="{{old('lastName') ? old('lastName') : $chefSecteur->lastName}}">
                                                @if ($errors->has('lastName'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('lastName') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('firstName') ? ' has-error' : '' }}">
                                            <label for="firstName" class="col-sm-4 text-primary control-label">Prénom</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" name="firstName" id="firstName" value="{{old('firstName') ? old('firstName') : $chefSecteur->firstName}}">
                                                @if ($errors->has('firstName'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('firstName') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group {{ $errors->has('nationalIdentity') ? ' has-error' : '' }}">
                                            <label for="nationalIdentity" class="col-sm-4 text-primary control-label">N° D'identité</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" name="nationalIdentity" id="nationalIdentity" value="{{old('nationalIdentity') ? old('nationalIdentity') : $chefSecteur->nationalIdentity}}">
                                                @if ($errors->has('nationalIdentity'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('nationalIdentity') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-sm-4 text-primary control-label">Email</label>

                                            <div class="col-sm-8">
                                                <input type="email" class="form-control text-warning" name="email" id="email" value="{{old('email') ? old('email') : $chefSecteur->email}}">
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('email') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                            <label for="telephone" class="col-sm-4 text-primary control-label">N° Telephone</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" name="telephone" id="telephone" value="{{old('telephone') ? old('telephone') : $chefSecteur->telephone}}">
                                                @if ($errors->has('telephone'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('telephone') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('dateOfBirth') ? ' has-error' : '' }}">
                                            <label for="dateOfBirth" class="col-sm-4 text-primary control-label">Date Naissance</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" name="dateOfBirth" id="dateOfBirth" value="{{old('dateOfBirth') ? old('dateOfBirth') : $chefSecteur->dateOfBirth}}">
                                                @if ($errors->has('dateOfBirth'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('dateOfBirth') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('hireDate') ? ' has-error' : '' }}">
                                            <label for="hireDate" class="col-sm-4 text-primary control-label">Début de travail</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" name="hireDate" id="hireDate" value="{{old('hireDate') ? old('hireDate') : $chefSecteur->hireDate}}">
                                                @if ($errors->has('hireDate'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('hireDate') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group {{ $errors->has('region_id') ? ' has-error' : '' }}">
                                            <label for="region_id" class="col-sm-4 text-primary control-label">Secteur</label>

                                            <div class="col-sm-8">
                                                <select name="secteur_id" id="secteur_id" class="form-control">
                                                    @foreach($secteurs as $secteur)
                                                        <option value="{{$secteur->id}}"  {{$chefSecteur->secteur->id == $secteur->id ? 'selected' : ''}}>{{$secteur->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('secteur_id'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('secteur_id') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>


                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">

                                        <button type="submit" class="btn btn-info pull-right">Valider</button>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>

                        </div>
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->

            </div>
        </div>
    </div>
@endsection
