@php use Carbon\Carbon; @endphp
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que  chef du secteur {{Auth::user()->quartier->name}}
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="2">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Terrains/Parcelles</h3>
                        <div class="box-tools pull-right ">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">

                                    <div class="box">
                                        <div class="box-header">
                                            <h3 class="box-title">Liste des Parcelles mise en vente par le propriétaire.</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="box-body table-responsive no-padding">
                                                @if($terrains->count()==0)
                                                    <h2 class="text-center text-red">Il n'y a pas de vente confirmé par le propriétaire du terrain.</h2>
                                                @else
                                                    <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th class=" ">Identité</th>
                                                            <th>Nouveau Proprietaire</th>
                                                            <th>Telephone</th>
                                                            <th class=" ">Code</th>
                                                            <th class=" ">Forme</th>
                                                            <th class=" ">Dimensions</th>
                                                            <th class="">Dommaine</th>
                                                            <th class="">Ajouter l'attestation</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($terrains as $terrain)
                                                            <tr>
                                                                <td class="text-uppercase text-bold   text-center">
                                                                    <a href="{{asset($terrain->picture)}}" >
                                                                        <img src="{{asset($terrain->picture)}}"  class="img-sm img-circle " alt=""> {{$terrain->nationalIdentity}}
                                                                    </a>
                                                                </td>
                                                                <td class="">{{$terrain->lastName}} {{$terrain->firstName}}</td>
                                                                <td class=" ">{{$terrain->telephone}}</td>
                                                                <td class="text-uppercase text-bold  ">{{$terrain->codeSecteur}} </td>
                                                                <td class=" ">{{$terrain->forme}}</td>
                                                                <td class=" ">{{$terrain->longer}} * {{$terrain->larger}} <span class="text-red">m*m</span></td>

                                                                <td class="">{{$terrain->dommaine}}</td>
                                                                <td>
                                                                    <form role="form" enctype="multipart/form-data" method="post" autocomplete="off" action="{{route('chefquartier.chf_q_gestion_des_ventes.update', $terrain->terrainID)}}" >
                                                                        {{csrf_field()}}
                                                                        {{method_field('put')}}
                                                                        <div class="box-body">
                                                                            <div class="form-group {{ $errors->has('attestationAchat') ? ' has-error' : '' }}" >
                                                                                <input type="file"  id="attestationAchat" name="attestationAchat" value="choisir une photo" class="form-control" required >
                                                                                @if ($errors->has('attestationAchat'))
                                                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('attestationAchat') }}</strong>
                                                       </span>
                                                                                @endif
                                                                            </div>

                                                                            <input type="submit" class="btn btn-md btn-info form-control" value="Ajouter">
                                                                        </div>
                                                                    </form>
                                                                </td>


                                                            </tr>
                                                        @endforeach
                                                        </tbody>


                                                    </table>
                                                    <div class="pull-right">
                                                        {{$terrains->links()}}
                                                    </div>
                                                @endif
                                            </div>

                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
