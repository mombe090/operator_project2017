@php use Carbon\Carbon; @endphp
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que chef du quartier {{Auth::user()->quartier->name}}
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Quartier/Secteurs</h3>
                        <div class="box-tools pull-right hidden-xs">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">

                                    @if(count($secteurs)!=0)
                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">Liste des secteurs du quartier {{Auth::user()->quartier->name}}</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table id="example1" class="table table-bordered table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th class="hidden-xs col-sm-2">Code</th>
                                                        <th class="col-sm-2">Nom</th>
                                                        <th class="col-sm-2">Chef secteurs</th>
                                                        <th class="col-sm-2">Telephone</th>
                                                        <th class="col-sm-2 hidden-xs">Nombres de parcelles verfiées</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($secteurs as $secteur)
                                                        <tr>
                                                            <td class="text-uppercase text-bold hidden-xs col-sm-2">{{$secteur->code}} </td>
                                                            <td class="col-sm-1">{{$secteur->name}}</td>
                                                            <td class="col-sm-1">
                                                                @if($secteur->chefsecteur['picture']!=null)
                                                                    <a href="{{asset($secteur->chefsecteur['picture'])}}">
                                                                        <img src="{{asset($secteur->chefsecteur['picture'])}}"  class="img-sm img-circle" alt="">
                                                                    </a>
                                                                    @endif
                                                                <b class="text-uppercase">{{$secteur->chefsecteur['lastName']}}</b> {{$secteur->chefsecteur['firstName']}}</td>
                                                            <td class="col-sm-1"><b class="text-uppercase">{{$secteur->chefsecteur['telephone']}}</b></td>
                                                            <td class="col-sm-1 hidden-xs">{{$secteur->terrains->where('verifiedByChefQuartier', 'yes')->count()}}</td>

                                                        </tr>
                                                    @endforeach
                                                    </tbody>


                                                </table>

                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    @else
                                        <h2 class="text-red text-center">Il n'y pas de parcelle valider dans le quartier. Svp veillez exiger à vos chefs secteurs de rajouter des parcelles à la base de donnée.</h2>
                                @endif
                                <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
