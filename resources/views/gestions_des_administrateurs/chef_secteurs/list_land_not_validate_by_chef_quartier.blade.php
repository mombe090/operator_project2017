@php use Carbon\Carbon; @endphp
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que chef secteur.
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="2">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Terrains/Parcelles</h3>
                        <div class="box-tools pull-right ">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">

                                    <div class="box">
                                        <div class="box-header">
                                            <h3 class="box-title">Liste des Parcelles en attente de validation par le chef de quartier.</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="box-body table-responsive no-padding">
                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th class=" ">Identité</th>
                                                        <th class=" ">Code</th>
                                                        <th class=" ">Dimensions</th>
                                                        <th class=" ">Forme</th>
                                                        <th class="">Dommaine</th>
                                                        <th class="">Utilisation</th>
                                                        <th class=" ">Construit</th>
                                                        <th class=" ">Date d'ajout</th>
                                                        <th class="">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    @foreach($terrains as $terrain)
                                                        <tr>
                                                            <td class="text-uppercase text-bold   text-center">
                                                                <a href="{{asset($terrain->proprietaire->picture)}}" >
                                                                    <img src="{{asset($terrain->proprietaire->picture)}}"  class="img-sm img-circle " alt=""> {{$terrain->proprietaire->nationalIdentity}}
                                                                </a>
                                                            </td>
                                                            <td class="text-uppercase text-bold  ">{{$terrain->codeSecteur}} </td>
                                                            <td class=" ">{{$terrain->forme}}</td>
                                                            <td class=" ">{{$terrain->longer * $terrain->larger}} <span class="text-red">m*m</span></td>
                                                            <td class="">{{$terrain->dommaine}}</td>
                                                            <td class="">{{$terrain->usage}}</td>
                                                            <td class=" ">{{$terrain->build}}</td>
                                                            <td class=" "><b class="text-uppercase text-bold">{{Carbon::parse($terrain->created_at)->format('d / F / Y')}}</b> </td>
                                                            <td class="">

                                                                <a href="{{route('chefsecteur.gestion_chefs_secteur_terrains.show', $terrain)}}" title="Modifier une terrain/parcelle" class="btn btn-xs btn-info  glyphicon glyphicon-eye-open"></a>

                                                                <a href="{{route('chefsecteur.gestion_chefs_secteur_terrains.edit', $terrain)}}" title="Modifier une terrain/parcelle" class="btn btn-xs btn-warning glyphicon glyphicon-edit"></a>

                                                                <form style="display: inline-block" action="{{route('chefsecteur.gestion_chefs_secteur_terrains.destroy', $terrain)}}" method="post" class="form-inline">
                                                                    {{csrf_field()}}
                                                                    {{method_field('delete')}}
                                                                    <input type="hidden" name="delete_land_not_validate_by_chef_quartier">
                                                                    <button type="submit"  title="Supprimer la terrain/parcelle" class="btn btn-xs btn-danger glyphicon glyphicon-remove-circle" onclick="return confirm('Voulez-vous vraiment suppremer l\'enregistrement de ce terrain ?')"></button>
                                                                </form>
                                                            </td>

                                                        </tr>
                                                    @endforeach
                                                    </tbody>


                                                </table>
                                                <div class="pull-right">
                                                    {{$terrains->links()}}
                                                </div>
                                            </div>

                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
