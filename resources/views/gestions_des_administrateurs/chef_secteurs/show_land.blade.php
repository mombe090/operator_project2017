
@extends('adminlte::page')

@section('htmlheader_title')
    Terrain en attente de validation par le chef de quartier.
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que chef {{Auth::user()->role=='chefSecteur' ? 'secteur' : 'quartier'}}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Affichage d'un(e) terrain/parcelle non validé(e) par le chef quartier .</h3>
                        <div class="box-tools pull-right hidden-xs">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <h2 class="page-header">Voilà les information ajoutées {{Auth::user()->role=='chefQuartier' ? 'par ' : ''}}</h2>

                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Widget: user widget style 1 -->
                                    <div class="box box-widget widget-user-2">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                        <div class="widget-user-header bg-primary">
                                            <div class="widget-user-image">
                                                <img class="img-circle" src="{{asset(Auth::user()->picture)}}" alt="User Avatar">
                                            </div>
                                            <!-- /.widget-user-image -->
                                            <h3 class="widget-user-username"><span class="text-uppercase text-bold">{{Auth::user()->role=='chefSecteur' ? Auth::user()->lastName : $terrain->secteur->chefsecteur->lastName}}</span> {{Auth::user()->role=='chefSecteur' ? Auth::user()->firstName : $terrain->secteur->chefsecteur->firstName}}</h3>
                                            <h5 class="widget-user-desc">Chef du secteur {{Auth::user()->role=='chefSecteur' ? Auth::user()->secteur->name : $terrain->secteur->name}}</h5>
                                        </div>
                                        <div class="box-footer no-padding">
                                            <ul class="nav nav-stacked">
                                                <li><a href="">Propriétaire<span class="pull-right badge bg-blue-active">{{$terrain->proprietaire->lastName}} {{$terrain->proprietaire->firstName}}</span><img
                                                                src="{{asset($terrain->proprietaire->picture)}}"  class="img-sm pull-right" alt=""></a></li>
                                                <li><a href="">Code du secteur <span class="pull-right badge bg-blue-active">{{$terrain->codeSecteur}}</span></a></li>
                                                <li><a href="">Forme <span class="pull-right badge bg-blue-active">{{$terrain->forme}}</span></a></li>
                                                <li><a href="">Dimension <span class="pull-right badge bg-blue-active">{{$terrain->longer}}*{{$terrain->larger}} <i>m*m</i></span></a></li>
                                                <li><a href="">Dommaine <span class="pull-right badge bg-blue-active">{{$terrain->dommaine}} </span></a></li>
                                                <li><a href="">Usage <span class="pull-right badge bg-blue-active">{{$terrain->usage}}</span></a></li>
                                                <li><a href="">Construit <span class="pull-right badge bg-blue-active">{{$terrain->build}}</span></a></li>
                                            </ul>
                                            <p>
                                                <img src="{{asset($terrain->attestationAchat)}}" alt="">
                                            </p>
                                            @if($terrain->planDeMass!=null)
                                                <p>
                                                    <img src="{{asset($terrain->planDeMasse)}}" alt="">
                                                </p>
                                            @endif
                                            @if($terrain->titreFoncier!=null)
                                                <p>
                                                    <img src="{{asset($terrain->titreFonciere)}}" alt="">
                                                </p>
                                            @endif

                                        </div>

                                        <div >
                                            <br>
                                            <form style="display: inline-block" action="{{route('chefquartier.gestion_terrains_chef_quartiers.update', $terrain)}}" method="post" class="form-inline {{Auth::user()->role=='chefQuartier' ? '' : 'hidden'}}">
                                                {{csrf_field()}}
                                                {{method_field('put')}}
                                                <input type="hidden" name="valider">
                                                <button type="submit"  title="Supprimer la terrain/parcelle" class="btn btn-success  " onclick="return confirm('Voulez-vous vraiment confirmé l\'enregistrement de ce terrain ?')">Confirmer <span class="fa fa-thumbs-o-up"></span></button>
                                            </form>
                                            <a href="{{route('chefquartier.gestion_terrains_chef_quartiers.edit', $terrain)}}" title="Modifier une terrain/parcelle" class="btn btn-warning pull-right">Modifier <span class="fa fa-edit"></span></a>
                                        </div>
                                    </div>
                                    <!-- /.widget-user -->
                                </div>
                            </div>
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
