
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que chef secteur de {{Auth::user()->secteur->name}}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ajout du 3 ème temoin de l'acheteur..</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <!-- left column -->
                                <div class="col-md-6 col-md-offset-3 well">
                                @if(session('emailConfirmationError'))
                                    @include('partials.emailConfirmationError')
                                @endif
                                <!-- general form elements -->
                                    {{--Premier temoins de l'acheteur--}}
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Veillez remplir les données.</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->
                                        <form role="form" method="post" enctype="multipart/form-data"  autocomplete="off" action="{{route('chefsecteur.gestion_des_temoins.store')}} ">
                                            {{csrf_field()}}
                                            <input type="hidden" name="numeroTemoin" id="" value="3">
                                            <div class="box-body">
                                                <div class="form-group {{ $errors->has('nationalIdentity') ? ' has-error' : '' }}" >
                                                    <label for="nationalIdentity">N° Carte Identité Nationale</label>
                                                    <input type="text" class="form-control" id="nationalIdentity" placeholder="Ex : 2345124/16" name="nationalIdentity" value="{{old('nationalIdentity')}}" >
                                                    @if ($errors->has('nationalIdentity'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('nationalIdentity') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>



                                                <div class="form-group {{ $errors->has('firstName') ? ' has-error' : '' }}" >
                                                    <label for="firstName">Prénom</label>
                                                    <input type="text" class="form-control" id="firstName" placeholder="Ex : Mamadou Kalil" name="firstName" value="{{old('firstName')}}" >
                                                    @if ($errors->has('firstName'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('firstName') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->has('lastName') ? ' has-error' : '' }}" >
                                                    <label for="lastName">NOM</label>
                                                    <input type="text" class="form-control" id="lastName" placeholder="Ex : SOUMAH" name="lastName" value="{{old('lastName')}}" >
                                                    @if ($errors->has('lastName'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('lastName') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                                <div class="form-group" >
                                                    <div class="row">
                                                        <p class="text-center">
                                                            Date de naissance
                                                        </p>
                                                        <div class="col-md-4 col-sm-12">
                                                            <select name="jour" id="" class="form-control">

                                                                @for($i=1; $i<31; $i++)

                                                                    @if($i < 10)
                                                                        <option value="{{old('jour') ? old('jour') : $i}}">{{old('jour') ? old('jour') : $i}}</option>
                                                                    @else
                                                                        <option value="{{old('jour') ? old('jour') : $i}}">{{old('jour') ? old('jour') : $i}}</option>
                                                                    @endif

                                                                @endfor
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4 col-sm-12">
                                                            <select name="mois" id="" class="form-control">
                                                                @foreach($months as $month)
                                                                    <option value="{{old('mois') ? old('mois') :  $month->id}}">{{old('mois') ? $month->name :  $month->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4 col-sm-12">
                                                            <select name="annee" id="" class="form-control">

                                                                @for( $i = 1999; $i > 1950 ;$i--)
                                                                    <option value="{{ old('annee') ? old('annee') : $i}}">{{ old('annee') ? old('annee') : $i}}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}" >
                                                    <label for="email">Email</label>
                                                    <input type="text" class="form-control" id="email" placeholder="Ex : amadou@gmail.com" name="email" value="{{old('email')}}" >
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('email') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}" >
                                                    <label for="telephone">Telephone</label>
                                                    <input type="text" class="form-control" id="telephone" placeholder="Ex : amadou@gmail.com" name="telephone" value="{{old('telephone')}}" >
                                                    @if ($errors->has('telephone'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('telephone') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->has('picture') ? ' has-error' : '' }}" >
                                                    <label for="picture">Photo d'identité</label>
                                                    <input type="file"  id="picture" placeholder="Ex : amadou@gmail.com" name="picture" value="{{old('picture')}}" >
                                                    @if ($errors->has('picture'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('picture') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                            </div>
                                            <!-- /.box-body -->

                                            <div class="box-footer">
                                                <a href="">Ajouter les temoins du vendeur</a>
                                                <button type="submit" class="btn btn-primary pull-right" onclick="return confirm('Avez-vous bien verifié les données que vous avez rentrées.')">Valider</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                <!--/.col (left) -->

                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
