
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que chef secteur.
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ajout d'un(e) terrain/parcelle dans votre secteur.</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <!-- left column -->
                                <div class="col-md-6 col-md-offset-3 well">
                                    <!-- general form elements -->
                                    @if(session('emailConfirmationError'))
                                            @include('partials.emailConfirmationError')
                                    @endif
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Veillez remplir les données.</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->
                                        <form role="form" method="post" enctype="multipart/form-data" autocomplete="off" action="{{route('chefsecteur.gestion_chefs_secteur_terrains.store')}}">
                                            {{csrf_field()}}
                                            <div class="box-body">
                                                <div class="form-group {{ $errors->has('identifiant') ? ' has-error' : '' }}" >
                                                    <label for="identifiant">N° D'identité / email / telephone du Propriétaire </label>
                                                    <input type="text" class="form-control" id="identifiant" placeholder="Exemple : 1247254/17 ou ex@gmail.com out 644.........." name="identifiant" value="{{old('identifiant')}}" >
                                                    @if ($errors->has('identifiant'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('identifiant') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->has('attestationAchat') ? ' has-error' : '' }}" >
                                                    <label for="attestationAchat">Attestation Achat</label>
                                                    <input type="file"  id="attestationAchat"  name="attestationAchat" value="{{old('attestationAchat')}}" >
                                                    @if ($errors->has('attestationAchat'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('attestationAchat') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>


                                                <div class="form-group {{ $errors->has('planDeMasse') ? ' has-error' : '' }}" >
                                                    <label for="planDeMasse">Plan de masse.</label>
                                                    <input type="file"  id="planDeMasse"  name="planDeMasse" value="{{old('planDeMasse')}}" >
                                                    @if ($errors->has('planDeMasse'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('planDeMasse') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>


                                                <div class="form-group {{ $errors->has('titreFoncier') ? ' has-error' : '' }}" >
                                                    <label for="titreFoncier">Titre Foncier</label>
                                                    <input type="file"  id="titreFoncier"  name="titreFoncier" value="{{old('titreFoncier')}}" >
                                                    @if ($errors->has('titreFoncier'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('titreFoncier') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->has('longer') ? ' has-error' : '' }}" >
                                                    <label for="longer">Longueur de la parcelle <span class="text-red">(en m)</span></label>
                                                    <input type="text" class="form-control" id="longer" placeholder="Exemple : 25.67" name="longer" value="{{old('longer')}}" >
                                                    @if ($errors->has('longer'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('longer') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->has('larger') ? ' has-error' : '' }}" >
                                                    <label for="larger">Largeur de la parcelle <span class="text-red">(en m)</span></label>
                                                    <input type="text" class="form-control" id="larger" placeholder="26.56" name="larger" value="{{old('larger')}}">
                                                    @if ($errors->has('larger'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('larger') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>


                                                <div class="form-group {{ $errors->has('dommaine') ? ' has-error' : '' }}" >
                                                    <label for="dommaine">Dommaine</label>
                                                    <select name="dommaine" id="dommaine" class="form-control">
                                                        @foreach($dommaines as $dommaine)
                                                            <option value="{{$dommaine->id }}" {{ old('dommaine') ? 'selected' : '' }} class="text-capitalize"><span class="text-capitalize">{{$dommaine->name}}</span></option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('dommaine'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('dommaine') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->has('forme') ? ' has-error' : '' }}" >
                                                    <label for="forme">Forme de la parcelle</label>
                                                    <select name="forme" id="forme" class="form-control">
                                                        <option value="Rectangulaire" {{old('forme')=='Rectangulaire' ? 'selected' : ''}}>Rectangulaire</option>
                                                        <option value="Triangulaire" {{old('forme')=='Triangulaire' ? 'selected' : ''}}>Triangulaire</option>
                                                        <option value="Autre" {{old('forme')=='Autre' ? 'selected' : ''}}>Autre</option>
                                                    </select>
                                                    @if ($errors->has('forme'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('forme') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>


                                                <div class="form-group {{ $errors->has('usage') ? ' has-error' : '' }}" >
                                                    <label for="usage">Utilsation</label>
                                                    <select name="usage" id="usage" class="form-control">
                                                        @foreach($usages as $usage)
                                                            <option value="{{$usage->id}}" {{old('usage') ? 'selected' : ''}}>{{$usage->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('usage'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('usage') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                                <div class="form-group {{ $errors->has('build') ? ' has-error' : '' }}" >
                                                    <label for="build">Construit</label>
                                                    <select name="build" id="build" class="form-control">
                                                        <option value="yes" {{old('build')=='yes' ? 'selected' : ''}}>Oui</option>
                                                        <option value="no" {{old('build')=='no' ? 'selected' : ''}}>Non</option>
                                                    </select>
                                                    @if ($errors->has('build'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('build') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                                <div class="form-group {{ $errors->has('codeSecteur') ? ' has-error' : '' }}" >
                                                    <label for="codeSecteur">N° Parcelle au niveau de votre secteur</label>
                                                    <input type="text" name="codeSecteur" id="codeSecteur" placeholder="Exemple : 000001" class="form-control" value="{{old('codeSecteur')}}">
                                                    @if ($errors->has('codeSecteur'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('codeSecteur') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                            </div>
                                            <!-- /.box-body -->

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">Valider</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.box -->

                                </div>
                                <!--/.col (left) -->

                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
