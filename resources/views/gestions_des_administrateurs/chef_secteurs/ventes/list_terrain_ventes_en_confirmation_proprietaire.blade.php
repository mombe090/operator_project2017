@php use Carbon\Carbon; @endphp
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecg
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="2">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Terrains/Parcelles</h3>
                        <div class="box-tools pull-right ">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">

                                    <div class="box">
                                        <div class="box-header">
                                            <h3 class="box-title">Liste des Parcelles mise en vente par le propriétaire.</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="box-body table-responsive no-padding">
                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th class=" ">Identité</th>
                                                        <th>Propriétaire</th>
                                                        <th>Telephone</th>
                                                        <th class=" ">Code</th>
                                                        <th class=" ">Forme</th>
                                                        <th class=" ">Dimensions</th>

                                                        <th class="">Action</th>

                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    @foreach($terrains as $terrain)
                                                        <tr>
                                                            <td class="text-uppercase text-bold   text-center">
                                                                <a href="{{asset($terrain->proprietaire->picture)}}" >
                                                                    <img src="{{asset($terrain->proprietaire->picture)}}"  class="img-sm img-circle " alt=""> {{$terrain->proprietaire->nationalIdentity}}
                                                                </a>
                                                            </td>
                                                            <td class="">{{$terrain->proprietaire->lastName}} {{$terrain->proprietaire->firstName}}</td>
                                                            <td class=" ">{{$terrain->proprietaire->telephone}}</td>
                                                            <td class="text-uppercase text-bold  ">{{$terrain->codeSecteur}} </td>
                                                            <td class=" ">{{$terrain->forme}}</td>
                                                            <td class=" ">{{$terrain->longer}} * {{$terrain->larger}} <span class="text-red">m*m</span></td>
                                                       <td>
                                                           <form style="display: inline-block" action="{{route('proprietaire.gestion_own_proprietaire_terrien.update', $terrain)}}" method="post" class="form-inline">
                                                               {{csrf_field()}}
                                                               {{method_field('put')}}
                                                               <input type="hidden" name="confirmVente">
                                                               <button type="submit"  title="confirmer le/la terrain/parcelle" class="btn btn-success fa fa-thumbs-o-up" onclick="return confirm('Voulez-vous vraiment confirmé la vente de ce terrain ?')">
                                                                   Valider cette vente
                                                               </button>
                                                           </form>

                                                       </td>

                                                        </tr>
                                                    @endforeach
                                                    </tbody>


                                                </table>
                                                <div class="pull-right">
                                                    {{$terrains->links()}}
                                                </div>
                                            </div>

                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
