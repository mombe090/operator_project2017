
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que chef secteur de {{Auth::user()->secteur->name}}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ajout d'un(e) propriétaire de parcelles/terrains dans votre secteur.</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <!-- left column -->
                                <div class="col-md-6 col-md-offset-3 well">
                                @if(session('emailConfirmationError'))
                                    @include('partials.emailConfirmationError')
                                @endif
                                <!-- general form elements -->
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Veillez remplir les données.</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->
                                        <form role="form" method="post" autocomplete="off" action="{{route('chefsecteur.add_terrain_proprietaire_preuve')}}">
                                            {{csrf_field()}}
                                            <div class="box-body">
                                                <div class="form-group {{ $errors->has('terrain_id') ? ' has-error' : '' }}" >
                                                    <label for="terrain_id">Selectionner le code du terrain</label>
                                                    <select name="terrain_id" id="" class="form-control">
                                                        @foreach($terrains as $terrain)
                                                            <option value="{{$terrain->id}}">{{$terrain->codeSecteur}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('terrain_id'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('terrain_id') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary" onclick="return confirm('Avez-vous bien verifié les données que vous avez rentrées.')">Valider</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.box -->

                                </div>
                                <!--/.col (left) -->

                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
