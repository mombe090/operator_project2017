
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que chef secteur de {{Auth::user()->secteur->name}}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ajout des preuve que le terrain l'appartien bien à M./Mlle {{$nom}} {{$prenom}}</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <!-- left column -->
                                <div class="col-md-6 col-md-offset-3 well">
                                    <!-- general form elements -->
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Veillez scannez ces documents jpeg ou png</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->
                                        <form role="form" enctype="multipart/form-data" method="post" autocomplete="off" action="{{route('chefsecteur.store_preuve_proprietaire')}}" >
                                            {{csrf_field()}}
                                            <input type="hidden" name="terrain_id" id="" value={{$terrain_id}}>
                                            <div class="box-body">
                                                <div class="form-group {{ $errors->has('attestationAchat') ? ' has-error' : '' }}" >
                                                    <label for="attestationAchat">Attestation d'acquisition de la parcelle</label>
                                                    <input type="file"  id="attestationAchat" name="attestationAchat" value="choisir une photo" class="form-control" required >
                                                    @if ($errors->has('attestationAchat'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('attestationAchat') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->has('planDeMasse') ? ' has-error' : '' }}" >
                                                    <label for="planDeMasse">Plan de masse de la parcelle</label>
                                                    <input type="file"  id="planDeMasse" name="planDeMasse" value="choisir une photo" class="form-control" required >
                                                    @if ($errors->has('planDeMasse'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('planDeMasse') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>


                                                <div class="form-group {{ $errors->has('titreFoncier') ? ' has-error' : '' }}" >
                                                    <label for="titreFoncier">Titre foncier.</label>
                                                    <input type="file"  id="titreFoncier" name="titreFoncier" value="choisir une photo" class="form-control" required >
                                                    @if ($errors->has('titreFoncier'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('titreFoncier') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                                <input type="submit" class="btn btn-md btn-info form-control" value="Modifer">
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.box -->

                                </div>
                                <!--/.col (left) -->

                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
