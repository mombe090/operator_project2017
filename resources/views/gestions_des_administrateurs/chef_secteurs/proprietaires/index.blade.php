@php use Carbon\Carbon; @endphp
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que chef secteur du secteur {{Auth::user()->secteur->name}} dans le quartier {{Auth::user()->secteur->quartier->name}}
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="2">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Terrains/Parcelles</h3>
                        <div class="box-tools pull-right ">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    @if($terrains->count()!=0)
                        <div class="box-body">
                            <!-- Main content -->
                            <section class="content">
                                <div class="row">
                                    <div class="col-xs-12">

                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">Liste des propriétaires dont les parcelles ont été validées  par le chef de quartier M. {{Auth::user()->secteur->quartier->chefquartier->lastName}}</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="box-body table-responsive no-padding">
                                                    <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th class=" ">Code Terrains</th>
                                                        <th class=" ">Propriétaire</th>
                                                        <th class="">Telephone</th>
                                                        <th class="">Email</th>
                                                        <th class="">Date de naissance</th>
                                                        <th class="">Construit</th>
                                                        <th class=" ">Date d'ajout</th>
                                                        <th class=" ">Preuves</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($terrains as $terrain)
                                                        <tr>
                                                            <td class="text-uppercase text-bold  ">{{$terrain->codeSecteur}} </td>
                                                            <td class=" ">
                                                                <a href="{{asset($terrain->proprietaire->picture)}}">
                                                                    <img src="{{asset($terrain->proprietaire->picture)}}" class="img-sm img-circle" alt="">
                                                                </a>
                                                                {{$terrain->proprietaire->lastName }} {{$terrain->proprietaire->firstName}}
                                                            </td>
                                                            <td class="">{{$terrain->proprietaire->telephone}}</td>
                                                            <td class="">{{$terrain->proprietaire->email}}</td>
                                                            <td class="">{{$terrain->proprietaire->dateOfBirth}}</td>
                                                            <td class="">{{$terrain->build}}</td>
                                                            <td class=" "><b class="text-uppercase text-bold">{{Carbon::parse($terrain->created_at)->format('d / F / Y')}}</b> </td>
                                                            <td class=" ">
                                                                <a href="{{asset($terrain->attestationAchat)}}">
                                                                    <img src="{{asset($terrain->attestationAchat)}}" alt="" class="img-sm img-thumbnail" title="Attestation Achat du terrain.">
                                                                </a>
                                                                @if($terrain->planDeMasse!=null)
                                                                    <a href="{{asset($terrain->planDeMasse)}}">
                                                                        <img src="{{asset($terrain->planDeMasse)}}" alt="" class="img-sm img-thumbnail" title="Plan de masse.">
                                                                    </a>
                                                                @endif
                                                                @if($terrain->titreFoncier!=null)
                                                                    <a href="{{asset($terrain->titreFoncier)}}">
                                                                        <img src="{{asset($terrain->titreFoncier)}}" alt="" class="img-sm img-thumbnail" title="Titre Foncier">
                                                                    </a>
                                                                @endif

                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>


                                                </table>
                                                <div class="pull-right">
                                                    {{$terrains->links()}}
                                                </div>
                                            </div>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </section>
                            <!-- /.content -->
                        </div>
                    @else
                        <div class="text-red text-bold">
                            <h2 class="text-center">Il n'y a aucun terrain validé par le chef de quartier.</h2>
                        </div>
                @endif
                <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
