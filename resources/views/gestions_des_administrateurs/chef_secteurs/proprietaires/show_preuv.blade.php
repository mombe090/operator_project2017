@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que chef secteur de {{Auth::user()->secteur->name}}
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Liste des données scannées. </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <p>
                                    Attestation d'Achat :
                                    <a href="{{$preuves->attestationAchat}}"><img src="{{$preuves->attestationAchat}}" alt="" style="width: 100%; height: 400px"></a>
                                </p>
                                <p>
                                    Plan de Masse :
                                    <a href="{{$preuves->planDeMasse}}"> <img src="{{$preuves->planDeMasse}}" alt="" style="width: 100% ; height: 400px"></a>
                                </p>

                                <p>
                                    Titre Foncier :
                                    <a href="{{$preuves->titreFoncier}}"><img src="{{$preuves->titreFoncier}}" alt="" style="width: 100% ; height: 400px"></a>
                                </p>

                                <!--/.col (left) -->

                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
