
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que chef secteur de {{Auth::user()->secteur->name}}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ajout d'un(e) acheteur de parcelles/terrains dans votre secteur.</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <p class="text-center">
                                <label for="choice">Est-il déjà un propriétaire de terrain quelques part en Guinée</label>
                            </p>
                                <div class="col-md-4 col-md-offset-4">
                                    <form action="{{route('chefsecteur.gestion_des_ventes.create')}}" method="get">
                                        {{csrf_field()}}

                                       <div class="form-group">
                                           <select name="choice" id="choice" class="form-control">
                                               <option value="oui">OUI</option>
                                               <option value="non">NON</option>

                                           </select>
                                       </div>
                                        <p>
                                            <input type="submit" name="valider" id="" value="Valider" class="btn btn-md btn-primary form-control">
                                        </p>
                                    </form>
                                </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
