@php use Carbon\Carbon; @endphp
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que Directeur Général du Bureau de Conservation Foncier de la region de  {{Auth::user()->region->nom}}.
@endsection

@section('main-content')
    <!-- Main content -->
    <section class="content">


        <h2 class="page-header">Informations du terrain</h2>

        <div class="row">

            <!-- /.col -->
            <div class="col-md-6 col-md-offset-3 col-xs-12">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-aqua-active">
                        <h3 class="widget-user-username"><span class="text-bold text-uppercase">{{$terrain->proprietaire->lastName}}</span> {{$terrain->proprietaire->firstName}}</h3>
                        <h5 class="widget-user-desc">Propriétaire de terrain</h5>
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle" src="{{asset($terrain->proprietaire->picture)}}" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <span>Code </span>
                                <i class="pull-right"> {{$terrain->codeSecteur}}</i><br>

                                <span>Secteur </span>
                                <i class="pull-right"> {{$terrain->secteur->name}}</i><br>

                                <span>Dommaine </span>
                                <i class="pull-right"> {{$terrain->dommaine}}</i><br>

                                <span>Dommaine </span>
                                <i class="pull-right"> {{$terrain->dommaine}}</i><br>

                                <span>N° D'identité du propriétaire </span>
                                <i class="pull-right"> {{$terrain->proprietaire->nationalIdentity}}</i><br>
                                <span>Titre foncier existant </span>
                                <i class="pull-right">
                                    <a href="{{asset($terrain->titreFoncier)}}">
                                        <img src="{{asset($terrain->titreFoncier)}}" class="img-md img-thumbnail" alt="">
                                    </a>
                                </i><br>

                                <div class="col-md-3 col-md-6 col-md-offset-3">
                                    <p class="text-center">
                                        modifier le titre foncier

                                    </p>
                                    <form role="form" enctype="multipart/form-data" method="post" autocomplete="off" action="{{route('superadmin.bureau_conservation_fonciere.update', $terrain->id)}}" >
                                        {{csrf_field()}}
                                        {{method_field('put')}}
                                        <div class="box-body">
                                            <input type="hidden" name="editTitre" id="">
                                            <div class="form-group {{ $errors->has('titreFoncier') ? ' has-error' : '' }}" >
                                                <input type="file"  id="titreFoncier" name="titreFoncier" value="choisir une photo"  >
                                                @if ($errors->has('titreFoncier'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('titreFoncier') }}</strong>
                                                       </span>
                                                @endif
                                            </div>

                                            <input type="submit" class="btn btn-md btn-info form-control" onclick="return confirm('Etes-vous sûre de vouloir modifier le titre foncier ?')" value="Modifer">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>

        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection
