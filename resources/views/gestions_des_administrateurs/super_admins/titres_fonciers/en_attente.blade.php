@php use Carbon\Carbon; @endphp
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que Directeur Général du Bureau de Conservation Foncier de la region de  {{Auth::user()->region->name}}.
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Terrains/Parcelles</h3>
                        <div class="box-tools pull-right hidden-xs">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">

                                    @if(count($terrains)!=0)
                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">Liste des Parcelles en attente de validation par vous M. <span class="text-bold text-uppercase">{{Auth::user()->lastName}}</span> {{Auth::user()->firstName}} dans la région de {{Auth::user()->region->name}}.</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="box-body table-responsive no-padding">
                                                    <div class="box-body table-responsive no-padding">
                                                        <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th class="hidden-xs">Code préfectorale</th>
                                                            <th>Proprietaire</th>
                                                            <th>N° Identité</th>
                                                            <th class="">Dommaine</th>
                                                            <th class="">Utilisation</th>
                                                            <th class=" ">Construit</th>
                                                            <th class=" ">Quartier</th>
                                                            <th class="">Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($terrains as $terrain)
                                                            <tr>
                                                                <td class="text-uppercase text-bold hidden-xs">{{$terrain->codeCommune}} </td>
                                                                <td class="text-uppercase text-bold ">{{$terrain->lastName}} {{$terrain->firstName}} </td>
                                                                <td class="text-uppercase text-bold ">{{$terrain->nationalIdentity}} </td>

                                                                <td class="">{{$terrain->dommaine}}</td>
                                                                <td class="">{{$terrain->usage}}</td>
                                                                <td class=" ">{{$terrain->build=='yes' ? 'Oui' : 'Non'}}</td>
                                                                <td class=" ">{{$terrain->quartierName}}</td>
                                                                <td class="">

                                                                    <a href="{{route('superadmin.bureau_conservation_fonciere.show', $terrain->terrainID)}}" title="Affiche une terrain/parcelle" class="btn btn-xs btn-info  fa fa-eye"></a>


                                                                </td>

                                                            </tr>
                                                        @endforeach
                                                        </tbody>


                                                    </table>
                                                   <div class="pull-right">
                                                       {{$terrains->links()}}
                                                   </div>
                                                </div>
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    @else
                                        <h2 class="text-red text-center">Il n'y pas de parcelle en attente de votre confirmation.</h2>
                                @endif
                                <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
