@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    <span class="text-info">Modification des information du super administrateur <i class="text-warning">{{$admin->lastName}} {{$admin->firstName }}</i></span>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-11 col-md-offset-1">

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-5 col-xs-12">
                            <!-- general form elements -->
                            <div class="box box-danger">
                                <div class="box-header with-border">
                                    <h3 class="box-title">mise à jour de votre photo de profile</h3>
                                </div>

                                <div class="box-body">
                                    <form role="form" enctype="multipart/form-data" method="post" autocomplete="off" action="{{route('superadmin.gestion_admins.update', $admin)}}" >
                                        {{csrf_field()}}
                                        {{method_field('put')}}
                                        <div class="box-body">
                                            <div class="form-group {{ $errors->has('picture') ? ' has-error' : '' }}" >
                                                <input type="file"  id="picture" name="picture" value="" class="form-control" required>
                                                @if ($errors->has('picture'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('picture') }}</strong>
                                                       </span>
                                                @endif
                                            </div>

                                            <input type="submit" class="btn btn-md btn-info form-control" value="Modifer">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Informations actuellement stockées</h3>
                                    <p class="center-block">
                                        <a href="{{asset($admin->picture)}}">
                                            <img class="profile-user-img img-md " src="{{asset($admin->picture)}}" alt="Image du profile">
                                        </a>
                                    </p>

                                </div>

                                <div class="box-body">
                                    <p>
                                        <b class="text-primary">Nom : </b> <span class="text-uppercase">{{$admin->lastName}}</span>
                                    </p>
                                    <hr>
                                    <p>
                                        <b class="text-primary">Prénom : </b> <span class="text-capitalize">{{$admin->firstName}}</span>
                                    </p>
                                    <hr>

                                    <p>
                                        <b class="text-primary">N° Carte d'identité national : </b> <span class="text-uppercase">{{$admin->nationalIdentity}}</span>
                                    </p>
                                    <hr>
                                    <p>
                                        <b class="text-primary">email : </b> <span class="text-capitalize">{{$admin->email}}</span>
                                    </p>
                                    <hr>

                                    <p>
                                        <b class="text-primary">Telephone : </b> <span class="text-uppercase">{{$admin->telephone}}</span>
                                    </p>
                                    <hr>
                                    <p>
                                        <b class="text-primary">Date de naissance : </b> <span class="text-capitalize">{{$admin->dateOfBirth}}</span>
                                    </p>
                                    <hr>
                                    <p>
                                        <b class="text-primary">Date d'embauchement au compte de la région de  <b class="text-primary text-uppercase">{{$admin->prefecture->nom}}</b> : </b> <span class="text-capitalize">{{$admin->hireDate}}</span>
                                    </p>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-7 col-xs-12">
                            <!-- Horizontal Form -->
                            <div class="box box-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Mise à jour.</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->

                                <form action="{{route('superadmin.gestion_admins.update', $admin)}}" class="form-horizontal" method="post">
                                    {{method_field('put')}}
                                    {{csrf_field()}}
                                    <input type="hidden" name="trueUpdate" value="true">

                                    <div class="box-body">
                                        <div class="form-group {{ $errors->has('lastName') ? ' has-error' : '' }}">
                                            <label for="lastName" class="col-sm-4 text-primary control-label">Nom</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" name="lastName" id="lastName" value="{{old('lastName') ? old('lastName') : $admin->lastName}}">
                                                @if ($errors->has('lastName'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('lastName') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('firstName') ? ' has-error' : '' }}">
                                            <label for="firstName" class="col-sm-4 text-primary control-label">Prénom</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" name="firstName" id="firstName" value="{{old('firstName') ? old('firstName') : $admin->firstName}}">
                                                @if ($errors->has('firstName'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('firstName') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group {{ $errors->has('nationalIdentity') ? ' has-error' : '' }}">
                                            <label for="nationalIdentity" class="col-sm-4 text-primary control-label">N° D'identité</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" name="nationalIdentity" id="nationalIdentity" value="{{old('nationalIdentity') ? old('nationalIdentity') : $admin->nationalIdentity}}">
                                                @if ($errors->has('nationalIdentity'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('nationalIdentity') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-sm-4 text-primary control-label">Email</label>

                                            <div class="col-sm-8">
                                                <input type="email" class="form-control text-warning" name="email" id="email" value="{{old('email') ? old('email') : $admin->email}}">
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('email') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                            <label for="telephone" class="col-sm-4 text-primary control-label">N° Telephone</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" name="telephone" id="telephone" value="{{old('telephone') ? old('telephone') : $admin->telephone}}">
                                                @if ($errors->has('telephone'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('telephone') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('dateOfBirth') ? ' has-error' : '' }}">
                                            <label for="dateOfBirth" class="col-sm-4 text-primary control-label">Date Naissance</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" name="dateOfBirth" id="dateOfBirth" value="{{old('dateOfBirth') ? old('dateOfBirth') : $admin->dateOfBirth}}">
                                                @if ($errors->has('dateOfBirth'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('dateOfBirth') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('hireDate') ? ' has-error' : '' }}">
                                            <label for="hireDate" class="col-sm-4 text-primary control-label">Début de travail</label>

                                            <div class="col-sm-8">
                                                <input type="text" class="form-control text-warning" name="hireDate" id="hireDate" value="{{old('hireDate') ? old('hireDate') : $admin->hireDate}}">
                                                @if ($errors->has('hireDate'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('hireDate') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group {{ $errors->has('region_id') ? ' has-error' : '' }}">
                                            <label for="region_id" class="col-sm-4 text-primary control-label">Préfecture</label>

                                            <div class="col-sm-8">
                                                <select name="prefecture_id" id="prefecture_id" class="form-control">
                                                    @foreach($prefectures as $prefecture)
                                                        <option value="{{$prefecture->id}}"  {{$admin->prefecture_id == $prefecture->id ? 'selected' : ''}}>{{$prefecture->nom}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('prefecture_id'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('prefecture_id') }}</strong>
                                                       </span>
                                                @endif
                                            </div>
                                        </div>


                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">

                                        <button type="submit" class="btn btn-info pull-right">Valider</button>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>

                        </div>
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->

            </div>
        </div>
    </div>
@endsection
