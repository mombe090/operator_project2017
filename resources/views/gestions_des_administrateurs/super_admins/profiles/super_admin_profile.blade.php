@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    <span class="text-info">Profile de M. {{$superAdmin->lastName}} {{$superAdmin->firstName}} administrateur de la region de {{$superAdmin->region->name}}. </span>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <!-- Main content -->

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-md-3">

                            <!-- Profile Image -->
                            <div class="box box-primary">
                                <div class="box-body box-profile">
                                    <img class="profile-user-img img-responsive img-circle" src="{{asset($superAdmin->picture)}}" alt="User profile picture">

                                    <h3 class="profile-username text-center">{{$superAdmin->lastName}} {{$superAdmin->firstName}}</h3>

                                    <p class="text-muted text-center">Administrateur de {{$superAdmin->region->name}}</p>

                                    {{--<ul class="list-group list-group-unbordered">
                                        <li class="list-group-item">
                                            <b>Préfectures</b> <a class="pull-right">{{$prefectureNumbers}}</a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Following</b> <a class="pull-right">543</a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Friends</b> <a class="pull-right">13,287</a>
                                        </li>
                                    </ul>--}}

                                    <form role="form" enctype="multipart/form-data" method="post" autocomplete="off" action="{{route('superadmin.gestion_own_superadmins.update', Auth::user())}}" >
                                        {{csrf_field()}}
                                        {{method_field('put')}}
                                        <div class="box-body">
                                            <div class="form-group {{ $errors->has('picture') ? ' has-error' : '' }}" >
                                                <input type="file"  id="picture" name="picture" value="choisir une photo" class="form-control" required >
                                                @if ($errors->has('picture'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('picture') }}</strong>
                                                       </span>
                                                @endif
                                            </div>

                                            <input type="submit" class="btn btn-md btn-info form-control" value="Modifer">
                                        </div>
                                    </form>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->

                            <!-- About Me Box -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Bio</h3>
                                    <p>{{str_limit(str_replace('<br />', PHP_EOL, $superAdmin->bio), 20)}}...</p>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <strong><i class="fa fa-book margin-r-5"></i> Je suis</strong>

                                    <p class="text-muted">

                                    </p>

                                    <hr>

                                    <strong><i class="fa fa-map-marker margin-r-5"></i> Localisation</strong>

                                    <p class="text-muted">Guinée, {{$superAdmin->region->name}}</p>

                                    <hr>


                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-9">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#activity" data-toggle="tab">Préfecture</a></li>

                                    <li><a href="#settings" data-toggle="tab">Mise à jour</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="active tab-pane" id="activity">
                                        <!-- Post -->
                                        <div class="post">
                                            <div class="user-block">
                                                <img class="img-circle img-bordered-sm" src="{{asset($superAdmin->picture)}}" alt="user image">
                                                <span class="username">
                          <a href="#">{{$superAdmin->lastName}}.</a>
                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                                                <span class="description">Mettre à jour votre biographie, parcours etc...</span>
                                            </div>
                                            <!-- /.user-block -->
                                            <p>
                                                {{str_replace('<br />', PHP_EOL, $superAdmin->bio)}}
                                            </p>

                                            <div class="box-header">
                                                <h3 class="box-title">Rajoutez vos informations
                                                    <small>Facilement on poura vous reconnaitre</small>
                                                </h3>
                                                <!-- tools box -->
                                                <div class="pull-right box-tools">
                                                    <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i></button>
                                                    <button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i></button>
                                                </div>
                                                <!-- /. tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body pad">

                                                <form method="post" action="{{route('superadmin.gestion_own_superadmins.update', Auth::user())}}">
                                                    {{method_field('put')}}
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="isBio">
                                                    <textarea {{ $errors->has('bio') ? ' has-error' : '' }}  name="bio" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('bio') ? old('bio') : str_replace('<br />', PHP_EOL, $superAdmin->bio)}}</textarea>
                                                    @if ($errors->has('bio'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('bio') }}</strong>
                                                       </span>
                                                    @endif
                                                    <input type="submit" class="btn btn-md btn-info pull-right">
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.post -->
                                        <!-- /.post -->
                                    </div>


                                    <div class="tab-pane" id="settings">

                                        <form class="form-horizontal" method="post" action="{{route('superadmin.gestion_own_superadmins.update', Auth::user())}}">
                                            {{csrf_field()}}
                                            {{method_field('put')}}
                                            <input type="hidden" name="allUp">
                                            <div class="form-group {{ $errors->has('firstName') ? ' has-error' : '' }}">
                                                <label for="firstName" class="col-sm-4 control-label">Prénom</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="firstName" name="firstName" value="{{$superAdmin->firstName}}">
                                                    @if ($errors->has('firstName'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('firstName') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('lastName') ? ' has-error' : '' }}">
                                                <label for="lastName" class="col-sm-4 control-label">NOM</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="lastName" name="lastName" value="{{$superAdmin->lastName}}">
                                                    @if ($errors->has('lastName'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('lastName') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('nationalIdentity') ? ' has-error' : '' }}">
                                                <label for="nationalIdentity" class="col-sm-4 control-label">N° Carte D'identité</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="nationalIdentity" name="nationalIdentity" value="{{$superAdmin->nationalIdentity}}">
                                                    @if ($errors->has('nationalIdentity'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('nationalIdentity') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                                <label for="telephone" class="col-sm-4 control-label">Telephone</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="telephone" name="telephone" value="{{$superAdmin->telephone}}">
                                                    @if ($errors->has('telephone'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('telephone') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email" class="col-sm-4 control-label">Email</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="email" name="email" value="{{$superAdmin->email}}">
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('email') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('dateOfBirth') ? ' has-error' : '' }}">
                                                <label for="dateOfBirth" class="col-sm-4 control-label">Date de naissance</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="dateOfBirth" name="dateOfBirth" value="{{$superAdmin->dateOfBirth}}">
                                                    @if ($errors->has('dateOfBirth'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('dateOfBirth') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('hireDate') ? ' has-error' : '' }}">
                                                <label for="hireDate" class="col-sm-4 control-label">Prise de fonction</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="hireDate" name="hireDate" value="{{$superAdmin->hireDate}}">
                                                    @if ($errors->has('hireDate'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('hireDate') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('region_id') ? ' has-error' : '' }}">
                                                <label for="region_id" class="col-sm-4 control-label">Region</label>

                                                <div class="col-sm-8">
                                                    <select name="region_id" id="region_id" class="form-control">
                                                        @foreach($regions as $region)
                                                            <option value="{{old('region_id') ? old('region_id') : $region->id}}" {{$region->id==$superAdmin->region_id ? 'selected' : ''}}>{{$region->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('region_id'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('region_id') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-offset-9 col-sm-3">
                                                    <button type="submit" class="btn btn-info form-control">Valider</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div>
                            <!-- /.nav-tabs-custom -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                </section>
                <!-- /.content -->
                <!-- /.content -->

            </div>
        </div>
    </div>
@endsection
