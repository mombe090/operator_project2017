@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    <span class="text-info">Profile de M. {{$maire->lastName}} {{$maire->firstName}} maire de la commune de {{$maire->commune->name}}. </span>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">
                <section class="content">
                    <div class="row">
                        <div class="col-md-3">
                            <!-- Profile Image -->
                            <div class="box box-primary">
                                <div class="box-body box-profile">
                                    <img class="profile-user-img img-responsive img-circle" src="{{asset($maire->picture)}}" alt="User profile picture">

                                    <h3 class="profile-username text-center">{{$maire->lastName}} {{$maire->firstName}}</h3>

                                    <p class="text-muted text-center">Administrateur de {{$maire->commune->name}}</p>

                                    {{--<ul class="list-group list-group-unbordered">
                                        <li class="list-group-item">
                                            <b>Préfectures</b> <a class="pull-right">{{$prefectureNumbers}}</a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Following</b> <a class="pull-right">543</a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Friends</b> <a class="pull-right">13,287</a>
                                        </li>
                                    </ul>--}}

                                    <form role="form" enctype="multipart/form-data" method="post" autocomplete="off" action="{{route('maire.gestion_own_maires.update', Auth::user())}}" >
                                        {{csrf_field()}}
                                        {{method_field('put')}}
                                        <div class="box-body">
                                            <div class="form-group {{ $errors->has('picture') ? ' has-error' : '' }}" >
                                                <input type="file"  id="picture" name="picture" value="choisir une photo" class="form-control" required >
                                                @if ($errors->has('picture'))
                                                    <span class="help-block">
                                                                 <strong>{{ $errors->first('picture') }}</strong>
                                                       </span>
                                                @endif
                                            </div>

                                            <input type="submit" class="btn btn-md btn-info form-control" value="Modifer">
                                        </div>
                                    </form>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->

                            <!-- About Me Box -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Bio</h3>
                                    <p>{{str_limit(str_replace('<br />', PHP_EOL, $maire->bio), 20)}}...</p>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <strong><i class="fa fa-book margin-r-5"></i> Je suis</strong>

                                    <p class="text-muted">

                                    </p>

                                    <hr>

                                    <strong><i class="fa fa-map-marker margin-r-5"></i> Localisation</strong>

                                    <p class="text-muted">Guinée, {{$maire->commune->name}}</p>

                                    <hr>


                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-9">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#activity" data-toggle="tab">Préfecture</a></li>

                                    <li><a href="#settings" data-toggle="tab">Mise à jour</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="active tab-pane" id="activity">
                                        <!-- Post -->
                                        <div class="post">
                                            <div class="user-block">
                                                <img class="img-circle img-bordered-sm" src="{{asset($maire->picture)}}" alt="user image">
                                                <span class="username">
                          <a href="#">{{$maire->lastName}}.</a>
                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                                                <span class="description">Mettre à jour votre biographie, parcours etc...</span>
                                            </div>
                                            <!-- /.user-block -->
                                            <p>
                                                {{str_replace('<br />', PHP_EOL, $maire->bio)}}
                                            </p>

                                            <div class="box-header">
                                                <h3 class="box-title">Rajoutez vos informations
                                                    <small>Facilement on poura vous reconnaitre</small>
                                                </h3>
                                                <!-- tools box -->

                                                <!-- /. tools -->
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body pad">

                                                <form method="post" action="{{route('maire.gestion_own_maires.update', Auth::user())}}">
                                                    {{method_field('put')}}
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="isBio">
                                                    <textarea {{ $errors->has('bio') ? ' has-error' : '' }}  name="bio" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('bio') ? old('bio') : str_replace('<br />', PHP_EOL, $maire->bio)}}</textarea>
                                                    @if ($errors->has('bio'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('bio') }}</strong>
                                                       </span>
                                                    @endif
                                                    <input type="submit" class="btn btn-md btn-info pull-right" value="Valider">
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.post -->
                                        <!-- /.post -->
                                    </div>


                                    <div class="tab-pane" id="settings">

                                        <form class="form-horizontal" method="post" action="{{route('maire.gestion_own_maires.update', Auth::user())}}">
                                            {{csrf_field()}}
                                            {{method_field('put')}}
                                            <input type="hidden" name="allUp">
                                            <div class="form-group {{ $errors->has('firstName') ? ' has-error' : '' }}">
                                                <label for="firstName" class="col-sm-4 control-label">Prénom</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="firstName" name="firstName" value="{{$maire->firstName}}">
                                                    @if ($errors->has('firstName'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('firstName') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('lastName') ? ' has-error' : '' }}">
                                                <label for="lastName" class="col-sm-4 control-label">NOM</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="lastName" name="lastName" value="{{$maire->lastName}}">
                                                    @if ($errors->has('lastName'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('lastName') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('nationalIdentity') ? ' has-error' : '' }}">
                                                <label for="nationalIdentity" class="col-sm-4 control-label">N° Carte D'identité</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="nationalIdentity" name="nationalIdentity" value="{{$maire->nationalIdentity}}">
                                                    @if ($errors->has('nationalIdentity'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('nationalIdentity') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                                <label for="telephone" class="col-sm-4 control-label">Telephone</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="telephone" name="telephone" value="{{$maire->telephone}}">
                                                    @if ($errors->has('telephone'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('telephone') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email" class="col-sm-4 control-label">Email</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="email" name="email" value="{{$maire->email}}">
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('email') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('dateOfBirth') ? ' has-error' : '' }}">
                                                <label for="dateOfBirth" class="col-sm-4 control-label">Date de naissance</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="dateOfBirth" name="dateOfBirth" value="{{$maire->dateOfBirth}}">
                                                    @if ($errors->has('dateOfBirth'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('dateOfBirth') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('hireDate') ? ' has-error' : '' }}">
                                                <label for="hireDate" class="col-sm-4 control-label">Prise de fonction</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="hireDate" name="hireDate" value="{{$maire->hireDate}}">
                                                    @if ($errors->has('hireDate'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('hireDate') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->has('region_id') ? ' has-error' : '' }}">
                                                <label for="region_id" class="col-sm-4 control-label">Commune</label>

                                                <div class="col-sm-8">
                                                    <select name="commune_id" id="commune_id" class="form-control">
                                                        @foreach($communes as $commune)
                                                            <option value="{{old('commune_id') ? old('commune_id') : $commune->id}}" {{$commune->id==$maire->commune_id ? 'selected' : ''}}>{{$commune->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('commune_id'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('commune_id') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-offset-9 col-sm-3">
                                                    <button type="submit" class="btn btn-info form-control">Valider</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div>
                            <!-- /.nav-tabs-custom -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                </section>
                <!-- /.content -->
                <!-- /.content -->

            </div>
        </div>
    </div>
@endsection
