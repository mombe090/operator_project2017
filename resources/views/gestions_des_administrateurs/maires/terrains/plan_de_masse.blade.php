@php use Carbon\Carbon; @endphp
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que chef de quartier.
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Terrains/Parcelles</h3>
                        <div class="box-tools pull-right hidden-xs">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">

                                    @if(count($terrains)!=0)
                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">Liste des Parcelles en attente de validation par vous M. <span class="text-bold text-uppercase">{{Auth::user()->lastName}}</span> {{Auth::user()->firstName}}</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="box-body table-responsive no-padding">
                                                    <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th class="hidden-xs col-md-2">Code du terrain</th>
                                                        <th class="hidden-xs col-md-2">Propriétaire</th>
                                                        <th class="col-md-1 ">Construit</th>
                                                        <th class="col-md-2">Date d'ajout</th>
                                                        <th class="col-md-4">Selectionner le plan numérisé</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($terrains as $terrain)
                                                        <tr>
                                                            <td class="text-uppercase text-bold hidden-xs col-md-2">{{$terrain->codeSecteur}} </td>
                                                            <td class="text-uppercase text-bold hidden-xs col-md-2">
                                                                <a href="{{asset($terrain->picture)}}">
                                                                    <img src="{{asset($terrain->picture)}}" alt="" class="img-sm img-circle">
                                                                    {{$terrain->lastName}} {{$terrain->firstName}}
                                                                </a>
                                                            </td>

                                                            <td class="col-md-1 hidden-xs">{{$terrain->build=='yes' ? 'Oui' : 'Non'}}</td>
                                                            <td class="col-md-2 hidden-xs"><b class="text-uppercase text-bold">{{Carbon::parse($terrain->created_at)->format('d / F / Y')}}</b> </td>
                                                            <td class="col-md-1">

                                                                <form role="form" enctype="multipart/form-data" method="post" autocomplete="off" action="{{route('maire.mairie_terrains.update', $terrain->terrainID)}}" >
                                                                    {{csrf_field()}}
                                                                    {{method_field('put')}}
                                                                    <input type="hidden" name="planDeMasse" id="">
                                                                    <div class="box-body">
                                                                        <div class="form-group {{ $errors->has('planDeMasse') ? ' has-error' : '' }}" >
                                                                            <input type="file"  id="planDeMasse" name="planDeMasse" value="choisir une photo" class="" required >
                                                                            @if ($errors->has('planDeMasse'))
                                                                                <span class="help-block">
                                                                                     <strong>{{ $errors->first('planDeMasse') }}</strong>
                                                                                  </span>
                                                                            @endif
                                                                        </div>

                                                                        <input type="submit" class="btn btn-md btn-info form-control" value="Ajouter">
                                                                    </div>
                                                                </form>
                                                            </td>

                                                        </tr>
                                                    @endforeach
                                                    </tbody>


                                                </table>
                                                <div class="pull-right">
                                                    {{$terrains->links()}}
                                                </div>
                                            </div>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    @else
                                        <h2 class="text-red text-center">Il n'y pas de parcelles .</h2>
                                @endif
                                <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
