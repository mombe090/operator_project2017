
@extends('adminlte::page')

@section('htmlheader_title')
    Terrain en attente de validation par le maire.
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que maire de la commune de {{Auth::user()->commune->name}}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Affichage d'un(e) terrain/parcelle non validé(e) par le maire.</h3>
                        <div class="box-tools pull-right hidden-xs">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <h2 class="page-header">Voilà les information confirmées par  <b class="text-uppercase">{{$terrain->secteur->quartier->chefquartier->lastName}}</b> {{$terrain->secteur->quartier->chefquartier->firstName}} chef quartier de {{$terrain->secteur->quartier->name}}</h2>

                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Widget: user widget style 1 -->
                                    <div class="box box-widget widget-user-2">
                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                        <div class="widget-user-header bg-primary">
                                            <div class="widget-user-image">
                                                <img class="img-circle" src="{{asset(Auth::user()->picture)}}" alt="User Avatar">
                                            </div>
                                            <!-- /.widget-user-image -->
                                            <h3 class="widget-user-username"><span class="text-uppercase text-bold">{{Auth::user()->role=='chefSecteur' ? Auth::user()->lastName : $terrain->secteur->chefsecteur->lastName}}</span> {{Auth::user()->role=='chefSecteur' ? Auth::user()->firstName : $terrain->secteur->chefsecteur->firstName}}</h3>
                                            <h5 class="widget-user-desc">Chef du secteur {{Auth::user()->role=='chefSecteur' ? Auth::user()->secteur->name : $terrain->secteur->name}} dans lequel se trouve la parcelle.</h5>
                                        </div>
                                        <div class="box-footer no-padding">
                                            <ul class="nav nav-stacked">
                                                <li><a href="">Code du quartier <span class="pull-right badge bg-blue-active">{{$terrain->codeQuartier}}</span></a></li>
                                                <li><a href="">Forme <span class="pull-right badge bg-blue-active">{{$terrain->forme}}</span></a></li>
                                                <li><a href="">Dimension <span class="pull-right badge bg-blue-active">{{$terrain->longer}}*{{$terrain->larger}} <i>m*m</i></span></a></li>
                                                <li><a href="">Dommaine <span class="pull-right badge bg-blue-active">{{$terrain->dommaine}} </span></a></li>
                                                <li><a href="">Usage <span class="pull-right badge bg-blue-active">{{$terrain->usage}}</span></a></li>
                                                <li><a href="">Construit <span class="pull-right badge bg-blue-active">{{$terrain->build}}</span></a></li>
                                            </ul>
                                            <p>
                                            <h3>Attestation d'acquisition de la parcelle</h3>
                                            <img src="{{asset($terrain->attestationAchat)}}" alt="">
                                            </p>

                                            <p>
                                            <h3>Plan de masse</h3>
                                            <img src="{{asset($terrain->planDeMasse)}}" alt="">
                                            </p>

                                            <p>
                                                <h3>Titre Foncier</h3>
                                                <img src="{{asset($terrain->titreFoncier)}}" alt="">
                                            </p>

                                        </div>

                                        <div >
                                            <br>
                                            <form style="display: inline-block" action="{{route('maire.mairie_terrains.update', $terrain)}}" method="post" class="form-inline">
                                                {{csrf_field()}}
                                                {{method_field('put')}}
                                                <input type="hidden" name="valider">
                                                <button type="submit"  title="Supprimer la terrain/parcelle" class="btn btn-success  " onclick="return confirm('Voulez-vous vraiment confirmé l\'enregistrement de ce terrain ?')">Confirmer <span class="fa fa-thumbs-o-up"></span></button>
                                            </form>
                                            <a href="{{route('maire.mairie_terrains.edit', $terrain)}}" title="Modifier une terrain/parcelle" class="btn btn-warning pull-right">Modifier <span class="fa fa-edit"></span></a>
                                        </div>
                                    </div>
                                    <!-- /.widget-user -->
                                </div>
                            </div>
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
