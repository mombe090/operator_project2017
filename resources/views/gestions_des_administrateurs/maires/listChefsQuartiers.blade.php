@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    <span class="text-info">Liste des chefs de quartiers {{isset($confirm) ? 'Confirmés' : 'en attente de votre confirmation.'}}</span>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-success">
                                    <h3 class="box-title text-center text-bold">Concernés</h3>
                                </div>

                                <!-- /.box-header -->
                                @if($chefQuartiers->count()==0)

                                    <H3 class="text-center text-danger">Il n'y a pas d'inscription de super utilisateur en attente de confirmation.</H3>

                                @else

                                    <div class="box-body">
                                        <div class="box-body table-responsive no-padding">
                                            <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th class="">NOM</th>
                                                <th>Prénom</th>
                                                <th class="">email</th>
                                                <th class="">N° D'identité</th>
                                                <th class="">Quartier de</th>
                                                <th>Photo</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            @foreach($chefQuartiers as $chefQuartier)

                                                @if(Auth::user()->commune_id==$chefQuartier->quartier->commune_id)
                                                    <tr>
                                                        <td class="text-uppercase">{{$chefQuartier->lastName}}</td>
                                                        <td>{{$chefQuartier->firstName}}</td>

                                                        <td class="">{{$chefQuartier->email}}</td>
                                                        <td class="">{{$chefQuartier->nationalIdentity}}</td>
                                                        <td class="">{{$chefQuartier->quartier->name}}</td>
                                                        <td>
                                                            <p>
                                                                <a href="{{asset($chefQuartier->picture)}}" title="Afficher la photo du super administrateur."> <img src="{{asset($chefQuartier->picture)}}" alt="Image de super utilisateur" class="img-sm img-circle img-bordered-sm"></a>
                                                            </p>
                                                        </td>
                                                        <td>


                                                            <form action="{{route("maire.gestion_chefs_de_quartiers.show", $chefQuartier)}}" method="get" class="form-inline" style="display: inline-block">
                                                                <button type="submit" title="Afficher toutes les informations du super utilisateur" class="btn btn-xs btn-primary fa fa-eye" ></button>
                                                            </form>


                                                            @if(isset($confirm))
                                                                <form action="{{route("maire.gestion_chefs_de_quartiers.edit", $chefQuartier)}}" method="get" class="form-inline" style="display: inline-block">

                                                                    <button type="submit"  title="Modification des informations d'un  administrateur préfectorales" class="btn btn-xs btn-warning fa fa-edit" onclick="return confirm('Etes-vous sûr de vouloir modifier les informations de cet administrateur préfectorale')"></button>

                                                                </form>
                                                            @else
                                                                <form action="{{route("maire.gestion_chefs_de_quartiers.update", $chefQuartier)}}" method="post" class="form-inline" style="display: inline-block">
                                                                    {{csrf_field()}}
                                                                    {{method_field('put')}}
                                                                    <input type="hidden" name="confirm" id="">

                                                                    <button type="submit"  title="Confirmation d'un chef de quartier" class="btn btn-xs btn-success fa fa-thumbs-o-up" onclick="return confirm('Etes-vous sûr de vouloir confirmer l\'inscription de ce chef de quartier.')"></button>

                                                                </form>
                                                            @endif


                                                            <form action="{{route("maire.gestion_chefs_de_quartiers.destroy", $chefQuartier)}}" method="post" class="form-inline" style="display: inline-block">
                                                                {{csrf_field()}}
                                                                {{method_field('delete')}}

                                                                <button type="submit"  title="Supression d'un maire de commune." class="btn btn-xs btn-danger fa fa-remove" onclick="return confirm('Etes-vous sûr de vouloir supprimer chef de quartier ?')"></button>

                                                            </form>

                                                        </td>

                                                    </tr>
                                                    {{--On gère pour la ville de conakry--}}
                                                @elseif(Auth::user()->prefecture_id==34 AND ($chefQuartier->commune_id>=34 AND $chefQuartier->commune_id<=38))
                                                    <tr>
                                                        <td class="text-uppercase">{{$chefQuartier->lastName}}</td>
                                                        <td>{{$chefQuartier->firstName}}</td>

                                                        <td class="">{{$chefQuartier->email}}</td>
                                                        <td class="">{{$chefQuartier->nationalIdentity}}</td>
                                                        <td class="">{{$chefQuartier->commune->name}}</td>
                                                        <td>
                                                            <p>
                                                                <a href="{{asset($chefQuartier->picture)}}" title="Afficher la photo du super administrateur."> <img src="{{asset($chefQuartier->picture)}}" alt="Image de super utilisateur" class="img-sm img-circle img-bordered-sm"></a>
                                                            </p>
                                                        </td>
                                                        <td>


                                                            <form action="{{route("admin.gestion_maires.show", $chefQuartier)}}" method="get" class="form-inline" style="display: inline-block">
                                                                <button type="submit" title="Afficher toutes les informations du super utilisateur" class="btn btn-xs btn-primary fa fa-eye" ></button>
                                                            </form>


                                                            @if(isset($confirm))
                                                                <form action="{{route("admin.gestion_maires.edit", $chefQuartier)}}" method="get" class="form-inline" style="display: inline-block">

                                                                    <button type="submit"  title="Modification des informations d'un  administrateur préfectorales" class="btn btn-xs btn-warning fa fa-edit" onclick="return confirm('Etes-vous sûr de vouloir modifier les informations de cet administrateur préfectorale')"></button>

                                                                </form>
                                                            @else
                                                                <form action="{{route("admin.gestion_maires.update", $chefQuartier)}}" method="post" class="form-inline" style="display: inline-block">
                                                                    {{csrf_field()}}
                                                                    {{method_field('put')}}
                                                                    <input type="hidden" name="confirm" id="">

                                                                    <button type="submit"  title="Confirmation d'un maire de commune" class="btn btn-xs btn-success fa fa-thumbs-o-up" onclick="return confirm('Etes-vous sûr de vouloir confirmer l\'inscription de cet maire')"></button>

                                                                </form>
                                                            @endif


                                                            <form action="{{route("admin.gestion_maires.destroy", $chefQuartier)}}" method="post" class="form-inline" style="display: inline-block">
                                                                {{csrf_field()}}
                                                                {{method_field('delete')}}

                                                                <button type="submit"  title="Supression d'un maire de commune." class="btn btn-xs btn-danger fa fa-remove" onclick="return confirm('Etes-vous sûr de vouloir supprimer cet maire')"></button>

                                                            </form>

                                                        </td>

                                                    </tr>
                                                @endif
                                            @endforeach
                                            <tbody>

                                            </tbody>
                                            <tfoot>

                                            </tfoot>
                                        </table>

                                    </div>
                                    </div>
                            @endif

                            <!-- /.box-body -->
                            </div>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->

            </div>
        </div>
    </div>
@endsection
