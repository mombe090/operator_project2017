@php use Carbon\Carbon; @endphp
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que Maire de la commune de {{Auth::user()->commune->name}}
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Terrains/Parcelles</h3>
                        <div class="box-tools pull-right ">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">

                                    @if(count($quartiers)!=0)
                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">Liste des quartiers de la commune de {{Auth::user()->commune->name}}</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="box-body table-responsive no-padding">
                                                    <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th class=" ">Code</th>
                                                        <th class="">Nom</th>
                                                        <th class="">Nombre secteurs</th>
                                                        <th class="">Chef quartiers</th>
                                                        <th class="">Telephone</th>
                                                        {{--<th class=" ">Nombres de parcelles</th>--}}
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($quartiers as $quartier)
                                                        <tr>
                                                            <td class="text-uppercase text-bold  ">{{$quartier->code}} </td>
                                                            <td class="">{{$quartier->name}}</td>
                                                            <td class=""><b class="text-uppercase">{{$quartier->secteurs->count()}}</b></td>
                                                            <td class=" "><span class="text-uppercase text-bold">{{$quartier->chefquartier['lastName']}}</span> {{$quartier->chefquartier['firstName']}}</td>
                                                            <td> {{$quartier->chefquartier['telephone']}}</td>


                                                        </tr>
                                                    @endforeach
                                                    </tbody>


                                                </table>
                                                    <div class="pull-right">
                                                        {{$quartiers->links()}}
                                                    </div>
                                            </div>
                                            </div>

                                            <!-- /.box-body -->
                                        </div>
                                    @else
                                        <h2 class="text-red text-center">Il n'y pas de parcelle valider dans le quartier. Svp veillez exiger à vos chefs secteurs de rajouter des parcelles à la base de donnée.</h2>
                                @endif
                                <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
