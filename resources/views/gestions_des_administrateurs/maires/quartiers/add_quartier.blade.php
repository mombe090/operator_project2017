
@extends('adminlte::page')

@section('htmlheader_title')
    Ajout d'un quartier dans un quartier.
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que maire de la commune de {{Auth::user()->commune->name}}
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ajouter un quartier  dans votre commune.</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <!-- left column -->
                                <div class="col-md-6 col-md-offset-3 well">
                                    <!-- general form elements -->
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Veillez remplir les données.</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->
                                        <form role="form" method="post" autocomplete="off" action="{{route('maire.mairie_quartier.store')}}">
                                            {{csrf_field()}}
                                            <div class="box-body">
                                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}" >
                                                    <label for="name">Nom du quartier </label>
                                                    <input type="text" class="form-control" id="name" placeholder="Exemple : Lambandji, Kipé ..." name="name" value="{{old('name')}}" >
                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('name') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                            </div>

                                                <!-- /.box-body -->

                                                <div class="box-footer">
                                                    <button type="submit" class="btn btn-primary">Valider</button>
                                                </div>

                                        </form>
                                    </div>
                                    <!-- /.box -->

                                </div>
                                <!--/.col (left) -->

                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
