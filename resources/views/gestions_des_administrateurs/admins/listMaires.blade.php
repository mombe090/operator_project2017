@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    <span class="text-info">Liste des administrateurs {{isset($confirm) ? 'Confirmés' : 'en attente de votre confirmation.'}}</span>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-success">
                                    <h3 class="box-title text-center text-bold">Concernés</h3>
                                </div>

                                <!-- /.box-header -->
                                @if($maires->count()==0)

                                    <H3 class="text-center text-danger">Il n'y a pas d'inscription de super utilisateur en attente de confirmation.</H3>

                                @else

                                    <div class="box-body">
                                        <table id="example2" class="datatables table table-bordered table-hover table-responsive">
                                            <thead>
                                            <tr>
                                                <th class="">NOM</th>
                                                <th>Prénom</th>
                                                <th class="hidden-xs">email</th>
                                                <th class="hidden-xs">N° D'identité</th>
                                                <th class="hidden-xs">Mairie de</th>
                                                <th>Photo</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            @foreach($maires as $maire)

                                                @if(Auth::user()->prefecture_id==$maire->commune_id)
                                                    <tr>
                                                        <td class="text-uppercase">{{$maire->lastName}}</td>
                                                        <td>{{$maire->firstName}}</td>

                                                        <td class="hidden-xs">{{$maire->email}}</td>
                                                        <td class="hidden-xs">{{$maire->nationalIdentity}}</td>
                                                        <td class="hidden-xs">{{$maire->commune->name}}</td>
                                                        <td>
                                                            <p>
                                                                <a href="{{asset($maire->picture)}}" title="Afficher la photo du super administrateur."> <img src="{{asset($maire->picture)}}" alt="Image de super utilisateur" class="img-sm img-circle img-bordered-sm"></a>
                                                            </p>
                                                        </td>
                                                        <td>


                                                            <form action="{{route("admin.gestion_maires.show", $maire)}}" method="get" class="form-inline" style="display: inline-block">
                                                                <button type="submit" title="Afficher toutes les informations du super utilisateur" class="btn btn-xs btn-primary fa fa-eye" ></button>
                                                            </form>


                                                            @if(isset($confirm))
                                                                <form action="{{route("admin.gestion_maires.edit", $maire)}}" method="get" class="form-inline" style="display: inline-block">

                                                                    <button type="submit"  title="Modification des informations d'un  administrateur préfectorales" class="btn btn-xs btn-warning fa fa-edit" onclick="return confirm('Etes-vous sûr de vouloir modifier les informations de cet administrateur préfectorale')"></button>

                                                                </form>
                                                            @else
                                                                <form action="{{route("admin.gestion_maires.update", $maire)}}" method="post" class="form-inline" style="display: inline-block">
                                                                    {{csrf_field()}}
                                                                    {{method_field('put')}}
                                                                    <input type="hidden" name="confirm" id="">

                                                                    <button type="submit"  title="Confirmation d'un maire de commune" class="btn btn-xs btn-success fa fa-thumbs-o-up" onclick="return confirm('Etes-vous sûr de vouloir confirmer l\'inscription de cet maire')"></button>

                                                                </form>
                                                            @endif


                                                            <form action="{{route("admin.gestion_maires.destroy", $maire)}}" method="post" class="form-inline" style="display: inline-block">
                                                                {{csrf_field()}}
                                                                {{method_field('delete')}}

                                                                <button type="submit"  title="Supression d'un maire de commune." class="btn btn-xs btn-danger fa fa-remove" onclick="return confirm('Etes-vous sûr de vouloir supprimer cet maire')"></button>

                                                            </form>

                                                        </td>

                                                    </tr>
                                                    {{--On gère pour la ville de conakry--}}
                                                @elseif(Auth::user()->prefecture_id==34 AND ($maire->commune_id>=34 AND $maire->commune_id<=38))
                                                    <tr>
                                                        <td class="text-uppercase">{{$maire->lastName}}</td>
                                                        <td>{{$maire->firstName}}</td>

                                                        <td class="hidden-xs">{{$maire->email}}</td>
                                                        <td class="hidden-xs">{{$maire->nationalIdentity}}</td>
                                                        <td class="hidden-xs">{{$maire->commune->name}}</td>
                                                        <td>
                                                            <p>
                                                                <a href="{{asset($maire->picture)}}" title="Afficher la photo du super administrateur."> <img src="{{asset($maire->picture)}}" alt="Image de super utilisateur" class="img-sm img-circle img-bordered-sm"></a>
                                                            </p>
                                                        </td>
                                                        <td>


                                                            <form action="{{route("admin.gestion_maires.show", $maire)}}" method="get" class="form-inline" style="display: inline-block">
                                                                <button type="submit" title="Afficher toutes les informations du super utilisateur" class="btn btn-xs btn-primary fa fa-eye" ></button>
                                                            </form>


                                                            @if(isset($confirm))
                                                                <form action="{{route("admin.gestion_maires.edit", $maire)}}" method="get" class="form-inline" style="display: inline-block">

                                                                    <button type="submit"  title="Modification des informations d'un  administrateur préfectorales" class="btn btn-xs btn-warning fa fa-edit" onclick="return confirm('Etes-vous sûr de vouloir modifier les informations de cet administrateur préfectorale')"></button>

                                                                </form>
                                                            @else
                                                                <form action="{{route("admin.gestion_maires.update", $maire)}}" method="post" class="form-inline" style="display: inline-block">
                                                                    {{csrf_field()}}
                                                                    {{method_field('put')}}
                                                                    <input type="hidden" name="confirm" id="">

                                                                    <button type="submit"  title="Confirmation d'un maire de commune" class="btn btn-xs btn-success fa fa-thumbs-o-up" onclick="return confirm('Etes-vous sûr de vouloir confirmer l\'inscription de cet maire')"></button>

                                                                </form>
                                                            @endif


                                                            <form action="{{route("admin.gestion_maires.destroy", $maire)}}" method="post" class="form-inline" style="display: inline-block">
                                                                {{csrf_field()}}
                                                                {{method_field('delete')}}

                                                                <button type="submit"  title="Supression d'un maire de commune." class="btn btn-xs btn-danger fa fa-remove" onclick="return confirm('Etes-vous sûr de vouloir supprimer cet maire')"></button>

                                                            </form>

                                                        </td>

                                                    </tr>
                                                @endif
                                            @endforeach
                                            <tbody>

                                            </tbody>
                                            <tfoot>

                                            </tfoot>
                                        </table>
                                    </div>
                            @endif

                            <!-- /.box-body -->
                            </div>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->

            </div>
        </div>
    </div>
@endsection
