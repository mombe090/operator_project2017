@php use Carbon\Carbon; @endphp
@extends('adminlte::page')

@section('htmlheader_title')
    Change Title here!
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que Directeur Général des Dommaines et Cadastre de la ville {{Auth::user()->prefecture->nom}}.
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Terrains/Parcelles</h3>
                        <div class="box-tools pull-right hidden-xs">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">

                                    @if(count($terrains)!=0)
                                        <div class="box">
                                            <div class="box-header">
                                                <h3 class="box-title">Liste des Parcelles validées.</h3>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div class="box-body table-responsive no-padding">
                                                    <div class="box-body table-responsive no-padding">
                                                        <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th class="hidden-xs">Code communal</th>
                                                            <th class=>Propriétaire</th>
                                                            <th class=>N° Identité</th>
                                                            <th class=" ">Dimensions</th>
                                                            <th class=" ">Forme</th>
                                                            <th class="">Dommaine</th>
                                                            <th class="">Utilisation</th>
                                                            <th class=" ">Construit</th>
                                                            <th class="">Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($terrains as $terrain)
                                                            <tr>
                                                                <td class="text-uppercase text-bold hidden-xs">{{$terrain->codeCommune}} </td>
                                                                <td class="text-uppercase text-bold">{{$terrain->lastName}} {{$terrain->firstName}} </td>
                                                                <td class="text-uppercase text-bold">{{$terrain->nationalIdentity}} </td>
                                                                <td class=" ">{{$terrain->longer * $terrain->larger}} <span class="text-red">m*m</span></td>
                                                                <td class=" ">{{$terrain->forme}}</td>
                                                                <td class="">{{$terrain->dommaine}}</td>
                                                                <td class="">{{$terrain->usage}}</td>
                                                                <td class=" ">{{$terrain->build=='yes' ? 'Oui' : 'Non'}}</td>
                                                                <td class="">

                                                                    <a href="{{route('admin.domaines_cadastre.show', $terrain->terrainID)}}" title="Modifier une terrain/parcelle" class="btn btn-xs btn-info  fa fa-eye"></a>

                                                                    <a href="{{route('admin.domaines_cadastre.edit', $terrain->terrainID)}}" title="Modifier une terrain/parcelle" class="btn btn-xs btn-warning  fa fa-edit"></a>

                                                                    <form style="display: inline-block" action="{{route('admin.domaines_cadastre.destroy', $terrain->terrainID)}}" method="post" class="form-inline">
                                                                        {{csrf_field()}}
                                                                        {{method_field('delete')}}
                                                                        <input type="hidden" name="delete_land_not_validate_by_chef_quartier">
                                                                        <button type="submit"  title="Supprimer la terrain/parcelle" class="btn btn-xs btn-danger fa fa-remove" onclick="return confirm('Voulez-vous vraiment suppremer l\'enregistrement de ce terrain ?')"></button>
                                                                    </form>
                                                                </td>

                                                            </tr>
                                                        @endforeach
                                                        </tbody>


                                                    </table>

                                                    <div class="pull-right">
                                                        {{$terrains->links()}}
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    @else
                                        <h2 class="text-red text-center">Il n'y pas de parcelle valider dans le quartier. Svp veillez exiger à vos chefs secteurs de rajouter des parcelles à la base de donnée.</h2>
                                @endif
                                <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
