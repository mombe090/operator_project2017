
@extends('adminlte::page')

@section('htmlheader_title')
    Modifcation d'un terrain confirmé par le chef de quartier.
@endsection
@section('contentheader_title')
    Vous êtes connecter en tant que Directeur Général des Dommaines et Cadastre de la ville {{Auth::user()->prefecture->nom}}.
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">

                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Modification d'un(e) terrain/parcelle dans  {{Auth::user()->role=='chefSecteur' ? 'votre secteur' : 'le secteur '.$terrain->secteur->name}}..</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <!-- left column -->
                                <div class="col-md-6 col-md-offset-3 well">
                                    <!-- general form elements -->
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Faites vos modifications dans les champs ci-dessous.</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->
                                        <form role="form" method="post" autocomplete="off" action="{{route('maire.mairie_terrains.update', $terrain)}}">
                                            {{csrf_field()}}
                                            {{method_field('put')}}
                                            <input type="hidden" name="allUp" id="">
                                            <input type="hidden" name="old" id="">
                                            <div class="box-body">
                                                <div class="form-group {{ $errors->has('longer') ? ' has-error' : '' }}" >
                                                    <label for="longer">Longueur de la parcelle <span class="text-red">(en m)</span></label>
                                                    <input type="text" class="form-control" id="longer" placeholder="Exemple : 25.67" name="longer" value="{{old('longer') ? old('longer') : $terrain->longer}}" >
                                                    @if ($errors->has('longer'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('longer') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->has('larger') ? ' has-error' : '' }}" >
                                                    <label for="larger">Largeur de la parcelle <span class="text-red">(en m)</span></label>
                                                    <input type="text" class="form-control" id="larger" placeholder="26.56" name="larger" value="{{old('larger') ? old('larger') : $terrain->longer}}">
                                                    @if ($errors->has('larger'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('larger') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>


                                                <div class="form-group {{ $errors->has('dommaine') ? ' has-error' : '' }}" >
                                                    <label for="dommaine">Dommaine</label>
                                                    <select name="dommaine" id="dommaine" class="form-control">
                                                        @foreach($dommaines as $dommaine)
                                                            <option value="{{$dommaine->id }}" {{ $dommaine->name==$terrain->dommaine ? 'selected' : '' }} class="text-capitalize"><span class="text-capitalize">{{$dommaine->name}}</span></option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('dommaine'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('dommaine') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                                <div class="form-group {{ $errors->has('forme') ? ' has-error' : '' }}" >
                                                    <label for="forme">Forme de la parcelle</label>
                                                    <select name="forme" id="forme" class="form-control">
                                                        <option value="Rectangulaire" {{ $terrain->forme =='Rectangulaire' ? 'selected' : ''}}>Rectangulaire</option>
                                                        <option value="Triangulaire" {{$terrain->forme=='Triangulaire' ? 'selected' : ''}}>Triangulaire</option>
                                                        <option value="Autre" {{$terrain->forme=='Autre' ? 'selected' : ''}}>Autre</option>
                                                    </select>
                                                    @if ($errors->has('forme'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('forme') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>


                                                <div class="form-group {{ $errors->has('usage') ? ' has-error' : '' }}" >
                                                    <label for="usage">Utilsation</label>
                                                    <select name="usage" id="usage" class="form-control">
                                                        @foreach($usages as $usage)
                                                            <option value="{{$usage->id}}" {{$terrain->usage==$usage->name ? 'selected' : ''}}>{{$usage->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('usage'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('usage') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                                <div class="form-group {{ $errors->has('build') ? ' has-error' : '' }}" >
                                                    <label for="build">Construit</label>
                                                    <select name="build" id="build" class="form-control">
                                                        <option value="yes" {{$terrain->build=='yes' ? 'selected' : ''}}>Oui</option>
                                                        <option value="no" {{$terrain->build=='no' ? 'selected' : ''}}>Non</option>
                                                    </select>
                                                    @if ($errors->has('build'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('build') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>
                                                <div class="form-group {{ $errors->has('codeCommune') ? ' has-error' : '' }}" >
                                                    <label for="codeCommune">N° Parcelle au niveau de votre secteur</label>
                                                    <input type="text" name="codeCommune" id="codeCommune" placeholder="Exemple : 000001" class="form-control" value="{{old('codeCommune') ? old('codeCommune') :
                                                    $terrain->codeCommune}}">
                                                    @if ($errors->has('codeCommune'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('codeCommune') }}</strong>
                                                       </span>
                                                    @endif
                                                </div>

                                            </div>
                                            <!-- /.box-body -->

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">Valider</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.box -->

                                </div>
                                <!--/.col (left) -->

                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection
