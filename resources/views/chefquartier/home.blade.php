@extends('adminlte::layouts.app')

@section('htmlheader_title')
    MTN Profile du super utilisateur
@endsection
@section('contentheader_title')
    Vous êtes connecté en tant que Chef Quartier.
@endsection

@section('main-content')

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{Auth::user()->quartier->secteurs->count()}}</h3>

                        <p>Secteurs</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$chefSecteurs->where('working', 'on')->count()}}</h3>

                        <p>Chefs secteurs verifiés</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-maroon-active">
                    <div class="inner">
                        <h3>{{$chefSecteurs->where('working', 'off')->count()}}</h3>

                        <p>Chefs secteurs en attente.</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>
                            {{$parcelles->where('verifiedByChefQuartier', 'yes')->count()}}
                        </h3>

                        <p>Parcelles verifiés</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow-active">
                    <div class="inner">
                        <h3>
                            {{$parcelles->where('verifiedByChefQuartier', 'no')->count()}}
                        </h3>

                        <p>Parcelles en attente</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>{{$parcelles->where('verifiedByChefQuartier', 'yes')->where('dommaine', 'etat')->count()}}</h3>

                        <p>Domaines de l'Etat</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>



        </div>
        <div class="row">

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$parcelles->where('verifiedByChefQuartier', 'yes')->where('dommaine', 'ambassade')->count()}}</h3>

                        <p>Embassade</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-maroon-active">
                    <div class="inner">
                        <h3>{{$parcelles->where('verifiedByChefQuartier', 'yes')->where('dommaine', 'prive')->where('usage', 'habitation')->count()}}</h3>

                        <p>Habitation Privé</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>


            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-fuchsia-active">
                    <div class="inner">
                        <h3>{{$parcelles->where('verifiedByChefQuartier', 'yes')->where('dommaine', 'etat')->where('usage', 'hopital')->count()}}</h3>

                        <p>Hôpitaux</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$parcelles->where('verifiedByChefQuartier', 'yes')->where('dommaine', 'etat')->where('usage', 'ecole')->count()}}</h3>

                        <p>Ecoles Publiques</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-teal-active">
                    <div class="inner">
                        <h3>{{$parcelles->where('verifiedByChefQuartier', 'yes')->where('dommaine', 'prive')->where('usage', 'ecole')->count()}}</h3>

                        <p>Ecoles Privées</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3>
                            {{$parcelles->where('verifiedByChefQuartier', 'yes')->where('dommaine', 'etat')->where('usage', 'mosque')->count()}}
                        </h3>

                        <p>Mosquées</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-purple-active">
                    <div class="inner">
                        <h3> {{$parcelles->where('verifiedByChefQuartier', 'yes')->where('dommaine', 'etat')->where('usage', 'eglise')->count()}}</h3>

                        <p>Eglises</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Afficher plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>




        </div>


    </section>
@endsection
