@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Enregistrement d'un administrateur.
@endsection

@section('content')

    <body class="hold-transition register-page">
    <div id="app" v-cloak>
        <div class="register-box">
            <div class="register-logo">
                <a href="{{ url('/chefquartier/home') }}"><b>MTN</b>Yello Care Challenge</a>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session('emailConfirmation'))
                @include('partials.emailConfirmation')
            @endif
            @if(session('emailConfirmationError'))
                @include('partials.emailConfirmationError')
            @endif
            @if(session('emailConfirmationInfo'))
                @include('partials.emailConfirmationInfo')
            @endif
            <div class="register-box-body">
                <p class="login-box-msg">Enregistrement d'un  chef de quartier.</p>
                <form action="{{ url('/chefquartier/register') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="communeID" value="{{ $_GET['commune'] }}">

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Le prenom ici. Ex : Mamadou, Malick, ..." name="firstName" value="{{ old('firstName') }}" autofocus/>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Le nom ici. Ex : DIALLO, KAMANO, ..." name="lastName" value="{{ old('lastName') }}" autofocus/>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Votre n° carte d'identité. Ex :  " name="nationalIdentity" value="{{ old('nationalIdentity') }}" autofocus/>
                        <span class="glyphicon glyphicon-check form-control-feedback"></span>
                    </div>

                    {{--  @if (config('auth.providers.users.field','email') === 'username')
                          <div class="form-group has-feedback">
                              <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.username') }}" name="username" autofocus/>
                              <span class="glyphicon glyphicon-user form-control-feedback"></span>
                          </div>
                      @endif--}}

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="n° telephone. Ex: 666000000" name="telephone" value="{{ old('telephone') }}"/>
                        <span class="fa fa-phone form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="{{ trans('adminlte_lang::message.email') }}" name="email" value="{{ old('email') }}"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <div class="row">
                            <p class="text-center">
                                Date de naissance
                            </p>
                            <div class="col-md-4 col-sm-12">
                                <select name="jour" id="" class="form-control">

                                    @for($i=1; $i<31; $i++)

                                        @if($i < 10)
                                            <option value="{{old('jour') ? old('jour') : $i}}">{{old('jour') ? old('jour') : $i}}</option>
                                        @else
                                            <option value="{{old('jour') ? old('jour') : $i}}">{{old('jour') ? old('jour') : $i}}</option>
                                        @endif

                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <select name="mois" id="" class="form-control">
                                    @foreach($months as $month)
                                        <option value="{{old('mois') ? old('mois') :  $month->id}}">{{old('mois') ? $month->name :  $month->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <select name="annee" id="" class="form-control">

                                    @for( $i = Carbon\Carbon::now()->year; $i > 1950 ;$i--)
                                        <option value="{{ old('annee') ? old('annee') : $i}}">{{ old('annee') ? old('annee') : $i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group has-feedback">
                        <div class="row">
                            <p class="text-center">
                                Date de d'embauchement
                            </p>
                            <div class="col-md-4 col-sm-12">
                                <select name="jourE" id="" class="form-control">

                                    @for($i=1; $i<31; $i++)

                                        @if($i < 10)
                                            <option value="{{old('jourE') ? old('jourE') : '0'.$i}}">{{old('jourE') ? old('jourE') : '0'.$i}}</option>
                                        @else
                                            <option value="{{old('jourE') ? old('jourE') : $i}}">{{old('jourE') ? old('jourE') : $i}}</option>
                                        @endif

                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <select name="moisE" id="" class="form-control">
                                    @foreach($months as $month)
                                        <option value="{{old('moisE') ? old('moisE') :  $month->id}}">{{old('moisE') ? $month->name :  $month->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <select name="anneeE" id="" class="form-control">

                                    @for( $i = Carbon\Carbon::now()->year; $i > 1950 ;$i--)
                                        <option value="{{ old('anneeE') ? old('anneeE') : $i}}">{{ old('anneeE') ? old('anneeE') : $i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group has-feedback">
                        <p>
                            Selectionnez votre quartier
                        </p>
                        <select name="quartier_id" id="" class="form-control">
                            @foreach($quartiers as $quartier)
                                <option value="{{old('quartier_id') ? old('quartier_id') : $quartier->id}}">{{old('quartier_id') ? $quartier->name : $quartier->name}}</option>
                            @endforeach
                        </select>

                    </div>


                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}" name="password"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Confirmez le mot de passe" name="password_confirmation"/>
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    </div>
                    <div class="row">


                        <div class="">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('adminlte_lang::message.register') }}</button>
                        </div><!-- /.col -->
                    </div>
                </form>



                <a href="{{ url('/chefquartier/login') }}" class="text-center">{{ trans('adminlte_lang::message.membreship') }}</a>
            </div><!-- /.form-box -->
        </div><!-- /.register-box -->
    </div>

    @include('adminlte::layouts.partials.scripts_auth')

    @include('adminlte::auth.terms')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>

    </body>

@endsection