@extends('adminlte::layouts.auth')

@section('htmlheader_title')
   Reinitialisation de mot de passe.
@endsection

@section('content')

    <body class="login-page">
    <div id="app">

        <div class="login-box">
            <div class="login-logo">
                <a href="{{ url('/home') }}"><b>MTN</b>Yello Care Challenge</a>
            </div><!-- /.login-logo -->

            @if (session('status'))
                <div class="alert alert-success">
                   Le lien à été envoyé avec succès.
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    Impossible de trouver une correspondance dans la base de donnée.
                </div>
            @endif

            <div class="login-box-body">
                <p class="login-box-msg">Reinitialisation de mot de passe</p>
                <form action="{{ url('/chefquartier/password/email') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" autofocus/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>

                    <div class="row">
                        <div class="col-xs-2">
                        </div><!-- /.col -->
                        <div class="col-xs-8">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Demander</button>
                        </div><!-- /.col -->
                        <div class="col-xs-2">
                        </div><!-- /.col -->
                    </div>
                </form>

                <a href="{{ url('/chefquartier/login') }}">Se connecter</a><br>


            </div><!-- /.login-box-body -->

        </div><!-- /.login-box -->
    </div>

    @include('adminlte::layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    </body>

@endsection