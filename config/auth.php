<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */

    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],

    /*
    |--------------------------------------------------------------------------
    | Authentication Guards
    |--------------------------------------------------------------------------
    |
    | Next, you may define every authentication guard for your application.
    | Of course, a great default configuration has been defined for you
    | here which uses session storage and the Eloquent user provider.
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | Supported: "session", "token"
    |
    */

    'guards' => [
        'root' => [
            'driver' => 'session',
            'provider' => 'roots',
        ],

        'proprietaire' => [
            'driver' => 'session',
            'provider' => 'proprietaires',
        ],

        'maire' => [
            'driver' => 'session',
            'provider' => 'maires',
        ],

        'chefquartier' => [
            'driver' => 'session',
            'provider' => 'chefquartiers',
        ],

        'chefsecteur' => [
            'driver' => 'session',
            'provider' => 'chefsecteurs',
        ],

        'superadmin' => [
            'driver' => 'session',
            'provider' => 'superadmins',
        ],

        'admin' => [
            'driver' => 'session',
            'provider' => 'admins',
        ],

        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'token',
            'provider' => 'users',
        ],
        'admins' => [
            'driver' => 'session',
            'provider' => 'admins',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | User Providers
    |--------------------------------------------------------------------------
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | If you have multiple user tables or models you may configure multiple
    | sources which represent each model / table. These sources may then
    | be assigned to any extra authentication guards you have defined.
    |
    | Supported: "database", "eloquent"
    |
    */

    'providers' => [
        'roots' => [
            'driver' => 'eloquent',
            'model' => App\Root::class,
        ],

        'proprietaires' => [
            'driver' => 'eloquent',
            'model' => App\Proprietaire::class,
        ],

        'maires' => [
            'driver' => 'eloquent',
            'model' => App\Maire::class,
        ],

        'chefquartiers' => [
            'driver' => 'eloquent',
            'model' => App\ChefQuartiers::class,
        ],

        'chefsecteurs' => [
            'driver' => 'eloquent',
            'model' => App\ChefSecteurs::class,
        ],

        'superadmins' => [
            'driver' => 'eloquent',
            'model' => App\Superadmin::class,
        ],

        'users' => [
            'driver' => 'eloquent',
            'model' => App\User::class,
        ],
        'admins' => [
            'driver' => 'eloquent',
            'model' => App\Admin::class,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Resetting Passwords
    |--------------------------------------------------------------------------
    |
    | You may specify multiple password reset configurations if you have more
    | than one user table or model in the application and you want to have
    | separate password reset settings based on the specific user types.
    |
    | The expire time is the number of minutes that the reset token should be
    | considered valid. This security feature keeps tokens short-lived so
    | they have less time to be guessed. You may change this as needed.
    |
    */

    'passwords' => [
        'roots' => [
            'provider' => 'roots',
            'table' => 'root_password_resets',
            'expire' => 60,
        ],

        'proprietaires' => [
            'provider' => 'proprietaires',
            'table' => 'proprietaire_password_resets',
            'expire' => 60,
        ],

        'maires' => [
            'provider' => 'maires',
            'table' => 'maire_password_resets',
            'expire' => 60,
        ],

        'chefquartiers' => [
            'provider' => 'chefquartiers',
            'table' => 'chefquartier_password_resets',
            'expire' => 60,
        ],

        'chefsecteurs' => [
            'provider' => 'chefsecteurs',
            'table' => 'chefsecteur_password_resets',
            'expire' => 60,
        ],

        'superadmins' => [
            'provider' => 'superadmins',
            'table' => 'superadmin_password_resets',
            'expire' => 60,
        ],

        'admins' => [
            'provider' => 'admins',
            'table' => 'admin_password_resets',
            'expire' => 60,
        ],

        'users' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
        ],
        'admins' => [
            'provider' => 'admins',
            'table' => 'password_resets',
            'expire' => 60,
        ],
    ],

];
