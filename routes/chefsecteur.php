<?php

use App\Proprietaire;

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('chefsecteur')->user();

    $parcelles = DB::table('secteurs')
        ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
        ->join('chefsecteurs', 'chefsecteurs.secteur_id', '=', 'terrains.secteur_id')
        ->where('chefsecteurs.id', Auth::user()->id)
        ->where('terrains.verifiedByChefQuartier', 'yes')
        ->count();

    $parcellesEtatiques = DB::table('secteurs')
        ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
        ->join('chefsecteurs', 'chefsecteurs.secteur_id', '=', 'terrains.secteur_id')
        ->where('chefsecteurs.id', Auth::user()->id)
        ->where('terrains.verifiedByChefQuartier', 'yes')
        ->where('terrains.dommaine', 'etat')
        ->count();

    $embassades = DB::table('secteurs')
        ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
        ->join('chefsecteurs', 'chefsecteurs.secteur_id', '=', 'terrains.secteur_id')
        ->where('chefsecteurs.id', Auth::user()->id)
        ->where('terrains.verifiedByChefQuartier', 'yes')
        ->where('terrains.dommaine', 'ambassade')
        ->count();

    $familles = DB::table('secteurs')
        ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
        ->join('chefsecteurs', 'chefsecteurs.secteur_id', '=', 'terrains.secteur_id')
        ->where('chefsecteurs.id', Auth::user()->id)
        ->where('terrains.verifiedByChefQuartier', 'yes')
        ->where('terrains.dommaine', 'prive')
        ->where('terrains.usage', 'habitation')
        ->count();

    $hopitaux = DB::table('quartiers')
        ->join('secteurs', 'quartiers.id', '=', 'secteurs.quartier_id')
        ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
        ->where('quartiers.id', Auth::user()->id)
        ->where('terrains.verifiedByChefQuartier', 'yes')
        ->where('terrains.dommaine', 'etat')
        ->where('terrains.usage', 'hopital')
        ->count();

    $ecoles =  DB::table('quartiers')
        ->join('secteurs', 'quartiers.id', '=', 'secteurs.quartier_id')
        ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
        ->where('quartiers.id', Auth::user()->id)
        ->where('terrains.verifiedByChefQuartier', 'yes')
        ->where('terrains.dommaine', 'etat')
        ->where('terrains.usage', 'ecole')
        ->count();


    $eglises =  DB::table('quartiers')
        ->join('secteurs', 'quartiers.id', '=', 'secteurs.quartier_id')
        ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
        ->where('quartiers.id', Auth::user()->id)
        ->where('terrains.verifiedByChefQuartier', 'yes')
        ->where('terrains.dommaine', 'etat')
        ->where('terrains.usage', 'eglise')
        ->count();


    $cimetieres =  DB::table('quartiers')
        ->join('secteurs', 'quartiers.id', '=', 'secteurs.quartier_id')
        ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
        ->where('quartiers.id', Auth::user()->id)
        ->where('terrains.verifiedByChefQuartier', 'yes')
        ->where('terrains.dommaine', 'etat')
        ->where('terrains.usage', 'cimetiere')
        ->count();


    $mosquee =  DB::table('quartiers')
        ->join('secteurs', 'quartiers.id', '=', 'secteurs.quartier_id')
        ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
        ->where('quartiers.id', Auth::user()->id)
        ->where('terrains.verifiedByChefQuartier', 'yes')
        ->where('terrains.dommaine', 'etat')
        ->where('terrains.usage', 'mosque')
        ->count();


  $loisirs =  DB::table('quartiers')
        ->join('secteurs', 'quartiers.id', '=', 'secteurs.quartier_id')
        ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
        ->where('quartiers.id', Auth::user()->id)
        ->where('terrains.verifiedByChefQuartier', 'yes')
        ->where('terrains.dommaine', 'etat')
        ->where('terrains.usage', 'loisir')
        ->count();

  $compagny =  DB::table('quartiers')
        ->join('secteurs', 'quartiers.id', '=', 'secteurs.quartier_id')
        ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
        ->where('quartiers.id', Auth::user()->id)
        ->where('terrains.verifiedByChefQuartier', 'yes')
        ->where('terrains.usage', 'company')
        ->count();

    return view('chefsecteur.home', compact('parcellesEtatiques', 'parcelles', 'hopitaux', 'embassades', 'ecoles', 'familles', 'cimetieres', 'eglises', 'mosquee', 'loisirs', 'compagny'));
})->name('home');


//Gestions des secteurs de commune
Route::resource('gestion_chefs_secteur_terrains', 'ChefSecteurGestionTerrain');
Route::get('/chefs_secteurs_terrain_confirm', 'ChefSecteurGestionTerrain@indexConfirm')->name('chefsSecteurTerrainsConfirmes');

//Gestions des chefs de quartiers par eux êmes

Route::resource('/gestion_own_chef_quartiers', 'ChefQuartierController');


//Gestions des secteurs des propriétaires
Route::resource('secteur_proprietaires', 'ChefSecteurGestionDesProprietairesTerriens');
Route::get('secteur_proprietaires_', 'ChefSecteurGestionDesProprietairesTerriens@createExiste')->name('add_existant_proprietaires');
Route::post('secteur_proprietaires_searche', 'ChefSecteurGestionDesProprietairesTerriens@searchProprietaire')->name('searchProprietaire');
Route::get('secteur_proprietaires_list_terrains', 'ChefSecteurGestionDesProprietairesTerriens@addTerrain')->name('add_terrain_proprietaire');
Route::post('secteur_proprietaires_preuve_terrain', 'ChefSecteurGestionDesProprietairesTerriens@addPreuve')->name('add_terrain_proprietaire_preuve');


Route::get('/add_preuve', function () {
    if (session('idProprietaire')){
        $proprieataire = Proprietaire::findOrFail(session('idProprietaire'));
        $nom = $proprieataire->firstName;
        $prenom = $proprieataire->lastName;
        $terrain_id = $proprieataire->terrain_id;

        return view('gestions_des_administrateurs.chef_secteurs.proprietaires.add_preuve', compact('nom', 'prenom', 'terrain_id'));
    }
})->name('add_preuve_proprietaire');
Route::resource('/preuve_de_propriete', 'PreuveController');
Route::post('/add_preuve', 'PreuveController@store')->name('store_preuve_proprietaire');

//Route::get('/chefs_secteurs_terrain_confirm', 'ChefSecteurGestionTerrain@indexConfirm')->name('chefsSecteurTerrainsConfirmes');

//Gestions des chefs de quartiers par eux êmes

Route::resource('/gestion_own_chef_secteurs', 'ChefSecteurController');

//Gestions des ventes de terrains
Route::resource('/gestion_des_ventes', 'ChefSecteurGestionDesVentes');
Route::get('/choix_dacheteur', 'ChefSecteurGestionDesVentes@choice')->name('select_type_acheteur');
Route::get('/select_land', 'ChefSecteurGestionDesVentes@selectAcheteur')->name('select_the_land');
Route::get('/gestion_des_terrains_en_attente', 'ChefSecteurGestionDesVentes@indexAttente')->name('terrains_attentes_pro');
Route::get('/gestion_des_terrains_confirmes', 'ChefSecteurGestionDesVentes@indexConfirme')->name('terrains_confirm_pro');

// Gestions des temoins d'une vente
Route::resource('/gestion_des_temoins', 'ChefSecteurGestionTemoin');
Route::get('/gestion_des_temoins2', 'ChefSecteurGestionTemoin@create2')->name('temoin_acheteur_2');
Route::get('/gestion_des_temoins3', 'ChefSecteurGestionTemoin@create3')->name('temoin_acheteur_3');

Route::get('/gestion_des_temoins_vend1', 'ChefSecteurGestionTemoin@create4')->name('temoin_vendeur_1');
Route::get('/gestion_des_temoins_vend2', 'ChefSecteurGestionTemoin@create5')->name('temoin_vendeur_2');
Route::get('/gestion_des_temoins_vend3', 'ChefSecteurGestionTemoin@create6')->name('temoin_vendeur_3');
