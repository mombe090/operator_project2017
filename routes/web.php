<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Commune;
use App\Http\Controllers\SearchAdministrateurByUserSimple;

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'middleware' => 'auth',

], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });


    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});
Route::get('about', function (){
    return view('about');
});

/*
 * Seach admins
 */
Route::get('recherche_administrateurs', 'SearchAdministrateurByUserSimple@index')->name('seachIndex');
Route::get('recherche_administrateurs', 'SearchAdministrateurByUserSimple@liste')->name('seachAdmins');

Route::post('recherche_terrains_en_vente', 'SearchAdministrateurByUserSimple@terrainCherche')->name('seachTerrains');
Route::get('recherche_terrains_en_vente', 'SearchAdministrateurByUserSimple@terrainCherche')->name('seachTerrains');
Route::post('send_mail_to_admin', 'FeedBack@store')->name('send_feed_back');



/*
 * Route for email confirmation by mombesoft
 *
 */
Route::get('/confirm/{id}/{confirmation_token}', 'Auth\RegisterController@confirm')->name('confirmation_compte');




/*
 * FOR ROOT
 *
 */

Route::group(['prefix' => 'root'], function () {

    Route::get('/login', 'RootAuth\LoginController@showLoginForm');
    Route::post('/login', 'RootAuth\LoginController@login');
    Route::post('/logout', 'RootAuth\LoginController@logout');

    Route::get('/register', 'RootAuth\RegisterController@showRegistrationForm');
    Route::post('/register', 'RootAuth\RegisterController@register');

    Route::post('/password/email', 'RootAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'RootAuth\ResetPasswordController@reset');
    Route::get('/password/reset', 'RootAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'RootAuth\ResetPasswordController@showResetForm');



});

Route::group(['middleware' => 'root'], function(){
    Route::get('hello', function () {
        dd(Auth::user());
    });
}) ;





/*
 * FOR SUPER ADMINS
 *
 */
Route::group(['prefix' => 'superadmin'], function () {
    Route::get('/login', 'SuperadminAuth\LoginController@showLoginForm');
    Route::post('/login', 'SuperadminAuth\LoginController@login');
    Route::post('/logout', 'SuperadminAuth\LoginController@logout');

    Route::get('/register', 'SuperadminAuth\RegisterController@showRegistrationForm')->name('superadminRegister');
    Route::post('/register', 'SuperadminAuth\RegisterController@register');

    Route::post('/password/email', 'SuperadminAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'SuperadminAuth\ResetPasswordController@reset');
    Route::get('/password/reset', 'SuperadminAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'SuperadminAuth\ResetPasswordController@showResetForm');

    Route::resource('gestions', 'SuperAdminController');

});
/*
 * FOR SUPER ADMINS
 *
 */

/*
 * FOR  ADMINS
 *
 */
Route::group(['prefix' => 'admin'], function () {
    Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::post('/logout', 'AdminAuth\LoginController@logout');

    Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm');
    Route::post('/register', 'AdminAuth\RegisterController@register');

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
/*
 * FOR  MAYOR
 *
 */

Route::group(['prefix' => 'maire'], function () {
    Route::get('/login', 'MaireAuth\LoginController@showLoginForm');
    Route::post('/login', 'MaireAuth\LoginController@login');
    Route::post('/logout', 'MaireAuth\LoginController@logout');

    Route::get('/register', 'MaireAuth\RegisterController@showRegistrationForm');
    Route::post('/register', 'MaireAuth\RegisterController@register');

    Route::post('/password/email', 'MaireAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'MaireAuth\ResetPasswordController@reset');
    Route::get('/password/reset', 'MaireAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'MaireAuth\ResetPasswordController@showResetForm');
});

/*
 * FOR  chef quartier
 *
 */
Route::group(['prefix' => 'chefquartier'], function () {
    Route::get('/chooseCommuneForChefQuartier', function () {
        $communes = Commune::all();
        return view('connexion.chooseCommuneForChefDeQuartier', compact('communes'));
    });

    Route::get('/login', 'ChefquartierAuth\LoginController@showLoginForm');
    Route::post('/login', 'ChefquartierAuth\LoginController@login');
    Route::post('/logout', 'ChefquartierAuth\LoginController@logout');

    Route::get('/register/add', 'ChefquartierAuth\RegisterController@afficherRegistrationForm');
    Route::post('/register', 'ChefquartierAuth\RegisterController@register');

    Route::post('/password/email', 'ChefquartierAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'ChefquartierAuth\ResetPasswordController@reset');
    Route::get('/password/reset', 'ChefquartierAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'ChefquartierAuth\ResetPasswordController@showResetForm');
});



Route::group(['prefix' => 'chefsecteur'], function () {

    //choix de la commune pour le chef secteur
    Route::get('/chooseCommuneForChefSecteurQuartier', 'ChefsecteurAuth\RegisterController@showRegistrationFormCommune')->name('chefSecteurCommune');

    //Choix du quartier pour le chef secteur
    Route::get('chooseQuartier', 'ChefsecteurAuth\RegisterController@showRegistrationFormQuartier')->name('chefSecteurQuartier');

    Route::get('/login', 'ChefsecteurAuth\LoginController@showLoginForm');
    Route::post('/login', 'ChefsecteurAuth\LoginController@login');
    Route::post('/logout', 'ChefsecteurAuth\LoginController@logout');

    Route::get('/register', 'ChefsecteurAuth\RegisterController@showRegistrationForm')->name('chefSecteurRegister');
    Route::post('/register', 'ChefsecteurAuth\RegisterController@register');

    Route::post('/password/email', 'ChefsecteurAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'ChefsecteurAuth\ResetPasswordController@reset');
    Route::get('/password/reset', 'ChefsecteurAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'ChefsecteurAuth\ResetPasswordController@showResetForm');
});





Route::group(['prefix' => 'proprietaire'], function () {
    Route::get('/login', 'ProprietaireAuth\LoginController@showLoginForm');
    Route::post('/login', 'ProprietaireAuth\LoginController@login');
    Route::post('/logout', 'ProprietaireAuth\LoginController@logout');

    Route::get('/register', 'ProprietaireAuth\RegisterController@showRegistrationForm');
    Route::post('/register', 'ProprietaireAuth\RegisterController@register');

    Route::post('/password/email', 'ProprietaireAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'ProprietaireAuth\ResetPasswordController@reset');
    Route::get('/password/reset', 'ProprietaireAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'ProprietaireAuth\ResetPasswordController@showResetForm');
});
//Confirmation de compte super admin
Route::get('/superadmin/confirm/{id}/{confirmation_token}', 'SuperadminAuth\RegisterController@confirm')->name('confirmation_compte');

//Confirmation de compte admin
Route::get('/admin/confirm/{id}/{confirmation_token}', 'AdminAuth\RegisterController@confirm')->name('confirmation_compte_admin');

//Confirmation de compte maire
Route::get('/maire/confirm/{id}/{confirmation_token}', 'MaireAuth\RegisterController@confirm')->name('confirmation_compte_maire');

//Confirmation de compte chef quartier
Route::get('/chefquartier/confirm/{id}/{confirmation_token}', 'ChefquartierAuth\RegisterController@confirm')->name('confirmation_compte_chefquartier');
//Confirmation de compte chef secteur
Route::get('/chefsecteur/confirm/{id}/{confirmation_token}', 'ChefsecteurAuth\RegisterController@confirm')->name('confirmation_compte_chefquartier');
//Confirmation de compte du proprietairesr
Route::get('/proprietaire/confirm/{id}/{confirmation_token}', 'ProprietaireAuth\RegisterController@showPasswordFormToProprietaire')->name('confirmation_compte_proprietaire');
Route::post('/proprietaire/confirm/{id}', 'ProprietaireAuth\RegisterController@confirm')->name('confirmation_compte_proprietaire_last');



Route::get('choiceTypeConnexion', 'ConnexionTypeController@index')->name('connexionType');
Route::get('choiceTypeConnexionTrue', 'ConnexionTypeController@choose')->name('connexionChoosed');

Route::get('choiceTypeInscription', 'InscriptionTypeController@index')->name('InscriptionType');
Route::get('choiceTypeInscriptionTrue', 'InscriptionTypeController@choose')->name('InscriptionChoosed');

/*
 * Root for gestions
 */



