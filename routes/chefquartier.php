<?php

use App\Secteur;

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('chefquartier')->user();

    $chefSecteurs = DB::table('quartiers')
        ->join('secteurs', 'quartiers.id', '=', 'secteurs.quartier_id')
        ->join('chefsecteurs', 'secteurs.id', '=', 'chefsecteurs.secteur_id')
        ->where('quartiers.id', Auth::user()->quartier->id)
        ->get();


    $parcelles = DB::table('quartiers')
        ->join('secteurs', 'quartiers.id', '=', 'secteurs.quartier_id')
        ->join('terrains', 'secteurs.id', '=', 'terrains.secteur_id')
        ->where('quartiers.id', Auth::user()->quartier->id)
        ->get();


    return view('chefquartier.home', compact( 'parcelles', 'chefSecteurs'));
})->name('home');


//Gestions des secteurs de commune
Route::resource('gestion_chefs_de_secteurs', 'ChefQuartierGestionDesChefSecteurs');
Route::get('/chefs_secteurs_confirm', 'ChefQuartierGestionDesChefSecteurs@indexConfirm')->name('chefsSecteurConfirmes');

//Gestions des secteurs du quartier
Route::resource('gestion_des_secteurs', 'ChefQuartierGestionDesSecteurs');
//Route::get('/chefs_secteurs_confirm', 'ChefQuartierGestionDesChefSecteurs@indexConfirm')->name('chefsSecteurConfirmes');

//Gestions des ventes de terrain.
Route::resource('chf_q_gestion_des_ventes', 'ChefQuartierGesttionDesVentes');
//Route::get('/chefs_secteurs_confirm', 'ChefQuartierGestionDesChefSecteurs@indexConfirm')->name('chefsSecteurConfirmes');

//Gestions des chefs de quartiers par eux êmes

Route::resource('/gestion_own_chef_quartiers', 'ChefQuartierController');

//Gestions des terrains dans le quartier par le chef secteur
Route::resource('/gestion_terrains_chef_quartiers', 'ChefQuartierGestionsDesTerrains');
Route::get('/gestion_terrains_chef_quartiers_show/{n}', 'ChefQuartierGestionsDesTerrains@showTwo')->where('n', '[0-9]+')->name('showTwo');
Route::get('/chefs_quartiers_terrains_confirm', 'ChefQuartierGestionsDesTerrains@indexConfirm')->name('chefsQuartierConfirmesTerrains');