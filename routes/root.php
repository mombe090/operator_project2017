<?php

Route::get('/home', 'RootController@home')->name('home')->middleware('web');
Route::get('/home/supadmins', 'RootController@confirmationSuperAmins');
Route::get('/home/supadmins/confirmes', 'RootController@SuperAminsConfirmes');


//Route relatives à la gestions des supers utilisateurs
Route::get('/show/profile/superadmins/{n}', 'RootController@showSuperAdminsInformations')->where('n', '[0-9]+');
Route::get('/edit/profile/superadmins/{n}', 'RootController@editSuperAdminsInformations')->where('n', '[0-9]+');
Route::put('/update/profile/superadmins/{n}', 'RootController@updateSuperAdminsInformations')->where('n', '[0-9]+');
Route::get('/delete/profile/superadmins/{n}', 'RootController@deleteSuperAdminsInformations')->where('n', '[0-9]+');
Route::get('confirm/superadmins/{n}', 'RootController@confirmSuperAdminsInformations')->where('n', '[0-9]+');


