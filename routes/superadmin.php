<?php

Route::get('/home', 'SuperAdminController@home')->name('home');

// Gestions des administrateurs de la region
Route::resource('/gestion_admins', 'SuperAdminGestionDesAdmins');
Route::get('/adminsconfirm', 'SuperAdminGestionDesAdmins@indexConfirm')->name('adminsConfirmes');
Route::get('/edit/{n}/confirm', 'SuperAdminGestionDesAdmins@edit')->name('editAdminConfirm');


//Gestions des superadmins par eux êmes

Route::resource('/gestion_own_superadmins', 'SuperAdminController');

//Gestions des titres foncier des terrains de chaque regions.
Route::resource('/bureau_conservation_fonciere', 'SuperAdminGestionTitreFoncier');
Route::get('/dg_bcf_terrains_confirm', 'SuperAdminGestionTitreFoncier@indexConfirm')->name('superAdminConfirmTerrain');