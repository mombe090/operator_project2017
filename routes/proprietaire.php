<?php

use App\Terrain;

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('proprietaire')->user();

    $parcelles = Terrain::where('proprietaire_id', Auth::user()->id)->paginate(5);


    return view('proprietaire.home', compact('parcelles'));
})->name('home');

Route::resource('/gestion_personnel_des_parcelle', 'ProprietaireController');

//Gestions des chefs de quartiers par eux êmes

Route::resource('/gestion_own_proprietaire_terrien', 'ProprietaireController');

