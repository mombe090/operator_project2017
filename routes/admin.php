<?php

use App\Region;

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    if (Auth::user()->prefecture->code!='capitale'){

        $quartiers = DB::table('communes')
            ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
            ->where('communes.id', Auth::user()->prefecture->id)
            ->count();

        $chefQuartiers = DB::table('communes')
            ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
            ->join('chefquartiers', 'chefquartiers.quartier_id', '=', 'quartiers.id')
            ->where('communes.id', Auth::user()->prefecture->id)
            ->get();


        $parcelles = DB::table('communes')
            ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
            ->where('communes.id', Auth::user()->prefecture->id)
            ->get();


        return view('admin.home',compact('quartiers', 'parcelles', 'chefQuartiers'));
    }


    else{
        $quartiers = DB::table('prefectures')
            ->join('communes', 'communes.prefecture_id', '=', 'prefectures.id')
            ->join('quartiers', 'quartiers.commune_id', '=', 'communes.id')
            ->where('prefectures.id', Auth::user()->prefecture->id)
            ->count();

        $chefQuartiers = DB::table('prefectures')
            ->join('communes', 'communes.prefecture_id', '=', 'prefectures.id')
            ->join('quartiers', 'quartiers.commune_id', '=', 'communes.id')
            ->join('chefquartiers', 'chefquartiers.quartier_id', '=', 'quartiers.id')
            ->where('prefectures.id', Auth::user()->prefecture->id)
            ->get();



        $mairesVerifies = DB::table('prefectures')
            ->join('communes', 'communes.prefecture_id', '=', 'prefectures.id')
            ->join('maires', 'maires.commune_id', '=', 'communes.id')
            ->where('prefectures.id', Auth::user()->prefecture->id)
            ->get();


        $parcelles = DB::table('prefectures')
            ->join('communes', 'prefectures.id', '=', 'communes.prefecture_id')
            ->join('quartiers', 'quartiers.commune_id', '=', 'communes.id')
            ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
            ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
            ->where('prefectures.id', Auth::user()->prefecture->id)
            ->get();


        $communes = DB::table('prefectures')
            ->join('communes', 'communes.prefecture_id', '=', 'prefectures.id')
            ->where('prefectures.id', Auth::user()->prefecture->id)
            ->count();




        return view('admin.home',compact('communes','mairesVerifies', 'quartiers', 'parcelles', 'chefQuartiers'));
    }




})->name('home');

//Gestions des admin de commune
Route::resource('gestion_maires', 'AdminGestionDesMaires');
Route::get('/mairesconfirm', 'AdminGestionDesMaires@indexConfirm')->name('mairesConfirmes');

//Gestions des admin par eux êmes

Route::resource('/gestion_own_admins', 'AdminController');

//Gestions des admins des terrains confirmés par le maires.
Route::resource('/domaines_cadastre', 'AdminGestionsTerrains');
Route::get('/dg_docad_terrains_confirm', 'AdminGestionsTerrains@indexConfirm')->name('adminConfirmTerrain');