<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('maire')->user();

    $quartiers = DB::table('communes')
        ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
        ->where('communes.id', Auth::user()->commune->id)
        ->count();

    $chefQuartiers = DB::table('communes')
        ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
        ->join('chefquartiers', 'chefquartiers.quartier_id', '=', 'quartiers.id')
        ->where('communes.id', Auth::user()->commune->id)
        ->get();


    $parcelles = DB::table('communes')
        ->join('quartiers', 'communes.id', '=', 'quartiers.commune_id')
        ->join('secteurs', 'secteurs.quartier_id', '=', 'quartiers.id')
        ->join('terrains', 'terrains.secteur_id', '=', 'secteurs.id')
        ->where('communes.id', Auth::user()->commune->id)
        ->get();

    return view('maire.home',compact('quartiers', 'chefQuartiers', 'chefQuartiersEnAttente', 'parcelles', 'parcellesNonVerifiees', 'parcellesEtatiques', 'embassades', 'familles', 'hopitaux', 'ecoles', 'ecolesPrive', 'mosques', 'eglises'));
})->name('home');

//Gestions des maires de commune
Route::resource('gestion_chefs_de_quartiers', 'MairesGestionDesChefsQuartiers');
Route::get('/chefs_quartiers_confirm', 'MairesGestionDesChefsQuartiers@indexConfirm')->name('chefsQuartierConfirmes');

//Gestions des quartiers de la communes
Route::resource('mairie_quartier', 'MaireGestionDesQuartiers');


Route::resource('mairie_terrains', 'MaireGestionDesTerrains');
Route::get('/maire_terrains_confirm', 'MaireGestionDesTerrains@indexConfirm')->name('maireConfirmesTerrains');
Route::get('/delivrer_plan_de_masse', 'MaireGestionDesTerrains@planDeMasse')->name('mairePlanDeMasse');
Route::get('/modifier_plan_de_masse', 'MaireGestionDesTerrains@planDeMasseUpdate')->name('mairePlanDeMasseModify');

//Gestions des maires par eux êmes

Route::resource('/gestion_own_maires', 'MaireController');


