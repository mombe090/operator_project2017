<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerrainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terrains', function (Blueprint $table) {
            $table->increments('id');
            $table->string('forme');
            $table->float('longer');
            $table->float('larger');
            $table->enum('dommaine', ['prive', 'etat', 'ambassade', 'compagny', 'none'])->default('none');
            $table->enum('usage', ['habitation', 'ecole', 'mosque', 'eglise', 'hopital', 'cimetiere', 'loisir', 'none'])->default('none');
            $table->enum('build', ['no', 'yes'])->default('no');
            $table->string('planDeMasse')->nullable();
            $table->string('attestationAchat')->nullable();
            $table->string('titreFoncier')->nullable();
            $table->string('codeSecteur')->nullable()->unique();
            $table->string('codeQuartier')->nullable()->unique();
            $table->string('codeCommune')->nullable()->unique();
            $table->string('codeVille')->nullable()->unique();
            $table->string('codeRegion')->nullable()->unique();
            $table->string('codeNational')->nullable()->unique();
            $table->enum('onSale', ['0', '1'])->default('0');
            $table->enum('onTransaction', ['0', '1'])->default('0');
            $table->enum('onSaleSend', ['0', '1'])->default('0');
            $table->enum('ValidSale', ['0', '1'])->default('0');
            $table->enum('verifiedByChefQuartier', ['yes', 'no'])->default('no');
            $table->enum('verifiedByMaire', ['yes', 'no'])->default('no');
            $table->enum('verifiedByAdmin', ['yes', 'no'])->default('no');




            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terrains');
    }
}
