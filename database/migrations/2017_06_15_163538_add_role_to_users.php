<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoleToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roots', function (Blueprint $table) {
            $table->enum('role', ['root', 'superadmin', 'admin', 'maire', 'chefQuartier', 'chefSecteur'])->default('root');
        });

        Schema::table('superadmins', function (Blueprint $table) {
            $table->enum('role', ['root', 'superadmin', 'admin', 'maire', 'chefQuartier', 'chefSecteur'])->default('superadmin');
        });

        Schema::table('admins', function (Blueprint $table) {
            $table->enum('role', ['root', 'superadmin', 'admin', 'maire', 'chefQuartier', 'chefSecteur'])->default('admin');
        });

        Schema::table('maires', function (Blueprint $table) {
            $table->enum('role', ['root', 'superadmin', 'admin', 'maire', 'chefQuartier', 'chefSecteur'])->default('maire');
        });

        Schema::table('chefquartiers', function (Blueprint $table) {
            $table->enum('role', ['root', 'superadmin', 'admin', 'maire', 'chefQuartier', 'chefSecteur'])->default('chefQuartier');
        });

        Schema::table('chefsecteurs', function (Blueprint $table) {
            $table->enum('role', ['root', 'superadmin', 'admin', 'maire', 'chefQuartier', 'chefSecteur'])->default('chefSecteur');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roots', function (Blueprint $table) {
            $table->dropColumn('role');
        });

        Schema::table('superadmins', function (Blueprint $table) {
            $table->dropColumn('role');
        });

        Schema::table('admins', function (Blueprint $table) {
            $table->dropColumn('role');
        });

        Schema::table('maires', function (Blueprint $table) {
            $table->dropColumn('role');
        });

        Schema::table('chefquartiers', function (Blueprint $table) {
            $table->dropColumn('role');
        });

        Schema::table('chefsecteurs', function (Blueprint $table) {
            $table->dropColumn('role');
        });


    }
}
