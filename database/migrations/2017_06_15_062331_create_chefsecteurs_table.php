<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChefsecteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chefsecteurs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('nationalIdentity')->unique();
            $table->string('telephone')->unique();
            $table->string('email')->unique();
            $table->string('dateOfBirth');
            $table->string('hireDate');
            $table->string('picture')->default('storage/images/avatars/avatar5.png');
            $table->enum('working', ['off', 'on'])->default('off');
            $table->string('verify')->nullable();
            $table->longText('bio')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
                        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chefsecteurs');
    }
}
