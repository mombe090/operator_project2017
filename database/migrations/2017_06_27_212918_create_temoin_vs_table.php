<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemoinVsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temoin_vs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nationalIdentity');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('email');
            $table->string('telephone');
            $table->string('dateOfBirth');
            $table->string('picture')->nullable();

            $table->integer('vendeur_id')->unsigned()->index();
            $table->foreign('vendeur_id')->references('id')->on('proprietaires')->onDelete('restrict');

            $table->integer('terrain_id')->unsigned()->index();
            $table->foreign('terrain_id')->references('id')->on('terrains')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temoin_vs');
    }
}
