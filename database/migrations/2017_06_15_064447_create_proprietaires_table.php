<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProprietairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proprietaires', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nationalIdentity')->unique();
            $table->string('firstName');
            $table->string('lastName');
            $table->string('email');
            $table->string('picture')->nullable();
            $table->string('telephone')->unique();
            $table->string('dateOfBirth');

            $table->string('verify')->nullable();
            $table->enum('working', ['off', 'on'])->default('off');
            $table->enum('role', ['proprietaire', 'none'])->default('proprietaire');
            $table->string('password')->nullable();
            $table->enum('leastOneLand', ['0', '1'])->default('0');
            $table->longText('bio')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proprietaires');
    }
}
