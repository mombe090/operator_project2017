<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fisrtName');
            $table->string('lastName');
            $table->string('email')->unique();
            $table->string('nationalIdentity');
            $table->string('verify')->nullable();
            $table->enum('role', ['superAdmin', 'maire', 'quarterChief', 'secteurChief', 'landOwner', 'none'])->default('none');
            $table->string('picture');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
