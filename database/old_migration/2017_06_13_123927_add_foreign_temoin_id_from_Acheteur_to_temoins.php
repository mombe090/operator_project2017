<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignTemoinIdFromAcheteurToTemoins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temoins', function (Blueprint $table) {
            $table->integer('acheteur_id')->unsigned()->index();
            $table->foreign('acheteur_id')->references('id')->on('acheteurs')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temoins', function (Blueprint $table) {
           $table->dropForeign(['acheteur_id']);
           $table->dropColumn('acheteur_id');
        });
    }
}
