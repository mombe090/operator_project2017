<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignQuartierFromSecteur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('secteurs', function (Blueprint $table) {
           $table->integer('quartier_id')->unsigned()->index();
           $table->foreign('quartier_id')->references('id')->on('quartiers')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('secteurs', function (Blueprint $table) {
           $table->dropForeign(['quartier_id']);
           $table->foreign('quartier_id');
        });
    }
}
