<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChefQuartiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chef_quartiers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nationalIdentity')->unique();
            $table->string('firstName');
            $table->string('lastName');
            $table->string('email');
            $table->string('dateOfBirth');
            $table->string('hireDate');
            $table->string('picture')->default('storage/images/avatars/avatar04.png');
            $table->enum('working', ['on', 'off'])->default('off');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chef_quartiers');
    }
}
