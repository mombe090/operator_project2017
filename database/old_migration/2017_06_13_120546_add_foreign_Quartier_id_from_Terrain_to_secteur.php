<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignQuartierIdFromTerrainToSecteur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('terrains', function (Blueprint $table) {
            $table->integer('secteur_id')->unsigned()->index();
            $table->foreign('secteur_id')->references('id')->on('secteurs')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('terrains', function (Blueprint $table) {
           $table->dropForeign(['secteur_id']);
           $table->dropColumn('secteur_id');
        });
    }
}

/*
 *
 */
