<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignsIdToVentes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ventes', function (Blueprint $table) {
            /*$table->integer('terrain_id')->unsigned()->index();
            $table->integer('chef_quartier_id')->unsigned()->index();
            $table->integer('propritaire_id')->unsigned()->index();
            $table->integer('acheteur_id')->unsigned()->index();
            $table->integer('temoin_id_one')->unsigned()->index();
            $table->integer('temoin_id_two')->unsigned()->index();
            $table->integer('temoin_id_three')->unsigned()->index();
            $table->integer('temoin_id_four')->unsigned()->index();


            $table->foreign('terrain_id')->references('id')->on('terrains')->onDelete('restrict');
            $table->foreign('chef_quartier_id')->references('id')->on('quartiers')->onDelete('restrict');
            $table->foreign('propritaire_id')->references('id')->on('proprietaires')->onDelete('restrict');
            $table->foreign('acheteur_id')->references('id')->on('acheteurs')->onDelete('restrict');
            $table->foreign('temoin_id_one')->references('id')->on('proprietaires')->onDelete('restrict');
            $table->foreign('temoin_id_two')->references('id')->on('proprietaires')->onDelete('restrict');
            $table->foreign('temoin_id_three')->references('id')->on('acheteurs')->onDelete('restrict');
            $table->foreign('temoin_id_four')->references('id')->on('acheteurs')->onDelete('restrict');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ventes', function (Blueprint $table) {
            $table->dropForeign([
               /* 'terrain_id',
                'chef_quartier_id',
                'propritaire_id',
                'acheteur_id',
                'temoin_id_one',
                'temoin_id_two',
                'temoin_id_three',
                'temoin_id_four'*/
            ]);

            $table->dropColumn(
                [
                   /* 'terrain_id',
                    'chef_quartier_id',
                    'propritaire_id',
                    'acheteur_id',
                    'temoin_id_one',
                    'temoin_id_two',
                    'temoin_id_three',
                    'temoin_id_four'*/
                ]
            );
        });

    }
}
