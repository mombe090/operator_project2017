<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChefSecteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chef_secteurs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nationalIdentity')->unique();
            $table->string('firstName');
            $table->string('lastName');
            $table->string('email');
            $table->string('dateOfBirth');
            $table->string('hireDate');
            $table->string('picture');
            $table->string('working');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chef_secteurs');
    }
}
