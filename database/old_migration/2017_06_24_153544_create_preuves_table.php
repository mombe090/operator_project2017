<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreuvesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preuves', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attestationAchat');
            $table->string('planDeMasse');
            $table->string('titreFoncier');
            $table->enum('verifiedByChefQuartier', ['no', 'yes'])->default('no');

            $table->integer('terrain_id')->unsigned()->index();
            $table->foreign('terrain_id')->references('id')->on('terrains')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preuves');
        Schema::table('preuves', function (Blueprint $table){
            $table->dropForeign(['terrain_id']);
            $table->dropForeign('terrain_id');
        });
    }
}
