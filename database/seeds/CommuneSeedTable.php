<?php

use App\Commune;
use App\Prefecture;
use Illuminate\Database\Seeder;

class CommuneSeedTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       if (Commune::all()->count()==0){
           $prefectures = Prefecture::all();
            $i=1;
             foreach ($prefectures as $prefecture) :
                if($prefecture->code=='capitale'){

                 }else{
                    Commune::create([
                        'code' => $prefecture->nom.'_commune_'.$i,
                        'name' => $prefecture->nom,
                        'prefecture_id' => $prefecture->id
                    ]);
                }
                 $i++;
             endforeach;

           Commune::create([
               'code' => 'capitale_commu_1',
               'name' => 'Kaloum',
               'prefecture_id' => 34
           ]);

           Commune::create([
               'code' => 'capitale_commu_2',
               'name' => 'Matam',
               'prefecture_id' => 34
           ]);

           Commune::create([
               'code' => 'capitale_commu_3',
               'name' => 'Dixin',
               'prefecture_id' => 34
           ]);

           Commune::create([
               'code' => 'capitale_commu_4',
               'name' => 'Ratoma',
               'prefecture_id' =>34
           ]);

           Commune::create([
               'code' => 'capitale_commu_5',
               'name' => 'Matoto',
               'prefecture_id' => 34
           ]);
       }
    }
}

