<?php

use App\Month;
use Illuminate\Database\Seeder;

class MounthTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Month::all()->count()==0){
            for($i=1; $i <= 12; $i++){
                if ($i==1){
                    Month::create([
                        'name' => 'Janvier',
                    ]);
                }else  if ($i==2){
                    Month::create([
                        'name' => 'Fevrier',
                    ]);
                }else  if ($i==3){
                    Month::create([
                        'name' => 'Mars',
                    ]);
                }else  if ($i==4){
                    Month::create([
                        'name' => 'Avril',
                    ]);
                }else  if ($i==5){
                    Month::create([
                        'name' => 'Mai',
                    ]);
                }else  if ($i==6){
                    Month::create([
                        'name' => 'Juin',
                    ]);
                }else  if ($i==7){
                    Month::create([
                        'name' => 'Juillet',
                    ]);
                }else  if ($i==8){
                    Month::create([
                        'name' => 'Aout',
                    ]);
                }else  if ($i==9){
                    Month::create([
                        'name' => 'Septembre',
                    ]);
                }else  if ($i==10){
                    Month::create([
                        'name' => 'Octobre',
                    ]);
                }else  if ($i==11){
                    Month::create([
                        'name' => 'Novembre',
                    ]);
                }else {
                    Month::create([
                        'name' => 'Decembre',
                    ]);
                }
            }
        }
    }
}
