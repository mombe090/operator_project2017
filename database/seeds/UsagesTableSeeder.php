<?php

use Illuminate\Database\Seeder;

class UsagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('usages')->count()==0){
            DB::table('usages')->insert([
                'name' => 'habitation'
            ]);
            DB::table('usages')->insert([
                'name' => 'ecole'
            ]);
            DB::table('usages')->insert([
                'name' => 'mosque'
            ]);
            DB::table('usages')->insert([
                'name' => 'eglise'
            ]);
            DB::table('usages')->insert([
                'name' => 'hopital'
            ]);
            DB::table('usages')->insert([
                'name' => 'cimetiere'
            ]);
            DB::table('usages')->insert([
                'name' => 'loisir'
            ]);
            DB::table('usages')->insert([
                'name' => 'autre'
            ]);
        }
    }
}
