<?php

use App\Proprietaire;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ProprietaireTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();
        for ($i=0; $i < 45; $i++){
            Proprietaire::create([
                'firstName' => $fake->firstName,
                'lastName' => $fake->lastName,
                'nationalIdentity' =>str_shuffle(rand(0000000,9999999)) .'/'.rand(10,17),
                'email' => $fake->safeEmail.''.str_random(3),
                'telephone' => '6'. rand(0,9).rand(0,9).str_shuffle(str_limit(str_shuffle('123456789123456789'), 6, '')),
                'dateOfBirth' => rand(1,31).'/'.rand(1,12).'/'.rand(1970, 2000),
                'picture' => '/storage/images/users/default1.jpg',
                'working' => rand(0,1) ? 'on' : 'off',
                'role' => 'proprietaire',
                'verify' => null,
                'password' => bcrypt('123456')
            ]);
        }
    }
}
