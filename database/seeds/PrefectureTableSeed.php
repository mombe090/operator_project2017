<?php

use App\Prefecture;
use Illuminate\Database\Seeder;

class PrefectureTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       if (Prefecture::all()->count()==0){
             /*
            * Liste des prefectures de boke
            */
           Prefecture::create([
               'code' => 'Basse_cote_Boke_01',
               'nom' => 'Boke',
               'region_id' => 3
           ]);
           Prefecture::create([
               'code' => 'Basse_cote_Boke_02',
               'nom' => 'Boffa',
               'region_id' => 3
           ]);

           Prefecture::create([
               'code' => 'Basse_cote_Boke_04',
               'nom' => 'Gaoual',
               'region_id' => 3
           ]);
           Prefecture::create([
               'code' => 'Basse_cote_Boke_05',
               'nom' => 'Koundara',
               'region_id' => 3
           ]);
           Prefecture::create([
               'code' => 'Basse_cote_Boke_06',
               'nom' => 'Fria',
               'region_id' => 3
           ]);
           /*
           *Fin  Liste des prefectures de boke
           */
           /*
           * Liste des prefectures de kindia
           */
           Prefecture::create([
               'code' => 'Basse_cote_Kindia_01',
               'nom' => 'Kindia',
               'region_id' => 2
           ]);
           Prefecture::create([
               'code' => 'Basse_cote_Kindia_03',
               'nom' => 'Forecariah',
               'region_id' => 2
           ]);

           Prefecture::create([
               'code' => 'Basse_cote_Kindia_04',
               'nom' => 'Telemele',
               'region_id' => 2
           ]);


           /*
           * Fin Liste des prefectures de kindia
           */
           /*
           *  Liste des prefectures de Mamou
           */
           Prefecture::create([
               'code' => 'Moyenne_Guinee_Mamou_01',
               'nom' => 'Mamou',
               'region_id' => 5
           ]);

           Prefecture::create([
               'code' => 'Moyenne_Guinee_Mamou_02',
               'nom' => 'Dalaba',
               'region_id' => 5
           ]);

           Prefecture::create([
               'code' => 'Moyenne_Guinee_Mamou_03',
               'nom' => 'Pita',
               'region_id' => 5
           ]);
           /*
           *  FIN Liste des prefectures de Mamou
           */

           /*
           *  Liste des prefectures de Labe
           */
           Prefecture::create([
               'code' => 'Moyenne_Guinee_Labe_01',
               'nom' => 'Labe',
               'region_id' => 4
           ]);

           Prefecture::create([
               'code' => 'Moyenne_Guinee_Labe_02',
               'nom' => 'Lelouma',
               'region_id' => 4
           ]);

           Prefecture::create([
               'code' => 'Moyenne_Guinee_Labe_03',
               'nom' => 'Mali',
               'region_id' => 4
           ]);

           Prefecture::create([
               'code' => 'Moyenne_Guinee_Labe_04',
               'nom' => 'Koubia',
               'region_id' => 4
           ]);

           Prefecture::create([
               'code' => 'Moyenne_Guinee_Labe_05',
               'nom' => 'Tougue',
               'region_id' => 4
           ]);

           /*
          * Fin  Liste des prefectures de Labe
          */
           /*
          * Liste des prefectures de Kankan
          */

           Prefecture::create([
               'code' => 'Haute_Guinee_Kankan_01',
               'nom' => 'Kankan',
               'region_id' => 6
           ]);
           Prefecture::create([
               'code' => 'Haute_Guinee_Kankan_02',
               'nom' => 'Kerouane',
               'region_id' => 6
           ]);
           Prefecture::create([
               'code' => 'Haute_Guinee_Kankan_03',
               'nom' => 'Mandiana',
               'region_id' => 6
           ]);
           Prefecture::create([
               'code' => 'Haute_Guinee_Kankan_04',
               'nom' => 'Siguiri',
               'region_id' => 6
           ]);
           Prefecture::create([
               'code' => 'Haute_Guinee_Kankan_05',
               'nom' => 'Kouroussa',
               'region_id' => 6
           ]);

           /*
          *  FIN Liste des prefectures de Kankan
          */

           /*
         * Liste des prefectures de Faranah
         */
           Prefecture::create([
               'code' => 'Haute_Guinee_Faranah_01',
               'nom' => 'Faranah',
               'region_id' => 7
           ]);
           Prefecture::create([
               'code' => 'Haute_Guinee_Faranah_02',
               'nom' => 'Dabola',
               'region_id' => 7
           ]);
           Prefecture::create([
               'code' => 'Haute_Guinee_Faranah_03',
               'nom' => 'Dinguiraye',
               'region_id' => 7
           ]);
           Prefecture::create([
               'code' => 'Haute_Guinee_Faranah_04',
               'nom' => 'Kissidougou',
               'region_id' => 7
           ]);
           /*
    * Fin Liste des prefectures de Faranah
    */

           /*
   Liste des prefectures de N'Zerekore
   */
           Prefecture::create([
               'code' => 'Guinee_Forestiere_01',
               'nom' => 'N\'Zerekore',
               'region_id' => 8
           ]);
           Prefecture::create([
               'code' => 'Guinee_Forestiere_02',
               'nom' => 'Lola',
               'region_id' => 8
           ]);
           Prefecture::create([
               'code' => 'Guinee_Forestiere_03',
               'nom' => 'Macenta',
               'region_id' => 8
           ]);
           Prefecture::create([
               'code' => 'Guinee_Forestiere_04',
               'nom' => 'Yomou',
               'region_id' => 8
           ]);
           Prefecture::create([
               'code' => 'Guinee_Forestiere_05',
               'nom' => 'Beyla',
               'region_id' => 8
           ]);

           Prefecture::create([
               'code' => 'Guinee_Forestiere_06',
               'nom' => 'Guekedou',
               'region_id' => 8
           ]);

           //Prefecture de conakry
           Prefecture::create([
               'code' => 'Basse_cote_Conakry_01',
               'nom' => 'Coyah',
               'region_id' => 1
           ]);
           Prefecture::create([
               'code' => 'Basse_cote_Conakry_02',
               'nom' => 'Dubreka',
               'region_id' => 1
           ]);

           Prefecture::create([
               'code' => 'capitale',
               'nom' => 'Conakry',
               'region_id' => 1
           ]);

       }
    }




}
