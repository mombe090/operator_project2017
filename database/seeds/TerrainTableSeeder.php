<?php

use App\Proprietaire;
use App\Secteur;
use App\Terrain;
use Illuminate\Database\Seeder;

class TerrainTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $secteurs = Secteur::all();
        $dommaines = [
          'prive', 'etat', 'ambassade', 'compagny', 'none'
        ];
        $usages = [
            'habitation', 'ecole', 'mosque', 'eglise', 'hopital', 'cimetiere', 'loisir', 'none'
        ];
  ;
        foreach ($secteurs as $secteur){

                Terrain::create([
                    'forme' => rand(0, 1) ? 'Triangulaire' : 'Rectangulaire',
                    'longer' => rand(10, 50),
                    'larger' => rand(10, 50),
                    'dommaine' => $dommaines[rand(0,4)],
                    'usage' => $usages[rand(0,7)],
                    'attestationAchat' => 'storage/terrains/attestations/default.png',
                    'planDeMasse' => 'storage/terrains/planDeMasses/default.png',
                    'titreFoncier' => 'storage/terrains/titreFonciers/default.png',
                    'build' => rand(0, 1) ? 'no' : 'yes',
                    'codeSecteur' => $secteur->name.'_0000'. str_shuffle(str_random(4)).'',
                    'codeQuartier' => $secteur->quartier->name.'_'.$secteur->name.'___0000'. str_shuffle(str_random(4)).'',
                    'codeCommune' => $secteur->quartier->name.'__'.$secteur->name.'_'. str_shuffle(str_random(4)).'',
                    'codeVille' => $secteur->quartier->commune->name.'_'.$secteur->quartier->name.'_'.$secteur->name.'__'. str_shuffle(str_random(4)).'',
                    'codeRegion' => $secteur->quartier->commune->prefecture->nom .'_'.$secteur->quartier->commune->name.'_'.$secteur->quartier->name.'__'. str_shuffle(str_random(4)).'',
                    'codeNational' => $secteur->quartier->commune->prefecture->region->name .'_'.$secteur->quartier->commune->prefecture->nom.'_'. str_shuffle(str_random(4)).'',
                    'onSale' => rand(0, 1) ? '0' : '1',
                    'verifiedByChefQuartier' => rand(0, 1) ? 'no' : 'yes',
                    'verifiedByMaire' => rand(0, 1) ? 'no' : 'yes',
                    'verifiedByAdmin' => rand(0, 1) ? 'no' : 'yes',
                    'secteur_id' => $secteur->id,
                    'proprietaire_id' => rand(1, 45)
                ]);
        }

    }
}
