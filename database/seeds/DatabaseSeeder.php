<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('compteseeds')->count()==0){
            $this->call(RootLoginInfo::class);
            $this->command->info("Le root. L'administrateur national à ete inscrit dans la base de donnees avec success. Son mot de passe est : root90");
            $this->call(RegionTableSeed::class);
            $this->command->info("Les 7 regions administratives plus la zone speciale de conakry ont ete ajouter avec succes.");
            $this->call(PrefectureTableSeed::class);
            $this->command->info("Les 33 prefectures plus la ville de conakry ont ete ajouter avec succes.");
            $this->call(CommuneSeedTable::class);
            $this->command->info("Les 38 communes  ont ete ajouter avec succes. (33 plus les 5 de conakry)");
            $this->call(MounthTableSeed::class);
            $this->call(DommainesTableSeeder::class);
            $this->call(UsagesTableSeeder::class);
            DB::table('compteseeds')->insert(
                ['number' => 1]
            );

        }
        else if(DB::table('compteseeds')->count()==1){
            $this->command->info("Svp veillez patienter un peu la generation de quartier dure un peu");
            $this->call(QuartierTableSeeder::class);
            $this->command->info("On a genere aleatoirement des quartiers pour chaque communes car on est en developpement.");
            $this->command->info("Avec l'interface web, un maire pourra ajouter ou supprimer un quartier");
            $this->command->info("Svp veillez patienter un peu la generation de secteur dure un peu");
            $this->call(SecteurTableSeeder::class);

            DB::table('compteseeds')->insert(
                ['number' => 2]
            );
        }
        else if(DB::table('compteseeds')->count()==2){
            $this->call(SuperAdminsTableSeeder::class);
            $this->command->info("7 Super Admin (Administrateur Regional) ont ete ajoute à la base de donnees avec succes");
            $this->command->info("On a genere aleatoirement des secteurs pour chaque quartiers car on est en developpement.");
            $this->command->info("Avec l'interface web, un chef de quartier pourra ajouter ou supprimer un secteur");
            $this->call(AdminsTableSeeder::class);
            $this->command->info("33 Admins (Administrateur Prefectoral) ont ete ajoute à la base de donnees avec succes");
            DB::table('compteseeds')->insert(
                ['number' => 3]
            );
        }
        else if(DB::table('compteseeds')->count()==3){
            $this->call(MairesTableSeeder::class);
            $this->command->info("33 maires (Administrateur Communal) ont ete ajoute à la base de donnees avec succes");
            DB::table('compteseeds')->insert(
                ['number' => 4]
            );
        }
        else if(DB::table('compteseeds')->count()==4){
            $this->command->info('Veillez patienter la generations de chefs quartiers prend un instant');
            $this->call(ChefQuartiersTableSeeder::class);
            $this->command->info("Les Chef Quartiers  ont ete ajoute à la base de donnees avec succes");
            DB::table('compteseeds')->insert(
                ['number' => 5]
            );
        }
        else if(DB::table('compteseeds')->count()==5){
            $this->command->info('Veillez patienter la generations de chefs secteurs prend un peu plus de temps');
            $this->call(ChefSecteursTableSeeder::class);
            $this->command->info("Les Chef Secteurs  ont ete ajoute à la base de donnees avec succes");
            DB::table('compteseeds')->insert(
                ['number' => 6]
            );
        }else if(DB::table('compteseeds')->count()==6){
            $this->call(ProprietaireTableSeeder::class);
            $this->command->info("45 proprietaires ont ete generes avec succes");
            $this->command->info("Veillez patienter la generation de terrains prend un peu de temps");
            $this->call(TerrainTableSeeder::class);
            $this->command->info('Terrains generes avec succes');
            DB::table('compteseeds')->insert(
                ['number' => 6]
            );

        }else{
            $this->command->info("Toutes les informations necessaires ont ete generees");
        }

    }
}
