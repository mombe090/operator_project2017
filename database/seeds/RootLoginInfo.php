<?php

use App\Root;
use Illuminate\Database\Seeder;

class RootLoginInfo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Root::all()->count()==0){
            Root::create([
                'name' => 'DIALLO Mamadou Yaya',
                'email' => 'root@gmail.com',
                'password' => bcrypt('root'),
                'role' => 'root'
            ]);
        }
    }
}
