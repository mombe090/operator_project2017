<?php

use App\Region;
use Illuminate\Database\Seeder;

class RegionTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $region = Region::all();
        if ($region->count()==0){
            Region::create([
                'code' => 'Base_Guinee_01',
                'name' => 'Conakry',
            ]);
            Region::create([
                'code' => 'Base_Guinee_02',
                'name' => 'Kindia',
            ]);
            Region::create([
                'code' => 'Base_Guinee_03',
                'name' => 'Boke',
            ]);
            Region::create([
                'code' => 'Moyenne_Guinee_01',
                'name' => 'Labe',
            ]);
            Region::create([
                'code' => 'Moyenne_Guinee_02',
                'name' => 'Mamou',
            ]);
            Region::create([
                'code' => 'Haute_Guinee_01',
                'name' => 'Kankan',
            ]);
            Region::create([
                'code' => 'Haute_Guinee_02',
                'name' => 'Faranah',
            ]);
            Region::create([
                'code' => 'Guinee_Forestière',
                'name' => "N'Zerekore",
            ]);
        }
    }
}
