<?php

use App\Commune;
use App\Maire;
use Faker\Factory;
use Illuminate\Database\Seeder;

class MairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();
        $communes = Commune::all();
        if ($communes->count()==38){
            foreach ($communes as $commune){
                if ($commune->id<=33){
                    Maire::create([
                        'firstName' => $fake->firstName,
                        'lastName' => $fake->lastName,
                        'nationalIdentity' =>str_shuffle(rand(0000000,9999999)) .'/'.rand(10,17),
                        'email' =>  strtolower($commune->name.'_maire@gmail.com'),
                        'telephone' => '6'. rand(0,9).rand(0,9).str_shuffle(str_limit(str_shuffle('123456789123456789'), 6, '')),
                        'dateOfBirth' => rand(1,31).'/'.rand(1,12).'/'.rand(1970, 2000),
                        'hireDate' =>  rand(1,31).'/'.rand(1,12).'/'.rand(2010, 2017),
                        'picture' => '/img/avatar3.png',
                        'working' => rand(0,1) ? 'on' : 'off',
                        'role' => 'maire',
                        'verify' => null,
                        'commune_id' => $commune->id,
                        'password' => bcrypt('123456')
                    ]);
                }
            }
        }
    }
}
