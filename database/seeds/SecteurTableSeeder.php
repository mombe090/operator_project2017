<?php

use App\Quartier;
use App\Secteur;
use Faker\Factory;
use Illuminate\Database\Seeder;

class SecteurTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();

            $quartiers = Quartier::all();
           foreach ($quartiers as $quartier){
               for ($i=1; $i<=2; $i++){
                   Secteur::create([
                       'code' => $quartier->name.'_'.'sect'.'_'.$i,
                       'name' => $quartier->name.'_'.$fake->city,
                       'quartier_id' =>$quartier->id
                   ]);
               }
           }

       }

}
