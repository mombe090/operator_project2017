<?php

use App\Region;
use App\Superadmin;
use Faker\Factory;
use Illuminate\Database\Seeder;

class SuperAdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();
        $regions = Region::all();
        foreach ($regions as $region){
                if ($region->name != 'Conakry'){
                    Superadmin::create([
                        'firstName' => $fake->firstName,
                        'lastName' => $fake->lastName,
                        'nationalIdentity' => rand(0000000,9999999).'/'.rand(10,17),
                        'email' =>strtolower($region->name.'_region@gmail.com'),
                        'telephone' => '6'. rand(0,9).rand(0,9).str_shuffle(str_limit(str_shuffle('123456789123456789'), 6, '')),
                        'dateOfBirth' => rand(1,31).'/'.rand(1,12).'/'.rand(1970, 2000),
                        'hireDate' =>  rand(1,31).'/'.rand(1,12).'/'.rand(2010, 2017),
                         'picture' => '/img/avatar.png',
                        'working' => 'on',
                        'role' => 'superadmin',
                        'verify' => null,
                        'region_id' => $region->id,
                        'password' => bcrypt('123456')
                    ]);
                }
        }
    }
}
