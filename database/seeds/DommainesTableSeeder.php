<?php

use Illuminate\Database\Seeder;

class DommainesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       if (DB::table('dommaines')->count()==0){
           DB::table('dommaines')->insert([
               'name' => 'privé'
           ]);
           DB::table('dommaines')->insert([
               'name' => 'etat'
           ]);
           DB::table('dommaines')->insert([
               'name' => 'ambassade'
           ]);
           DB::table('dommaines')->insert([
               'name' => 'compagny'
           ]);
           DB::table('dommaines')->insert([
               'name' => 'none'
           ]);
       }
    }
}
