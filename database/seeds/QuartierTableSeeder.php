<?php

use App\Commune;
use App\Quartier;
use Faker\Factory;
use Illuminate\Database\Seeder;

class QuartierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();

           $communes = Commune::all();
           foreach ($communes as $commune){
              for ($i=1; $i<=1; $i++){
                  Quartier::create([
                     'code' => $commune->name.'_'.$i,
                      'name' => $commune->name.'_'.$fake->city,
                      'commune_id' => $commune->id
                  ]);
              }
           }


    }
}
