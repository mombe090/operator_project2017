<?php

use App\Chefsecteur;
use App\Secteur;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ChefSecteursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();
        $secteurs = Secteur::all();

            foreach ($secteurs as $secteur){
                Chefsecteur::create([
                    'firstName' => $fake->firstName,
                    'lastName' => $fake->lastName,
                    'nationalIdentity' =>str_shuffle(rand(0000000,9999999)) .'/'.rand(10,17),
                    'email' =>strtolower(str_slug($secteur->name, '_').'_secteur@gmail.com'),
                    'telephone' => '6'. rand(0,9).rand(0,9).str_shuffle(str_limit(str_shuffle('1234567891234567895'), 6, '')),
                    'dateOfBirth' => rand(1,31).'/'.rand(1,12).'/'.rand(1970, 2000),
                    'hireDate' =>  rand(1,31).'/'.rand(1,12).'/'.rand(2010, 2017),
                    'picture' => '/img/avatar5.png',
                    'working' => rand(0,1) ? 'on' : 'off',
                    'role' => 'chefSecteur',
                    'verify' => null,
                    'secteur_id' => $secteur->id,
                    'password' => bcrypt('123456')
                ]);
            }
        }
}
