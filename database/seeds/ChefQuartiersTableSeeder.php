<?php


use App\Chefquartier;
use App\Quartier;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ChefQuartiersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Factory::create();
        $quartiers = Quartier::all();

            foreach ($quartiers as $quartier){

                    Chefquartier::create([
                        'firstName' => $fake->firstName,
                        'lastName' => $fake->lastName,
                        'nationalIdentity' =>str_shuffle(rand(0000000,9999999)) .'/'.rand(10,17),
                        'email' =>strtolower(str_slug($quartier->name, '_').'_quartier@gmail.com'),
                        'telephone' => '6'. rand(0,9).rand(0,9).str_shuffle(str_limit(str_shuffle('123456789123456789'), 6, '')),
                        'dateOfBirth' => rand(1,31).'/'.rand(1,12).'/'.rand(1970, 2000),
                        'hireDate' =>  rand(1,31).'/'.rand(1,12).'/'.rand(2010, 2017),
                        'picture' => '/img/avatar04.png',
                        'working' => rand(0,1) ? 'on' : 'off',
                        'role' => 'chefQuartier',
                        'verify' => null,
                        'quartier_id' => $quartier->id,
                        'password' => bcrypt('123456')
                    ]);
                }

        }

}
